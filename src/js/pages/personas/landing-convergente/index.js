// JS inicial para cargar el tema
import 'normalize.css'
import '../../../../scss/pages/personas/landing-convergente/index.scss';
import '../../../components/elementExists';
import 'jquery-mask-plugin/dist/jquery.mask'
import "jquery-validation/dist/jquery.validate";
import popupChipPlans from "./popupChipPlans";
popupChipPlans();
import lazyLoadInit from "../../../components/lazyload";
lazyLoadInit();

import Counter from "../../../components/counter";
Counter();

/** JS para la vista */

/** Scroll */
import anchorLink from "../../../components/anchorLink"
anchorLink()

/** Loading */
// import Loading from "../../../components/loading";
// new Loading().page()

/** Accordeon */
import '../../../components/accordion'

/** Datalayer */
import './datalayer'

/** Form */
// import '../../../components/eInput';
// import './form'

/** Modal */
import { CloseModal, Modal, SceneModal } from "../../../components/modal";
new Modal().init()
new CloseModal().init()

/** Sliders */
import slider from './slider'
slider()

import plans from './plans'
plans()

/** tab */
// import '../../../components/tab'

import banner from "./banner";
banner()

//Auto fill
import './autofill'

//form popup
import "./popupForm";


if(window.matchMedia('(max-width: 1023px)').matches){
    $('.entel-fixed-inbound').show();
    $('.e-oyp-banner').addClass('no-whatsapp')
} else {
    $('.entel-fixed-inbound').hide();
}