import "slick-carousel";
//import { OpenModal } from "../../../components/modal";


export default () => {
  class HomeSlider {
    constructor() {
      this.parameterD = this.getParameterURL('d');
      this.isMobile = window.matchMedia('(max-width: 1023px)').matches;
    }

    scrollAnimation(){
      $('.e-convergente-functions-bottom .entel-data-layer-btn').click(function(){
        $('html, body').animate({
            scrollTop: $(".e-convergente-banner").offset().top
        },1200);
    })
    }

    getParameterURL(parameter) {
      var pageUrl = decodeURIComponent(window.location.search.substring(1)),
            parameters = pageUrl.split('&'),
            parameterName,
            i

        for (i = 0; i < parameters.length; i++) {
            parameterName = parameters[i].split('=')

            if (parameterName[0] === parameter) {
                return parameterName[1] === undefined ? true: parameterName[1]
            }
        }
    }
    getFecha() {
      let newDate = new Date(),
        year = newDate.getFullYear(),
        month =
          newDate.getMonth() < 10
            ? "0" + newDate.getMonth()
            : newDate.getMonth(),
        day =
          newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate(),
        hour =
          newDate.getHours() < 10
            ? "0" + newDate.getHours()
            : newDate.getHours(),
        minutes =
          newDate.getMinutes() < 10
            ? "0" + newDate.getMinutes()
            : newDate.getMinutes(),
        seconds =
          newDate.getSeconds() < 10
            ? "0" + newDate.getSeconds()
            : newDate.getSeconds();

      return (
        year +
        "-" +
        month +
        "-" +
        day +
        " " +
        hour +
        ":" +
        minutes +
        ":" +
        seconds
      );
    }
    // TODO: mejorar form banners
    designByParameter(){
      
      var url = 'https://www.entel.pe/wp-content/uploads/';

      if(this.parameterD ==='69_9_cer_mu' || this.parameterD ==='69_9_cer_mo' || this.parameterD ==='69_9_com_mo' || this.parameterD ==='69_9_com_mu') {
        $('.e-oyp-banner-form--header__inbound').html('<p>Llamándonos al</p><a href="tel:080009013">0800-09013</a><p>o déjanos tu número para llamarte:</p>')
        $('.form-title').html('Empieza a ahorrar ya!')
        $('.e-oyp-popup-bar').show()
        $('.e-oyp-banner').addClass('e-llaa-cvm')
        $('.legales-desk').html('Vale para personas naturales y RUC 10 que contraten mínimo 2 líneas en el plan indicado y que se facturen en el mismo recibo. Sujeto a evaluación crediticia. Beneficio de costo S/ 34.90 aplica solo para el cargo fijo e incluye prorrateo. No acumulable con descuentos en el cargo fijo. Se garantiza el 40% de la velocidad contratada.')
        
        // //DISEÑO
        var imgName = '2023/03/e-banner-llaa-marzo'

        $('.e-oyp-banner-img').html('<picture><source srcset="'+url+imgName+'-big.png" media="(min-width: 1400px)"><source srcset="'+url+imgName+'-desk.png 1x, '+url+imgName+'-desk@2x.png 2x" media="(min-width: 1200px)"><source srcset="'+url+imgName+'-lap.png 1x, '+url+imgName+'-lap@2x.png 2x" media="(min-width: 1024px)"><source srcset="'+url+imgName+'-tab.png 1x, '+url+imgName+'-tab@2x.png 2x" media="(min-width: 768px)"><img src="'+url+imgName+'-mob.png" srcset="'+url+imgName+'-mob.png 1x, '+url+imgName+'-mob@2x.png 2x" alt="Migra este verano con Entel" title="Migra este verano con Entel"></picture>')

      } else if(this.parameterD === 'gsemoyp'){
        $('.e-oyp-banner-form--header__inbound').html('<p>Llamándonos al</p><a href="tel:080009012">0800-09012</a><p>o déjanos tu número para llamarte:</p>')
      } else {
        console.log(this.parameterD)
      }
    }

    codOrigenByParameter(){
      let codOrigen = ''

      if (this.parameterD ==='branding'){
        codOrigen = 'Otros_MediosBranding'
      } else if(this.parameterD ==='gsemoyp'){
        codOrigen = 'C2C_GoogleSEM_LLAA'
      } else if(this.parameterD ==='35_9_cer_mo'){
        codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
      } else if(this.parameterD ==='35_9_com_mo'){
        codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
      } else if(this.parameterD ==='35_9_cer_mu'){
        codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
      } else if(this.parameterD ==='35_9_com_mu'){
        codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
      } else if(this.parameterD ==='45_9_cer_mo'){
        codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
      } else if(this.parameterD ==='45_9_com_mo'){
        codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
      } else if(this.parameterD ==='45_9_cer_mu'){
        codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
      } else if(this.parameterD ==='45_9_com_mu'){
        codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
      } else if(this.parameterD ==='109_9_cer_mo'){
        codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
      } else if(this.parameterD ==='109_9_com_mo'){
        codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
      } else if(this.parameterD ==='109_9_cer_mu'){
        codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
      } else if(this.parameterD ==='109_9_com_mu'){
        codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
      } else if(this.parameterD ==='55_9_cer_mo'){
        codOrigen = 'C2C_CVM_LLAA_55_9_Cerrado_Mono'
      } else if(this.parameterD ==='55_9_com_mo'){
        codOrigen = 'C2C_CVM_LLAA_55_9_Compartido_Mono'
      } else if(this.parameterD ==='55_9_cer_mu'){
        codOrigen = 'C2C_CVM_LLAA_55_9_Cerrado_Mono'
      } else if(this.parameterD ==='55_9_com_mu'){
        codOrigen = 'C2C_CVM_LLAA_55_9_Compartido_Mono'
      } else if(this.parameterD ==='69_9_cer_mo'){
        codOrigen = 'C2C_CVM_LLAA_69_9_Cerrado_Mono'
      } else if(this.parameterD ==='69_9_com_mo'){
        codOrigen = 'C2C_CVM_LLAA_69_9_Compartido_Mono'
      } else if(this.parameterD ==='69_9_cer_mu'){
        codOrigen = 'C2C_CVM_LLAA_69_9_Cerrado_Mono'
      } else if(this.parameterD ==='69_9_com_mu'){
        codOrigen = 'C2C_CVM_LLAA_69_9_Compartido_Mono'
      } else if(this.parameterD ==='89_9_cer_mo'){
        codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
      } else if(this.parameterD ==='89_9_com_mo'){
        codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
      } else if(this.parameterD ==='89_9_cer_mu'){
        codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
      } else if(this.parameterD ==='89_9_com_mu'){
        codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
      } else{
        codOrigen = 'C2C_Web_LandingLLAA'
      }
      return codOrigen
    }

    sendToSmartLeads(data){
      let leadData = {},
          apiURL = 'https://middleware-entel.segmentid.pro/api/leads/sync',
          cod_origen = this.codOrigenByParameter(),
          current_url = window.location.origin + window.location.pathname

      leadData = {
        ip: this.getcurrentIp(),
        cod_origen: cod_origen,
        telefono: (data.phone) ? data.phone : '',
        operador: "",
        nombre: "",
        email: "",
        tipo_doc: "DNI",
        documento: "",
        modalidad: "Postpago Migra",
        plan: "Cuota cero",
        producto: "Banner Lineas Adicionales",
        departamento: "",
        fechalead: this.getFecha(),
        faseembudo: "",
        resultado_crediticia: "",
        resultado_portabilidad: "",
        utm_source: $('#utm_source').val(),
        utm_medium: $('#utm_medium').val(),
        utm_campaign: $('#utm_campaign').val(),
        utm_content: $('#utm_content').val(),
        utm_term: $('#utm_term').val(),
        gclid: $('#gclid').val(),
        producto_marca: "",
        producto_cantidad_cuotas: "",
        producto_monto_cuota_inicial: "",
        producto_monto_cuota_mensual: "",
        mercado: "personas",
        politicas_privacidad_datos: 1,
        fecha_programada: "",
        segmento: "",
        score: "",
        url_lead: current_url,
        extra1: "",
        extra2: "",
        extra3: "",
        prioridad: "",
      }
      var xhr = new XMLHttpRequest()

        xhr.onreadystatechange = function (e) {
            var responseId = xhr.responseText.slice(6).trim().split(',')[0]

            switch (xhr.readyState) {
                case 1:
                    if (data.isLoading !== null && data.isLoading !== undefined) {
                        data.isLoading()
                    }
                break

                case 4:
                    switch (xhr.status) {
                        // success
                        case 201:
                            var dataLayer = window.dataLayer || []
                            dataLayer.push({
                                event: 'gtm.event',
                                eventCategory: 'Ecommerce - Envío de formulario',
                                eventAction: 'Submit',
                                eventLabel: 'Success',
                                dimension1: (data.origin) ? data.origin : '',
                                dimension2: responseId,
                                dimension3: data.phone
                            })
                            dataLayer.push({
                                dimension1: '',
                                dimension2: '',
                                dimension3: ''
                            })

                            if (data.isSended !== null && data.isSended !== undefined) {
                                data.isSended()
                            }

                            if (data.isSuccess !== null && data.isSuccess !== undefined) {
                                data.isSuccess()
                            }
                        break

                        // repeated
                        case 200:
                            var dataLayer = window.dataLayer || []
                            dataLayer.push({
                                event: 'gtm.event',
                                eventCategory: 'Ecommerce - Envío de formulario',
                                eventAction: 'Submit',
                                eventLabel: 'Failure - Duplicated'
                            })

                            if (data.isSended !== null && data.isSended !== undefined) {
                                data.isSended()
                            }

                            if (data.isRepeated !== null && data.isRepeated !== undefined) {
                                data.isRepeated()
                            }
                        break

                        // error
                        default:
                            var dataLayer = window.dataLayer || []
                            dataLayer.push({
                                event: 'gtm.event',
                                eventCategory: 'Ecommerce - Envío de formulario',
                                eventAction: 'Submit',
                                eventLabel: 'Failure - Server Error'
                            })

                            if (data.isError !== null && data.isError !== undefined) {
                                data.isError()
                            }
                        break
                    }
                break
            }
        }

        setTimeout (function() {
            xhr.open('POST', apiURL)
            xhr.setRequestHeader('Content-Type', 'application/json')
            xhr.setRequestHeader('Accept', '*/*')
            xhr.send(JSON.stringify(leadData))
        }, 100)
    }

    sendBannerForm(){
      let $bannerForm = $('#entelFormBanner')
      
      $bannerForm.on('submit', function (evt) {
        evt.preventDefault()
    }).validate({
        rules: {
            lead_phone: {
                required: true,
                minlength: 7,
                maxlength: 9
            },
            lead_policy: {
                required: true
            }
        },
        messages: {
            lead_policy: {
                required: ''
            },
            lead_phone: {
                required: '',
                minlength: '',
                maxlength: ''
            }
        },
        highlight: function (element, errorClass) {
            $(element).siblings('.input-mask-top').addClass('input-mask-top-error')
            $(element).addClass('text-error')
        },
        unhighlight: function (element, errorClass) {
            $(element).siblings('.input-mask-top').removeClass('input-mask-top-error')
            $(element).removeClass('text-error')
        },
        submitHandler: function() {
            var dataForm
            // setting data to send to smart leads
            dataForm = {
                phone: $bannerForm.find('input[name="lead_phone"]').val(),
                isLoading: function() {
                    // is loading
                    $('#entel-form-top-btn').prop('disabled', true)
                    $('.e-oyp-banner-form--header').removeClass('active')
                    $('.e-oyp-banner-form--loading').addClass('active')

                },
                isSended: function() {
                    // is sended
                    $('.e-oyp-banner-form--loading').removeClass('active')
                    $('.e-oyp-banner-form--message').addClass('active')

                    $bannerForm.trigger('reset')
                    $('#entel-form-top-btn').prop('disabled', false)

                    setTimeout(function() {
                        $('.e-oyp-banner-form--message').removeClass('active')
                        $('.e-oyp-banner-form--header').addClass('active')
                    }, 3000)
                },
                isSuccess: function() {
                    // is success
                    $bannerForm.find('.msg-duplicate-number-banner').addClass('hidden')
                    $bannerForm.find('.msg-duplicate-number-banner').removeClass('bounce animated')

                    dataLayer.push({
                        'event': 'ModalOfertasPromociones',
                        'Ofertas y Promociones': [{
                            'data-gtm-category': 'form superior',
                            'data-gtm-label': $('#entel-form-btn').data('gtm-label'),
                            'data-gtm-action': window.location.href
                        }]
                    })
                },
                isRepeated: function() {
                    // is repeated
                    $bannerForm.find('.msg-duplicate-number-banner').removeClass('hidden')
                    $bannerForm.find('.msg-duplicate-number-banner').addClass('bounce animated')
                    $(containerName+ '__form--message').removeClass('active')

                    setTimeout(function() {
                        $bannerForm.find('.msg-duplicate-number-banner').addClass('hidden')
                        $bannerForm.find('.msg-duplicate-number-banner').removeClass('bounce animated')
                    }, 3000)
                },
                isError: function() {
                    // is sended
                    console.log('error')
                }
            }

            new HomeSlider().sendToSmartLeads(dataForm)

            return false;
          }
      })
    }

    sendPopupForm(){
      let $popupForm = $('#entelFormPopupCintillo')
      
      $popupForm.on('submit', function (evt) {
        evt.preventDefault()
    }).validate({
        rules: {
            lead_phone: {
                required: true,
                minlength: 7,
                maxlength: 9
            },
            lead_policy: {
                required: true
            }
        },
        messages: {
            lead_policy: {
                required: ''
            },
            lead_phone: {
                required: '',
                minlength: '',
                maxlength: ''
            }
        },
        highlight: function (element, errorClass) {
            $(element).siblings('.input-mask-top').addClass('input-mask-top-error')
            $(element).addClass('text-error')
        },
        unhighlight: function (element, errorClass) {
            $(element).siblings('.input-mask-top').removeClass('input-mask-top-error')
            $(element).removeClass('text-error')
        },
        submitHandler: function() {
            var dataForm
            // setting data to send to smart leads
            dataForm = {
                phone: $popupForm.find('input[name="lead_phone"]').val(),
                isLoading: function() {
                    // is loading
                    $('#entel-form-top-btn').prop('disabled', true)
                    $('.e-oyp-popup-form__content--header').removeClass('active')
                    $('.e-oyp-popup-form__content--loading').addClass('active')

                },
                isSended: function() {
                    // is sended
                    $('.e-oyp-popup-form__content--loading').removeClass('active')
                    $('.e-oyp-popup-form__content--message').addClass('active')

                    $popupForm.trigger('reset')
                    $('#entel-form-top-btn').prop('disabled', false)

                    setTimeout(function() {
                        $('.e-oyp-popup-form__content--message').removeClass('active')
                        $('.e-oyp-popup-form__content--header').addClass('active')
                    }, 3000)
                },
                isSuccess: function() {
                    // is success
                    $popupForm.find('.msg-duplicate-number-banner').addClass('hidden')
                    $popupForm.find('.msg-duplicate-number-banner').removeClass('bounce animated')

                    dataLayer.push({
                        'event': 'ModalOfertasPromociones',
                        'Ofertas y Promociones': [{
                            'data-gtm-category': 'form superior',
                            'data-gtm-label': $('#entel-form-btn').data('gtm-label'),
                            'data-gtm-action': window.location.href
                        }]
                    })
                },
                isRepeated: function() {
                    // is repeated
                    $popupForm.find('.msg-duplicate-number-banner').removeClass('hidden')
                    $popupForm.find('.msg-duplicate-number-banner').addClass('bounce animated')
                    $(containerName+ '__form--message').removeClass('active')

                    setTimeout(function() {
                        $popupForm.find('.msg-duplicate-number-banner').addClass('hidden')
                        $popupForm.find('.msg-duplicate-number-banner').removeClass('bounce animated')
                    }, 3000)
                },
                isError: function() {
                    // is sended
                    console.log('error')
                }
            }

            new HomeSlider().sendToSmartLeads(dataForm)

            return false;
          }
      })
    }
    
    eventCheckTerms(form) {
      var inputmasktop = form.querySelector(".input-mask-top");

      let handleClick = () => {
        $(form).find(".input-mask-top").toggleClass("error");

        if ($("#checktop").prop("checked")) {
          $("#checktop").prop("checked", false);
        } else {
          $("#checktop").prop("checked", true);
        }
      }

      inputmasktop.addEventListener("click", handleClick);
    }

    validateNumberInputForm(){
      $('input[type="tel"]').mask('Z99999999', {
        translation: {
            'Z': {
                pattern: /[9-9]/
            }
        }
      })

      $('.validonlynumeric').on('keypress keyup blur',function (event) {
          $(this).val($(this).val().replace(/[^\d].+/, ''))
          if ((event.which!= 13) && (event.which < 48 || event.which > 57)) {
          event.preventDefault()
          }
      })
    }

    validateCheckTerms(form) {
      var isChecked = document.getElementById("checktop").checked,
        divChecked = form.querySelector(".input-mask-top"),
        msj;

      if (!isChecked) {
        divChecked.classList.add("error");
        msj = { status: 0, msj: "Acepte terminos" };
      } else {
        divChecked.classList.remove("error");
        msj = { status: 1, msj: "Acepto" };
      }

      return msj;
    }

    getcurrentIp() {
      let ipObject = { ip: '' };
      const ipRequest = new XMLHttpRequest();
      ipRequest.open('GET', 'https://api.ipify.org/?format=json', false);
      
       try {
          ipRequest.send();

          if (ipRequest.status == 200) {
            ipObject = JSON.parse(ipRequest.responseText);
          }
       } catch (error) {
          console.log(error)
          return ipObject.ip;
       }

      return ipObject.ip;
    }

    cintoPopup(){
      $('#popup-banner').on('click',function(){
        $('.popup-overlay').show();
        $('.e-oyp-popup').show();
      })
      $('.e-oyp-popup-close').on('click',function(){
        $('.popup-overlay').hide();
        $('.e-oyp-popup').hide();
      })
    }

    load() {
      // this.slider()
      this.cintoPopup();
      this.validateNumberInputForm();
      this.sendPopupForm();
      this.sendBannerForm();
      // TODO: mejorar form banners
      // this.formBanners();
      // this.hiddenform();
      this.designByParameter();
      // this.inboundByParameter();
      this.scrollAnimation();
    }
  }

  // load
  new HomeSlider().load();
};
