import 'slick-carousel'
import '../../../components/appear'
import mediaQuery from "../../../components/mediaQuery";
import {
    codeOS,
} from "../../../components/devicesOs"
export default () => {
    class Plans {
        constructor() {
        }

        loadLinkMore() {
            let linkMore = document.querySelectorAll(".e-link__more"),
            currentTarget,
            nextTarget,
            actualTarget;
        
            if(linkMore.length > 0){
                linkMore.forEach((current) => {
                    current.addEventListener("click", (item,i) => {
                    currentTarget = item.currentTarget;
                    nextTarget = (currentTarget.nextElementSibling) ? currentTarget.nextElementSibling : false;
                    actualTarget = document.querySelector(".js--active");
        
                    if(actualTarget && actualTarget != currentTarget) {
                        actualTarget.nextElementSibling.classList.remove('js--active');
                        actualTarget.classList.remove('js--active');
                    }
        
                    if(!nextTarget.classList.contains('js--active')) {
                        nextTarget.classList.add('js--active');
                        currentTarget.classList.add('js--active');
                    } else {
                        nextTarget.classList.remove('js--active');
                        currentTarget.classList.remove('js--active');
                    }
        
                    });
                });
            };
        }

        load() {
            this.loadLinkMore();
        }
    }

    // init
    new Plans().load()
}