import 'jquery-validation/dist/jquery.validate'
import 'jquery-mask-plugin/dist/jquery.mask'

import { CloseModal, ChangeScene, OpenModal, Modal, SceneModal } from "../../../components/modal";

let contact = (function () {
    let init

    const formId = document.getElementById('landing-convergente__form')

    class Form {
        constructor($form) {
            this.$form = $form
        }

        init() {
            this.validateDoc()
            this.validate()
            this.mask()
        }

        reset() {
            formId.reset();
        }

        mask() {
            this.$form.find('#celular').mask('999999999')
            this.$form.find('#celular').mask('Z00000000', {
                translation: {
                    'Z': {
                        pattern: /[9]/,
                        optional: false
                    }
                }
            })
            this.$form.find('#ruc').mask('99999999999')
            this.$form.find('#ruc').mask('YZ000000000', {
                translation: {
                    'Y': {
                        pattern: /[2]/,
                        optional: false
                    },
                    'Z': {
                        pattern: /[0]/,
                        optional: false
                    }
                }
            })
        }

        validate() {
            let $form = this.$form
                // ruc = this.$form.find('#ruc').val(),
                // name = this.$form.find('#nombres').val(),
                // department = this.$form.find('#departamento').val(),
                // numPhones = this.$form.find('#equipo_cantidad').val(),
                // plan = this.$form.find('#plan').val()

            $form.on('submit', function (e) {
                e.preventDefault()
            }).validate({
                rules: {                    
                    nombre: {
                        required: true,
                        maxlength: 60
                    },
                    email: {
                        required: true,
                        maxlength: 80,
                        validate_email: true
                    },
                    celular: {
                        required: true,
                        maxlength: 9
                    },
                    term_politics: {
                        required: true,
                        minlength: 1
                    },
                    numero_documento: {
                        required: true
                    },
                    tipo_documento: {
                        required: true
                    },
                    autotratamiento: {
                        required: true
                    },
                    // servicio_elegido: {
                    //     required: true
                    // },
                },
                messages: {
                    nombres: {
                        required: "Ingrese un nombre",
                        maxlength: "Ingrese un nombre válido"
                    },
                    ruc: {
                        required: "Por favor ingrese un celular",
                        maxlength: "Ingrese un RUC válido"
                    },
                    email: {
                        required: "Por favor ingrese un email",
                        maxlength: "Ingrese un email válido",
                        email: "Ingrese un email válido"
                    },
                    celular: {
                        required: "Por favor ingrese un celular",
                        maxlength: "Ingrese un celular válido"
                    },
                    equipo_cantidad: {
                        required: "Por favor seleccione una opción",
                    },
                    term_politics: {
                        required: "Por favor acepte las políticas de privacidad",
                    }   
                },
                submitHandler: function (form) {
                    
                    // submithandler_ajax = $this.data('submithandler-ajax');

                    $.ajax({
                        type: 'POST',
                        url: ADMIN_URL,
                        data: {
                            action: 'enviar_cobranzas',
                            dataString: $form.serialize()
                        },
                        success: function (data) {
                            // new ChangeScene("e-trivia__back", "e-trivia__front").init()
                            new CloseModal().close()
                            formId.reset()
                            setTimeout(function(){
                                new OpenModal('e-mis-out__modal-success').init()
                            }, 200);

                        },
                        error: function (err) {
                            // console.log(':(')
                            new CloseModal().close()
                            formId.reset()
                            setTimeout(function(){
                                new OpenModal('e-mis-out__modal-error').init()
                            }, 200);
                        }
                    }) 
                },
                errorPlacement: function (error, element) {
                    error.insertBefore(element);
                }
            })

            $.validator.addMethod("validate_email", function (value, element) {
                if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                    return true
                } else {
                    return false
                }
            }, 'Correo Inv&aacute;lido.')

            $.validator.addMethod("validate_ruc", function (value, element) {
                if (value === "20000000000" 
                || value === "20111111111" 
                || value === "20222222222" 
                || value === "20333333333" 
                || value === "20444444444" 
                || value === "20555555555" 
                || value === "20666666666" 
                || value === "20777777777" 
                || value === "20888888888" 
                || value === "20999999999") {
                    return false
                } else {
                    return true
                }
            }, 'RUC Inv&aacute;lido.')
        }

        validateDoc(){
            let $tipodocumento = $('.entel-tipodocumento');
            $tipodocumento.each(function () {
                var $this = $(this),
                $select_tipodoc = $this.find('select[name="tipo_documento"]'),
                $input_numdoc = $this.find('input[name="numero_documento"]');
                
                $input_numdoc.prop('disabled', true);
                
                $select_tipodoc.on('change', function () {
                    var $this = $(this),
                    tipo_doc = $this.find('option:selected').text();
                    // console.log(tipo_doc)
                    
                    if (tipo_doc != "Tipo documento") {
                        $input_numdoc.prop('disabled', false);
                        
                        if (tipo_doc == 'CE' || tipo_doc == 'Pasaporte') {
                            // console.log('validando tipo doc')
                            $input_numdoc.val('');
                            $input_numdoc.attr('type', 'text');
                            $input_numdoc.attr('maxlength', '12');
                            $input_numdoc.mask('AAAAAAAAAAAA', {'translation': {
                                    A: {pattern: /[A-Z a-z áéúíóñÑÁÉÍÓÚ 0-9 .-]/}
                                }
                            });
    
                        } else if (tipo_doc == 'DNI') {
                            $input_numdoc.val('');
                            $input_numdoc.attr('type', 'tel');
                            $input_numdoc.attr('minlength', '8');
                            $input_numdoc.attr('maxlength', '8');
                            $input_numdoc.mask('00000000');
    
                        } else if (tipo_doc == 'RUC') {
                            $input_numdoc.val('');
                            $input_numdoc.attr('type', 'tel');
                            $input_numdoc.attr('maxlength', '11');
                            $input_numdoc.mask('AZ00000000Y', {
                                translation: {
                                    'A': {
                                        pattern: /[1-1]/
                                    },
                                    'Z': {
                                        pattern: /[0-0]/
                                    },
                                    'Y': {
                                        pattern: /[0-9]/
                                    }
                                },
                            });
    
                        }
                        
                    } else {
                        $input_numdoc.prop('disabled', true);
                    }
                });
            })
        }
    }

    init = function () {
        let $form = $('#landing-convergente__form')

        // load phrases section
        if ($form.mExists()) {
            new Form($form).init()
        }

    }

    return {
        init: init
    }
}())

contact.init()