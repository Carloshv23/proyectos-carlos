import 'slick-carousel'
import '../../../components/appear'
import mediaQuery from "../../../components/mediaQuery";
import {
    codeOS,
} from "../../../components/devicesOs"
export default () => {
    class Slider {
        constructor() {
            // this.sliderSelector = '.e-convergente-discount__slider'
            // this.$slider = document.querySelector(this.sliderSelector)

            this.sliderSelector2 = '.e-convergente-plans__slider'
            this.$slider2 = document.querySelector(this.sliderSelector2)
        }
        
        slider() {
            // $(this.sliderSelector).slick({
            //     rows: 0,
            //     dots: true,
            //     fade: true,
            //     arrows: false,
            //     autoplay: false,
            //     infinite: false,
            //     slidesToScroll: 1,
            //     mobileFirst: true,
            //     cssEase: 'ease',
            //     slidesToShow: 1,
            //     centerMode: false,
            //     pauseOnHover:false,
            //     autoplaySpeed: 5000,
            //     centerPadding: '0',
            //     adaptiveHeight: false,
            //     pauseOnDotsHover: false,
            //     centerMode: true,
            //     responsive: [
            //         {
            //             breakpoint: 767,
            //             settings: {
            //                 slidesToShow: 2,
            //                 fade: false,
            //             }
            //         },{
            //             breakpoint: 1023,
            //             settings: {
            //                 slidesToShow: 3,
            //                 fade: false,
            //             }
            //         }
            //     ]
            // })

            $(this.sliderSelector2).slick({
                rows: 0,
                dots: true,
                fade: true,
                arrows: true,
                autoplay: false,
                infinite: false,
                slidesToScroll: 1,
                mobileFirst: true,
                cssEase: 'ease',
                slidesToShow: 1,
                pauseOnHover:false,
                autoplaySpeed: 5000,
                centerPadding: '0',
                adaptiveHeight: false,
                pauseOnDotsHover: false,
                initialSlide: 2,
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            fade: false,
                        }
                    },{
                        breakpoint: 1023,
                        settings: {
                            slidesToShow: 3,
                            fade: false,
                        }
                    },{
                        breakpoint: 1300,
                        settings: {
                            slidesToShow: 4,
                            fade: false,
                        }
                    }
                ]
            })

            // $('.slick-arrow').addClass('entel-data-layer-btn');

            // $(".e-convergente-discount__slider").on('afterChange', function (event, slick, currentSlide, nextSlide) {
            //     dataLayer.push({
            //         event: "virtualEvent",
            //         category: "Landing Convergente",
            //         action: "Visualizar contenido – Flechas de desplazamiento",
            //         label: "Te contamos cómo - cambio de paso"
            //     });      
            // });

            $(".e-convergente-plans__slider").on('afterChange', function (event, slick, currentSlide, nextSlide) {
                dataLayer.push({
                    event: "virtualEvent",
                    category: "Landing Convergente",
                    action: "Visualizar contenido – Flechas de desplazamiento",
                    label: "Planes - rotacion de plan"
                });      
            });

        }

        load() {
            if (this.$slider2) {
                this.slider()
            }
        }
    }

    // init
    new Slider().load()
}