var autofill_app = {
    phone: "",
    init: function () {
        var _this = this

        _this.getPhone()

        if (_this.phone != "") {
            _this.fillPhone()
            _this.clearPhone()
            _this.sendDataLayer()
        }
    },
    getPhone: function () {
        var _this = this,
            phone = localStorage.getItem("autofill_phone")

        if (phone == null) {
            var hash = window.location.hash

            if (/cel=9\d{8}/.test(hash)) {
                _this.phone = hash.substring(hash.indexOf("cel=9"), 14).split("=")[1]
                localStorage.setItem("autofill_phone", _this.phone)
            }
        } else {
            _this.phone = phone;
        }
    },
    fillPhone: function () {
        var _this = this
        document.querySelectorAll(".entel-form-oyp-number input").forEach(function (element) {
            element.value = _this.phone
        })
    },
    clearPhone: function () {
        window.location.hash = window.location.hash.replace(/cel=9\d{8}/, "")
    },
    sendDataLayer: function() {
        var dataLayer = window.dataLayer || [];
        var _this = this,
            hashPhone = window.btoa(_this.phone)

        dataLayer.push({
          'event': 'gtm.event',
          'eventCategory': 'Landing Autofill',
          'eventAction': 'Autofill',
          'eventLabel': hashPhone
        })
    }
}
autofill_app.init()