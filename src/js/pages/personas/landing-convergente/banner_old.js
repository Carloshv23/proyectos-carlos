import "slick-carousel";

export default () => {
  class HomeSlider {
    constructor() {}

    scrollAnimation(){
      $('.e-convergente-functions-bottom .entel-data-layer-btn').click(function(){
        $('html, body').animate({
            scrollTop: $(".e-convergente-banner").offset().top
        },1200);
    })
    }

    getParameterURL(parameter) {
      var pageUrl = decodeURIComponent(window.location.search.substring(1)),
            parameters = pageUrl.split('&'),
            parameterName,
            i

        for (i = 0; i < parameters.length; i++) {
            parameterName = parameters[i].split('=')

            if (parameterName[0] === parameter) {
                return parameterName[1] === undefined ? true: parameterName[1]
            }
        }
    }
    getFecha() {
      let newDate = new Date(),
        year = newDate.getFullYear(),
        month =
          newDate.getMonth() < 10
            ? "0" + newDate.getMonth()
            : newDate.getMonth(),
        day =
          newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate(),
        hour =
          newDate.getHours() < 10
            ? "0" + newDate.getHours()
            : newDate.getHours(),
        minutes =
          newDate.getMinutes() < 10
            ? "0" + newDate.getMinutes()
            : newDate.getMinutes(),
        seconds =
          newDate.getSeconds() < 10
            ? "0" + newDate.getSeconds()
            : newDate.getSeconds();

      return (
        year +
        "-" +
        month +
        "-" +
        day +
        " " +
        hour +
        ":" +
        minutes +
        ":" +
        seconds
      );
    }
    // TODO: mejorar form banners
    designByParameter(){
      if(this.getParameterURL('d')==='109_9_cer_mo' || this.getParameterURL('d')==='109_9_cer_mu' || this.getParameterURL('d')==='109_9_com_mo' || this.getParameterURL('d')==='109_9_com_mu'){
        $('.e-convergente-banner-image').addClass('e-convergente-banner-image-extra')
        $('.e-convergente-banner-image__first-banner').removeClass('active')
        $('.e-convergente-banner-image__second-banner').addClass('active')
        $('.e-convergente-banner-legales').html('<p>Vale en entel.pe hasta el 31/03/22 para portar (origen postpago) con plan indicado. Sujeto a evaluación crediticia y con acuerdo de equipo en cuotas a 12 meses. Stock min: 10 equipos por modelo. Colores sujetos a disponibilidad. El pago mensual por 12 meses incluirá el cargo fijo del plan y la cuota mensual del equipo. Líneas adicionales: Válido para personas naturales o RUC 10 que contraten mínimo 2 líneas en el mismo plan Entel Power Familiar y que se facturen en el mismo recibo. Las líneas deberán pertenecer al mismo titular y mismo cargo fijo. Descuento incluye prorrateo y aplica solo sobre el cargo fijo. No es acumulable con otros descuentos. Condiciones para portar y restricciones: catalogo.entel.pe</p>')
      } else if(this.getParameterURL('d')==='55_9_cer_mo' || this.getParameterURL('d')==='55_9_cer_mu' || this.getParameterURL('d')==='55_9_com_mo' || this.getParameterURL('d')==='55_9_com_mu') {
        $('.e-convergente-banner-image').addClass('e-convergente-banner-image-extra')
        $('.e-convergente-banner-image__first-banner').removeClass('active')
        $('.e-convergente-banner-image__third-banner').addClass('active')
        $('.e-convergente-banner-legales').html('<p>Vale en entel.pe hasta el 31/03/22 para portar (origen postpago) con plan indicado. Sujeto a evaluación crediticia y con acuerdo de equipo en cuotas a 12 meses. Stock min: 10 equipos por modelo. Colores sujetos a disponibilidad. El pago mensual por 12 meses incluirá el cargo fijo del plan y la cuota mensual del equipo. Líneas adicionales: Válido para personas naturales o RUC 10 que contraten mínimo 2 líneas en el mismo plan Entel Power Familiar y que se facturen en el mismo recibo. Las líneas deberán pertenecer al mismo titular y mismo cargo fijo. Descuento incluye prorrateo y aplica solo sobre el cargo fijo. No es acumulable con otros descuentos. Condiciones para portar y restricciones: catalogo.entel.pe</p>')
      } else if(this.getParameterURL('d')==='69_9_cer_mo' || this.getParameterURL('d')==='69_9_cer_mu' || this.getParameterURL('d')==='69_9_com_mo' || this.getParameterURL('d')==='69_9_com_mu') {
        $('.e-convergente-banner-image').addClass('e-convergente-banner-image-extra')
        $('.e-convergente-banner-image__first-banner').removeClass('active')
        $('.e-convergente-banner-image__fourth-banner').addClass('active')
        $('.e-convergente-banner-legales').html('<p>Vale en entel.pe hasta el 31/03/22 para portar (origen postpago) con plan indicado. Sujeto a evaluación crediticia y con acuerdo de equipo en cuotas a 12 meses. Stock min: 10 equipos por modelo. Colores sujetos a disponibilidad. El pago mensual por 12 meses incluirá el cargo fijo del plan y la cuota mensual del equipo. Líneas adicionales: Válido para personas naturales o RUC 10 que contraten mínimo 2 líneas en el mismo plan Entel Power Familiar y que se facturen en el mismo recibo. Las líneas deberán pertenecer al mismo titular y mismo cargo fijo. Descuento incluye prorrateo y aplica solo sobre el cargo fijo. No es acumulable con otros descuentos. Condiciones para portar y restricciones: catalogo.entel.pe</p>')
      } else if(this.getParameterURL('d')==='89_9_cer_mo' || this.getParameterURL('d')==='89_9_cer_mu' || this.getParameterURL('d')==='89_9_com_mo' || this.getParameterURL('d')==='89_9_com_mu') {
        $('.e-convergente-banner-image').addClass('e-convergente-banner-image-extra')
        $('.e-convergente-banner-image__first-banner').removeClass('active')
        $('.e-convergente-banner-image__fifth-banner').addClass('active')
        $('.e-convergente-banner-legales').html('<p>Vale en entel.pe hasta el 31/03/22 para portar (origen postpago) con plan indicado. Sujeto a evaluación crediticia y con acuerdo de equipo en cuotas a 12 meses. Stock min: 10 equipos por modelo. Colores sujetos a disponibilidad. El pago mensual por 12 meses incluirá el cargo fijo del plan y la cuota mensual del equipo. Líneas adicionales: Válido para personas naturales o RUC 10 que contraten mínimo 2 líneas en el mismo plan Entel Power Familiar y que se facturen en el mismo recibo. Las líneas deberán pertenecer al mismo titular y mismo cargo fijo. Descuento incluye prorrateo y aplica solo sobre el cargo fijo. No es acumulable con otros descuentos. Condiciones para portar y restricciones: catalogo.entel.pe</p>')
      }
    }

    binEventsFormBanner(form) {
      if (form) {
        this.eventCheckTerms(form);
        
        form.addEventListener("input", () => {
          this.validateInputPhoneNumber(form);

          // pause slider
          $(this.homeSlider).slick('slickSetOption', 'autoplay', false).slick('slickPause');
          $(this.navSlider).slick('slickSetOption', 'autoplay', false).slick('slickPause');
        })

        let handleSubmit = (e) => {
          e.preventDefault();
          
          let isChecked = this.validateCheckTerms(form);
          let isNumero = this.validateInputPhoneNumber(form);
          let codOrigen = 'C2C_Web_LandingLLAA'

          if (isNumero.status === 1 && isChecked.status === 1) {
            $(form).find(".form--body").hide();
            $(form).find(".form--loading").show();

            if (this.getParameterURL('d')==='branding'){
              codOrigen = 'Otros_MediosBranding'
            } else if(this.getParameterURL('d')==='gsemoyp'){
              codOrigen = 'C2C_GoogleSEM_LLAA'
            } else if(this.getParameterURL('d')==='35_9_cer_mo'){
              codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='35_9_com_mo'){
              codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='35_9_cer_mu'){
              codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='35_9_com_mu'){
              codOrigen = 'C2C_CVM_LLAA_35_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='45_9_cer_mo'){
              codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='45_9_com_mo'){
              codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='45_9_cer_mu'){
              codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='45_9_com_mu'){
              codOrigen = 'C2C_CVM_LLAA_45_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='109_9_cer_mo'){
              codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='109_9_com_mo'){
              codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='109_9_cer_mu'){
              codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='109_9_com_mu'){
              codOrigen = 'C2C_CVM_LLAA_109_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='55_9_cer_mo'){
              codOrigen = 'C2C_CVM_LLAA_55_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='55_9_com_mo'){
              codOrigen = 'C2C_CVM_LLAA_55_9_Compartido_Mono'
            } else if(this.getParameterURL('d')==='55_9_cer_mu'){
              codOrigen = 'C2C_CVM_LLAA_55_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='55_9_com_mu'){
              codOrigen = 'C2C_CVM_LLAA_55_9_Compartido_Mono'
            } else if(this.getParameterURL('d')==='69_9_cer_mo'){
              codOrigen = 'C2C_CVM_LLAA_69_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='69_9_com_mo'){
              codOrigen = 'C2C_CVM_LLAA_69_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='69_9_cer_mu'){
              codOrigen = 'C2C_CVM_LLAA_69_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='69_9_com_mu'){
              codOrigen = 'C2C_CVM_LLAA_69_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='89_9_cer_mo'){
              codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='89_9_com_mo'){
              codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='89_9_cer_mu'){
              codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
            } else if(this.getParameterURL('d')==='89_9_com_mu'){
              codOrigen = 'C2C_CVM_LLAA_89_9_Cerrado_Mono'
            }
            
            // const numBanner = $(this.homeSlider).slick('slickCurrentSlide')+1;

            this.sendLeadData(form, codOrigen)

            // play slider
            $(this.navSlider).slick('slickSetOption', 'autoplay', true).slick('slickPlay');
            $(this.homeSlider).slick('slickSetOption', 'autoplay', true).slick('slickPlay');
            
            // next slider
            setTimeout(() => {
              $(this.homeSlider).slick('slickNext');
            }, 2000)
          }
        }
        
        form.addEventListener("submit", handleSubmit);
      }
    }

    eventCheckTerms(form) {
      var inputmasktop = form.querySelector(".input-mask-top");

      let handleClick = () => {
        $(form).find(".input-mask-top").toggleClass("error");

        if ($("#checktop").prop("checked")) {
          $("#checktop").prop("checked", false);
        } else {
          $("#checktop").prop("checked", true);
        }
      }

      inputmasktop.addEventListener("click", handleClick);
    }

    validateInputPhoneNumber(form) {
      var input = form.querySelector(".validonlynumeric"),
        tel = input.value,
        cont = 9,
        datalength,
        msj = { status: 0, msj: "" };

      if (tel !== "") {
        var fistNum = input.value[0];

        if (fistNum !== "9") {
          input.value = "";
        } else {
          input.value = tel.replace(/[^0-9.]/g, "");
          datalength = input.value;

          if (cont === datalength.length) {
            msj = { status: 1, msj: "Validado" };
          } else {
            msj = { status: 0, msj: "Ingrese telefono valido" };
          }
        }
      } else {
        msj = { status: 0, msj: "Ingrese telefono" };
      }

      //print validate
      if (msj.status === 1) {
        input.classList.remove("error");
      } else {
        input.classList.add("error");
      }

      return msj;
    }

    validateCheckTerms(form) {
      var isChecked = document.getElementById("checktop").checked,
        divChecked = form.querySelector(".input-mask-top"),
        msj;

      if (!isChecked) {
        divChecked.classList.add("error");
        msj = { status: 0, msj: "Acepte terminos" };
      } else {
        divChecked.classList.remove("error");
        msj = { status: 1, msj: "Acepto" };
      }

      return msj;
    }

    getcurrentIp() {
      let ipObject = { ip: '' };
      const ipRequest = new XMLHttpRequest();
      ipRequest.open('GET', 'https://api.ipify.org/?format=json', false);
      
       try {
          ipRequest.send();

          if (ipRequest.status == 200) {
            ipObject = JSON.parse(ipRequest.responseText);
          }
       } catch (error) {
          console.log(error)
          return ipObject.ip;
       }

      return ipObject.ip;
    }

    sendLeadData(form, codOrigen) {
      var input = form.querySelector(".validonlynumeric"),
        mw_url = "https://middleware-entel.segmentid.pro/api/leads/sync", 
        // mw_url = http://dev.middleware-entel.dtransforma.com/api/leads/sync
        current_url = window.location.origin + window.location.pathname,
        lead_data = {
          ip: this.getcurrentIp(),
          cod_origen: codOrigen,
          telefono: input.value,
          operador: "",
          nombre: "",
          email: "",
          tipo_doc: "DNI",
          documento: "",
          modalidad: "Postpago Migra",
          plan: "Cuota cero",
          producto: "Banner Lineas Adicionales",
          departamento: "",
          fechalead: this.getFecha(),
          faseembudo: "",
          resultado_crediticia: "",
          resultado_portabilidad: "",
          utm_source: $('#utm_source').val(),
          utm_medium: $('#utm_medium').val(),
          utm_campaign: $('#utm_campaign').val(),
          utm_content: $('#utm_content').val(),
          utm_term: $('#utm_term').val(),
          gclid: $('#gclid').val(),
          producto_marca: "",
          producto_cantidad_cuotas: "",
          producto_monto_cuota_inicial: "",
          producto_monto_cuota_mensual: "",
          mercado: "personas",
          politicas_privacidad_datos: 1,
          fecha_programada: "",
          segmento: "",
          score: "",
          url_lead: current_url,
          extra1: "",
          extra2: "",
          extra3: "",
          prioridad: "",
        };

      //Enviar Leads
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        var dataLayer = window.dataLayer || [];
        var responseId = xhr.responseText.slice(6).trim().split(",")[0];

        if (xhr.readyState === 4) {
          switch (xhr.status) {
            // succes
            case 201:
              // datalayer
              dataLayer.push({
                event: "gtm.event",
                eventCategory: "Ecommerce - Envío de formulario",
                eventAction: "Submit",
                eventLabel: "Success",
                dimension1: codOrigen,
                dimension2: responseId,
                product: "banner"
              });

              dataLayer.push({
                dimension1: "",
                dimension2: "",
              });

              // dataLayer.push({
              //   event: "ModalAyuda - Banner Formulario",
              //   Entel: [
              //     {
              //       "data-modalidad": current_url,
              //       "data-gtm-category": "Postpago Migra",
              //     },
              //   ],
              // });

              // messages
              input.value = "";
              $(form).find(".form--loading").hide();
              $(form).find(".form--thanks").show();

              setTimeout(() => {
                $(form).find(".form--thanks").hide();
                $(form).find(".form--body").show();
              }, 3000);
              break;

            // repeated
            case 200:
              dataLayer.push({
                event: "gtm.event",
                eventCategory: "Entel home_Banners",
                eventAction: "Submit",
                eventLabel: "Failure - Duplicated",
              });

              input.value = "";
              $(form).find(".form--loading").hide();
              $(form).find(".form--duplicate").show();

              setTimeout(() => {
                $(form).find(".form--duplicate").hide();
                $(form).find(".form--body").show();
              }, 3000);
              break;

            //error
            default:
              dataLayer.push({
                event: "gtm.event",
                eventCategory: "Entel home_Banners",
                eventAction: "Submit",
                eventLabel: "Failure - Server Error",
              });

              input.value = "";
              $(form).find(".form--loading").hide();
              $(form).find(".form--error").show();

              setTimeout(() => {
                $(form).find(".form--error").hide();
                $(form).find(".form--body").show();
              }, 3000);
              console.log("<b>Lo sentimos Ocurrió un error</b><br>al procesar tus datos.");
              break;
          }
        }
      };

      xhr.open("POST", mw_url);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.setRequestHeader("Accept", "*/*");
      xhr.send(JSON.stringify(lead_data));
    }
    //--------

    formBanners() {
      // const formMigraPower = document.getElementById("banner-form-migra-power");
      const forms = document.querySelectorAll(".banner__form");

      if (forms.length) {
        forms.forEach(function(currentItem, index) {
          new HomeSlider().binEventsFormBanner(currentItem);
        });
      }
      // this.binEventsFormBanner(formMigraPower);
    }

    load() {
      // this.slider()

      // TODO: mejorar form banners
      this.formBanners();
      // this.hiddenform();
      this.designByParameter();
      this.scrollAnimation();
    }
  }

  // load
  new HomeSlider().load();
};
