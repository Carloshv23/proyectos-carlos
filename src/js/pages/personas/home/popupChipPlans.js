export default () => {
    class PopupChipPlans {
      constructor() {}
  
      init() {
        this.setLinks();
  
        // close popup
        $('.popup-planesmovil-overlay').on('click', () => {
          this.closePopup();
        });
  
        $('.popup-planesmovil__close-btn').on('click', () => {
          this.closePopup();
        });
      }

      setLinks() {
        if(!$('.js--show-popup-chip-plans').data('blocked')){
          $('.js--show-popup-chip-plans').data('blocked',true);
          
          $('.js--show-popup-chip-plans').on('click', (e) => {
            e.preventDefault();

            var idPopup = $(e.currentTarget).data('id-popup'),
                $currentPlanPopupEl = $(`.popup-planesmovil[data-id-popup="${idPopup}"]`);

            this.openPopup($currentPlanPopupEl)
          });
        }
      }
  
      openPopup($currentPlanPopupEl) {
        $currentPlanPopupEl.addClass('popup-planesmovil--show');
        $('.popup-planesmovil-overlay').addClass('popup-planesmovil-overlay--show');
        $('#overlay_eheader').addClass('overlay_eheader');
        $('.lq-wrapper__menu').css('z-index', '101');
      }
  
      closePopup() {
        $('.popup-planesmovil').removeClass('popup-planesmovil--show');
        $('.popup-planesmovil-overlay').removeClass('popup-planesmovil-overlay--show');
        $('#overlay_eheader').removeClass('overlay_eheader');
        $('.lq-wrapper__menu').css('z-index', '99');
      }
    }
  
    $(() => {
      // init entel plans
      new PopupChipPlans().init();
    })
  };
  