import "slick-carousel";
import { Tabs } from "./tabs";
import "../../../components/appear";
import popupChipPlans from './popupChipPlans';
export default () => {
  class phonesSection {
    constructor() {
      this.$tabPlanes = document.querySelector("#planesTab");
    }
    tabPlanes() {
      // init tabs
      new Tabs({
        element: this.$tabPlanes,
      }).init();
      //slider load
      // let html, jsonphones;
      let html, jsonphones = [
        {
          "title": "Xiaomi Redmi Note 11",
          "image": "https://www.entel.pe/wp-content/uploads/2022/09/card-xiaomi-note-11.png",
          "plan": "Entel Power+ 74.90",
          "fee" : {
            "initial": "10000",
            "price": "10000",
            "months": "10000"
          },
          "unique_payment": {
            "current": "779",
            "before": ""
          },
          "link": "https://miportal.entel.pe/personas/producto/Equipos/prod180044?poId=PO_BSC_EQP_24648&modalidad=Portabilidad&oferta=especial",
          "best_seller_label": false
        },  
        {
          "title": "Samsung Galaxy A52s 5G",
          "image": "https://www.entel.pe/wp-content/uploads/2022/09/card-samsung-galaxy-a52s.png",
          "plan": "Entel Power+ 74.90",
          "fee" : {
            "initial": "10000",
            "price": "10000",
            "months": "10000"
          },
          "unique_payment": {
            "current": "1479",
            "before": ""
          },
          "link": "https://miportal.entel.pe/personas/producto/Equipos/prod140037?poId=PO_BSC_EQP_23953&modalidad=Portabilidad&oferta=especial",
          "best_seller_label": false
        },
        {
          "title": "Samsung Galaxy A13",
          "image": "https://www.entel.pe/wp-content/uploads/2022/09/card-samsung-a13.png",
          "plan": "Entel Power+ 74.90",
          "fee" : {
            "initial": "10000",
            "price": "10000",
            "months": "10000"
          },
          "unique_payment": {
            "current": "609",
            "before": ""
          },
          "link": "https://miportal.entel.pe/personas/producto/Equipos/prod180037?poId=PO_BSC_EQP_24896&modalidad=Portabilidad&oferta=especial",
          "best_seller_label": false
        },
        {
          "title": "Motorola Moto G22",
          "image": "https://www.entel.pe/wp-content/uploads/2022/09/card-motorola-g22.png",
          "plan": "Entel Power+ 74.90",
          "fee" : {
            "initial": "10000",
            "price": "10000",
            "months": "10000"
          },
          "unique_payment": {
            "current": "579",
            "before": ""
          },
          "link": "https:",
          "best_seller_label": false
        },
        // {
        //   "title": "Samsung Galaxy A52s 5G",
        //   "image": "https://miportal.entel.pe/static/052620221253554/images/Samsung_Galaxy_A52s_Miniaturas.jpg",
        //   "plan": "Entel Power 74.90 ",
        //   "fee" : {
        //     "initial": "0",
        //     "price": "197",
        //     "months": "12"
        //   },
        //   "unique_payment": {
        //     "current": "1489",
        //     "before": "1809"
        //   },
        //   "link": "https://miportal.entel.pe/personas/producto/Equipos/prod140037?poId=PO_BSC_EQP_23953&modalidad=Portabilidad&oferta=especial",
        //   "best_seller_label": false
        // },
        // {
        //   "title": "Motorola Moto G50 5G",
        //   "image": "https://miportal.entel.pe/static/052620221253554/images/Motorola_Moto_G50_5G_Miniaturas.jpg",
        //   "plan": "Entel Power 74.90 ",
        //   "fee" : {
        //     "initial": "0",
        //     "price": "148",
        //     "months": "12"
        //   },
        //   "unique_payment": {
        //     "current": "1119",
        //     "before": "1219"
        //   },
        //   "link": "https://miportal.entel.pe/personas/producto/Equipos/prod180056?poId=PO_BSC_EQP_23827&modalidad=Portabilidad&oferta=especial",
        //   "best_seller_label": false
        // },
        // {
        //   "title": "Xiaomi Redmi Note 10s",
        //   "image": "https://miportal.entel.pe/static/052620221253554/images/Xiaomi_Redmi_Note_10S_Miniatura.jpg",
        //   "plan": "Entel Power 74.90 ",
        //   "fee" : {
        //     "initial": "0",
        //     "price": "124",
        //     "months": "12"
        //   },
        //   "unique_payment": {
        //     "current": "939",
        //     "before": "1259"
        //   },
        //   "link": "https://miportal.entel.pe/personas/producto/Equipos/prod140040?poId=PO_BSC_EQP_23063&modalidad=Portabilidad&oferta=especial",
        //   "best_seller_label": false
        // },
        // {
        //   "title": "Samsung Galaxy A53 5G",
        //   "image": "https://miportal.entel.pe/static/052620221253554/images/SAMSUNG_GALAXY_A53_128GB_5G_MINIATURA2.jpg",
        //   "plan": "Entel Power 74.90 ",
        //   "fee" : {
        //     "initial": "0",
        //     "price": "228",
        //     "months": "12"
        //   },
        //   "unique_payment": {
        //     "current": "1719",
        //     "before": "2059"
        //   },
        //   "link": "https://miportal.entel.pe/personas/producto/Equipos/prod240140?poId=PO_BSC_EQP_24894&modalidad=Portabilidad&oferta=especial",
        //   "best_seller_label": false
        // },
        // {
        //   "title": "Samsung Galaxy A03 Core",
        //   "image": "https://miportal.entel.pe/static/052620221253554/images/SAMSUNG_GALAXY_A03_CORE_MINIATURA.jpg",
        //   "plan": "Entel Power 74.90 ",
        //   "fee" : {
        //     "initial": "0",
        //     "price": "50",
        //     "months": "12"
        //   },
        //   "unique_payment": {
        //     "current": "379",
        //     "before": "459"
        //   },
        //   "link": "https://miportal.entel.pe/personas/producto/Equipos/prod150040?poId=PO_BSC_EQP_24592&modalidad=Portabilidad&oferta=especial",
        //   "best_seller_label": false
        // }
      ];

      // let xobj = new XMLHttpRequest();
      // xobj.open('GET', 'https://catalogo.entel.pe/files/entel_atg_master_pack_landing.json'); // 
      // xobj.setRequestHeader("Content-Type", "application/json");
      // xobj.setRequestHeader("Accept", "*/*");
      // xobj.onreadystatechange = function () {
      //   if (xobj.readyState == 4 && xobj.status == "200") {
      //     jsonphones = JSON.parse(xobj.responseText);
          jsonphones.forEach(obj => {
            let classLabel ="";
            if (obj.label !== "") {
              classLabel = `label-${obj.label}`
            }

            html = `<div class="slider-planesequipos_item" style="width: 100%; display: inline-block;">
              <div class="equipo-title">${obj.title}</div>
              <div class="equipo-img ${classLabel}"><img src="${obj.image}" alt="${obj.title}"></div>
              <div class="equipo-plan">${obj.plan}</div>
              <div class="equipo-tipos">
                <div class="equipo-pagounico"> <span>En pago único desde</span><span>S/ ${obj.unique_payment.current}</span></div>
              </div><a class="equipo-link" href="${obj.link}" tabindex="0">Elige plan y pago</a>
              <div class="equipo-disclaimer">Sujeto a evaluación crediticia</div>
            </div>`;

            $(".slider-planesequipos").append(html);

          });
      //   }
      // };
      // xobj.send(null); 

      

      $(".slider-planesmovil").slick({
        arrows: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        initialSlide: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              infinite: true,
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 4,
            },
          },
        ],
      });

      $(".slider-planesmovil").on('beforeChange', function(event, slick, currentSlide, nextSlide){
        popupChipPlans()
      });

      //Slider de planes test A/B
      $(".slider-planesmovil-tstng").slick({
        arrows: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        initialSlide: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              infinite: true,
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 2,
            },
          },
        ],
      });

      $(".slider-planesequipos").slick({
        arrows: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              infinite: true,
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
        ],
      });
    }

    loadSliderBrands() {
      $(".e-brands__slider").slick({
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        mobileFirst: true,
        centerMode: false,
        pauseOnFocus: false,
        pauseOnHover: false,
        responsive: [
          {
            breakpoint: 579,
            settings: {
              infinite: true,
              slidesToShow: 2,
              slidesToScroll: 2,
              centerMode: false,
              arrows: true,
            },
          },
          {
            breakpoint: 767,
            settings: {
              infinite: true,
              slidesToShow: 3,
              slidesToScroll: 3,
              centerMode: false,
              arrows: true,
            },
          },
          {
            breakpoint: 1023,
            settings: {
              infinite: true,
              slidesToShow: 7,
              slidesToScroll: 7,
              centerMode: false,
              arrows: true,
            },
          },
        ],
      });

    }

    loadLinkMore() {
      let linkMore = document.querySelectorAll(".e-link__more"),
      currentTarget,
      nextTarget,
      actualTarget;

      if(linkMore.length > 0){
        linkMore.forEach((current) => {
          current.addEventListener("click", (item,i) => {
            currentTarget = item.currentTarget;
            nextTarget = (currentTarget.nextElementSibling) ? currentTarget.nextElementSibling : false;
            actualTarget = document.querySelector(".e-link__more-active");

            if(actualTarget && actualTarget != currentTarget) {
              actualTarget.nextElementSibling.classList.remove('e-box-plan__extra-show');
              actualTarget.classList.remove('e-link__more-active');
            }

            if(!nextTarget.classList.contains('e-box-plan__extra-show')) {
              nextTarget.classList.add('e-box-plan__extra-show');
              currentTarget.classList.add('e-link__more-active');
            } else {
              nextTarget.classList.remove('e-box-plan__extra-show');
              currentTarget.classList.remove('e-link__more-active');
            }

          });
        });
      };
    }

    loadPopupCountry () {
      let linkCountry = document.querySelectorAll('.e-link-country'),
      popupCountry = document.querySelector('.popup-country'),
      popupCountryBack = document.querySelector('.popup-country-back'),
      btnClose = document.querySelector('.popup-country__close-btn');
      
      if(linkCountry.length > 0){
        linkCountry.forEach((current) => {
          current.addEventListener("click", (item,i) => {
            popupCountry.classList.add('popup-country--show');
            popupCountryBack.classList.add('popup-country-back--show');

          });
        });
      };

      if(btnClose){
        btnClose.addEventListener("click", (item,i) => {
          popupCountry.classList.remove('popup-country--show');
          popupCountryBack.classList.remove('popup-country-back--show');
        });
      }


    }

    dataLayer() {

        var name_plan = function (index) {
            var plan = ''

            if (index < 55) {
                plan = 'Entel Chip ' + index + '.90 Plus'
            } else if (index == 159) {
                plan = 'Entel Chip ' + index + '.90 Plus'
            } else if (index > 109) {
                plan = 'Entel Chip ' + index + '.90'
            } else {
                plan = 'Entel Power ' + index + '.90'
            }

            return plan
        }

        document.querySelectorAll(".e-box-plan .e-button").forEach(function(element) {
            element.addEventListener("click", function() {
                var data_id = element.getAttribute('data-id-popup'),
                    dataLayer = window.dataLayer || []

                dataLayer.push({
                    'event': 'gtm.event',
                    'eventCategory': 'Parrillas Planes Home',
                    'eventAction': 'Click',
                    'eventLabel': 'Plan ' + name_plan(data_id)
                });
            })
        })
        
        //   document.querySelector('.m-modal--persons .m-modal__container .e-access__slider .dscto-migra20 .entel-access__item').addEventListener("click", function() {
        //     var dataLayer = window.dataLayer || []

        //     dataLayer.push({
        //         'event': 'gtm.event',
        //         'eventCategory': 'Botonera - Migra - Solo Chip',
        //         'eventAction': 'Click',
        //         'eventLabel': 'Solo Chip'
        //     });
        // })
        
        
    }

    init() {
      this.tabPlanes();
      this.loadSliderBrands();
      this.loadLinkMore();
      this.dataLayer();
      this.loadPopupCountry();
    }
  }
  new phonesSection().init();
};
