import 'slick-carousel'
import '../../../components/appear'

export default () => {
    class services {
        constructor() {
            this.$sliderServices = $('#slider-services')
            this.$services = document.querySelector('.entel-services__flex')
            this.$servicesItems = document.querySelectorAll('.e-services__slider__slide a')
            this.$servicePicture = document.querySelector('.entel-services__flex-right picture img')
        }

        animation() {
            $('.entel-services__flex-right').appear(() => {
                setTimeout(() => {
                    // active pictures
                    this.$servicePicture.classList.add('js--active')

                    // actives items
                    this.activeItems()
                }, 300)
            })
        }

        activeItems() {
            let
                timeOut = 0

            // iterating pictures
            this.$servicesItems.forEach(($item) => {
                $item.classList.add('js--active')

                timeOut = timeOut + 500

                setTimeout(() => {
                    $item.classList.remove('js--active')
                }, timeOut)
            })
        }

        resize() {
            // let resizeFunction,
            //     media = window.matchMedia(`(min-width: 768px)`)

            // // setting resize function
            // resizeFunction = (media) => {
            //     // validating match
            //     if (!media.matches) {
            //         if (!this.$sliderServices.hasClass('slick-initialized')) {
                        
            //         }
            //     } else {
            //         if (this.$sliderServices.hasClass('slick-initialized')) {
            //             this.$sliderServices.slick('unslick')
            //         }
            //     }
            // }

            // // init resize function
            // resizeFunction(media)
            // media.addListener(resizeFunction)
            this.$sliderServices.slick({
                dots: true,
                arrows: false,
                slidesToShow: 3,
                infinite: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            dots: true,
                            arrows: false,
                            slidesToShow: 1,
                            infinite: false
                        }
                    }
                ]
            })
        }

        init() {
            if (this.$services) {
                this.resize()

                // load animation
                document.addEventListener('DOMContentLoaded', () => {
                    this.animation()
                })
            }
        }
    }

    new services().init()
}