export default () => {
  class datalayer {
    slider() {
      window.dataLayer = window.dataLayer || [];

      let cont = 1;

      dataLayer.push({
        event: "promotionView",
        ecommerce: {
          promoView: {
            promotions: [
              {
                id: "Banner_Home_01",
                name: $("#slider-entel")
                  .find(
                    ".slick-slide.slick-current.slick-active .entel__slider--home__item"
                  )
                  .data("layer-name"),
                position: "Home - Banner Superior - 1",
                creative: $("#slider-entel")
                  .find(
                    ".slick-slide.slick-current.slick-active .entel__slider--home__item a"
                  )
                  .attr("href")
                  ? $("#slider-entel")
                      .find(
                        ".slick-slide.slick-current.slick-active .entel__slider--home__item a"
                      )
                      .attr("href")
                  : "not available",
              },
            ],
          },
        },
      });

      $("#slider-entel").on(
        "afterChange",
        function (event, slick, currentSlide, nextSlide) {
          let positionname = $(this)
            .find(
              ".slick-slide.slick-current.slick-active .entel__slider--home__item"
            )
            .data("layer-name");
          let valcreative = $(this)
            .find(
              ".slick-slide.slick-current.slick-active .entel__slider--home__item a"
            )
            .attr("href");
          if (!valcreative) {
            valcreative = "not available";
          }

          if (cont < $(this).slick("getSlick").slideCount) {
            dataLayer.push({
              event: "promotionView",
              ecommerce: {
                promoView: {
                  promotions: [
                    {
                      id: "Banner_Home_0" + (currentSlide + 1),
                      name: positionname,
                      position:
                        "Home - Banner Superior - " + (currentSlide + 1),
                      creative: valcreative,
                    },
                  ],
                },
              },
            });

            // console.log($(this).slick("getSlick").slideCount);

            cont++;
            // console.log(cont);
          }
        }
      );
      $("#slider-entel .entel__slider--home__item a").click(function () {
        let currentSlide = $("#slider-entel").slick("getSlick").currentSlide;
        let positionname = $(this)
          .closest(
            ".slick-slide.slick-current.slick-active .entel__slider--home__item"
          )
          .data("layer-name");
        let valcreative = $(this).attr("href");
        if (!valcreative) {
          valcreative = "not available";
          // console.log(!valcreative);
        }
        // dataLayer.push({
        //   event: "promotionClick",
        //   ecommerce: {
        //     promoView: {
        //       promotions: [
        //         {
        //           id: "Banner_Home_0" + (currentSlide + 1),
        //           name: positionname,
        //           position: "Home - Banner Superior - " + (currentSlide + 1),
        //           creative: valcreative,
        //         },
        //       ],
        //     },
        //   },
        // });

        dataLayer.push({
          event: "virtualEvent",
          category: "Entel home_Banners",
          action: "Accion en banner ",
          label: positionname + (currentSlide + 1),
          id: positionname + (currentSlide + 1),
          name: positionname + (currentSlide + 1)
        });
      });

      $(".btn-download-app-mientel").click(function () {
        dataLayer.push({
          event: "promotionClick",
          ecommerce: {
            promoView: {
              promotions: [
                {
                  id: "Banner_consulta_tu_saldo_tu_Recibo_recarga_o_accede_a_ofertas_y_promociones_desde_la_App_Mi_Entel",
                  name: "Descarga la App Mi Entel",
                  position: "Descarga la App Mi Entel- Banner Superior - 1",
                  creative: "https://pemientel.page.link/bannerwebapp",
                },
              ],
            },
          },
        });
      });

      // $(".e-container-flex__item_btn-honor-x7a").on('click', (e) => {
      //   dataLayer.push({
      //     event: "promotionClick",
      //     ecommerce: {
      //       promoView: {
      //         promotions: [
      //           {
      //             id: "​Banner_lanzamiento_honor_x7a",
      //             name: "Lanzamiento Honor X7a",
      //             position: "Lo quiero -Banner Superior - 1",
      //             creative: "​https://miportal.entel.pe/personas/catalogo/postpago/renovacion?marca=honor#productos",
      //           },
      //         ],
      //       },
      //     },
      //   });
      // });

      

      $('.js--show-popup-plans').on('click', (e) => {
          var dataLayer = window.dataLayer || [];
              dataLayer.push({
              'event': 'gtm.event',
              'eventCategory': 'ATG Pop-up',
              'eventAction': 'Show',
              'eventLabel': 'Pop-up'
          });
      })

      $('.popup-plans-overlay').on('click', function () {
          var dataLayer = window.dataLayer || [];
          dataLayer.push({
              'event': 'gtm.event',
              'eventCategory': 'ATG Pop-up',
              'eventAction': 'Click',
              'eventLabel': 'Cerrar'
          });
      });

      $('.popup-plans__close-btn > span').on('click', function () {
          var dataLayer = window.dataLayer || [];
              dataLayer.push({
              'event': 'gtm.event',
              'eventCategory': 'ATG Pop-up',
              'eventAction': 'Click',
              'eventLabel': 'Cerrar'
          });
      });

      $('.popup-plans__content__button--fill').on('click', function () {
          var dataLayer = window.dataLayer || [];
              dataLayer.push({
              'event': 'gtm.event',
              'eventCategory': 'ATG Pop-up',
              'eventAction': 'Click',
              'eventLabel': 'Comprar en la web'
          });
      })
      
      $('.popup-plans__content__button--default').on('click', function () {
          var dataLayer = window.dataLayer || [];
              dataLayer.push({
              'event': 'gtm.event',
              'eventCategory': 'ATG Pop-up',
              'eventAction': 'Click',
              'eventLabel': 'Comprar con un asesor'
          });
      })
    }
    entelAccess() {
      // start add data-gtm from old repo
      $(".entel-access .e-access__slider .entel-access__slider__card a").each(
        function () {
          $(this).addClass("entel-data-layer-link");
          $(this).attr("data-gtm-category", "Home Entel");
          $(this).attr("data-gtm-action", "Seleccionar boton");
          $(this).attr(
            "data-gtm-label",
            "¿Qué hacemos hoy? - " + $(this).find("span").text()
          );
        }
      );
      // end add data-gtm from old repo
      $(".entel-access .e-access__slider .entel-access__slider__card a").click(
        function () {
          $(this).find("span > br").replaceWith("&nbsp;").text();
          let btnselected = $(this).find("span").text();

          dataLayer.push({
            event: "virtualEvent",
            category: "Home Entel",
            action: "Seleccionar boton",
            label: `¿Qué hacemos hoy? - ${btnselected}`,
          });
        }
      );

      // TODO: 816
      $('#modal_migra .entel-access__slider__card .entel-access__item').on('click', function() {
          let btnselected = $(this).find("span").text();
          
          if (btnselected == "50% DtoSolo chip") {
            dataLayer.push({
              'event': 'gtm.event',
              'eventCategory': 'Botonera - Migra - Solo Chip',
              'eventAction': 'Click',
              'eventLabel': 'Solo Chip'
            });
          }
        }
      );
    }
    migraEntel() {
      // start add data-gtm from old repo
      $("#planesTab > nav ul").each(function (index) {
        $(this).attr("data-layer-section", "Equipos");
        // console.log($(this).data());
      });
      $("#planesTab > nav ul li").each(function (index) {
        if (index == 0) {
          $(this).attr("data-tab", "chips");
          $(this).attr("data-gtm-category", "Planes Chip");
          $(this).attr("data-gtm-action", "Click");
          $(this).attr("data-gtm-type", "click");
          $(this).attr("data-gtm-label", "Botón - Planes Chips - Home");
        }
        if (index == 1) {
          $(this).attr("data-tab", "migra");
          $(this).attr("data-gtm-category", "Planes Migra");
          $(this).attr("data-gtm-action", "Click");
          $(this).attr("data-gtm-type", "click");
          $(this).attr("data-gtm-label", "Botón - Planes Chips - Home");
        }
        // console.log(index);
      });
      // end add data-gtm from old repo
      $("#planesTab > nav ul li a").click(function () {
        let btnselected = $(this).text();
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Visualizar contenido – Tabs informativos",
          label: `Migra a Entel - ${btnselected}`,
        });

        // console.log($(this).text());
      });
      $(".slider-planesmovil .slider-planesmovil_item .text4 a").click(
        function () {
          let btnselected = $(this).text();
          let itemselected = $(this)
            .closest(".item-box")
            .find(".text1 .izq > span:nth-child(1)")
            .text();
          dataLayer.push({
            event: "virtualEvent",
            category: "Home Entel",
            action: "Seleccionar boton",
            label: `Migra a Entel - ${itemselected} - ${btnselected}`,
          });

          // console.log($(this).closest(".item-box").find(".text1 .izq > span:nth-child(1)").text());
        }
      );
      $(".slider-planesequipos .slider-planesmovil_item .text4 a").click(
        function () {
          let btnselected = $(this).text();
          let marca = $(this)
            .closest(".item-box")
            .find(".text1 .der > span:nth-child(1)")
            .text();
          let modelo = $(this)
            .closest(".item-box")
            .find(".text1 .der > span:nth-child(2)")
            .text();
          dataLayer.push({
            event: "virtualEvent",
            category: "Home Entel",
            action: "Seleccionar boton",
            label: `Migra a Entel - ${marca} - ${modelo} - ${btnselected}`,
          });

          // console.log($(this).closest(".item-box").find(".text1 .izq > span:nth-child(1)").text());
        }
      );
      $("#tab-planesmoviles > p a").click(function () {
        let btnselected = $(this).text();

        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Seleccionar boton",
          label: `Migra a Entel - Términos y condiciones - ${btnselected}`,
        });

        // console.log($(this).text());
      });

      $(".e-brands__slider .e-brand__box a").click(function () {
        let btnselected = $(this).find("img").attr("alt");
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Seleccionar boton",
          label: `Marca equipo - ${btnselected}`,
        });
        //   console.log($(this).find("img").attr("alt"));
      });
      $(".e-brands__slider .slick-arrow").click(function () {
        // let btnselected = $(this).find("img").attr("alt");
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Visualizar contenido – Flechas de desplazamiento",
          label: `Marca equipo`,
        });
        // console.log($(this));
      });
    }
    mundoEntel() {
      $("#entel-world-tabs nav ul li a").click(function () {
        let btnselected = $(this).find("span").text();
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Visualizar contenido – Tabs informativos",
          label: `Mundo Entel - ${btnselected}`,
        });

        // console.log($(this).find("span").text());
      });

      $("#entel-world-tabs .e-tabs__content a").click(function () {
        let planselected = $(this)
          .closest("#entel-world-tabs")
          .find("nav ul li a.js--active span")
          .text();
        let btnselected = $(this).find("span").text();
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Seleccionar boton",
          label: `Mundo Entel - ${planselected} - ${btnselected}`,
        });
        // console.log(
        //   $(this)
        //     .closest("#entel-world-tabs")
        //     .find("nav ul li a.js--active span")
        //     .text()
        // );
      });
    }
    beneficiosExclusivos() {
      // start add data-gtm from old repo
      // $(".e-benefits .slick-slide .e-benefits__slider__card_for_txt a").each(function (index) {
      //   let btnselected = $(this)[0].pathname.replace(/\//g, " ");
      //   switch ($(this)[0].pathname) {
      //     case "/app/":
      //       btnselected = "Nuestra nueva App"
      //       break
      //     case "/promociones/":
      //       btnselected = "Promociones";
      //       break;
      //     case "/entelday/":
      //       btnselected = "Entel Day";
      //       break;
      //     case "/chippower/":
      //         btnselected = "Chip Power";
      //         break;
      //     case "/mientel/":
      //         btnselected = "Nuestra nueva Web";
      //         break;

      //   }
      //   $(this).attr("data-gtm-category", "Home Entel");
      //   $(this).attr("data-gtm-action", "Seleccionar boton");
      //   $(this).attr(
      //     "data-gtm-label",
      //     `Beneficios exclusivos -  ${btnselected}`
      //   );
      //   // console.log($(this).data());
      // });
      // $("#slider-benefits-nav .slick-slide").click(function () {
        
      //   let btnselected = $(this).find("p").text();
      //   dataLayer.push({
      //     event: "virtualEvent",
      //     category: "Home Entel",
      //     action: "Visualizar contenido – Tabs informativos",
      //     label: `Beneficios exclusivos - ${btnselected}`,
      //   });
        // console.log(
        //   $(this)
        //     .closest("#entel-world-tabs")
        //     .find("nav ul li a.js--active span")
        //     .text()
        // );
        // console.log(btnselected);
      // });
      // end add data-gtm from old repo
      //Data Layer Beneficios
      $("#slider-benefits-for a.e-benefits_card_for").click(
        function () {
          let btnselected = $(this)[0].pathname.replace(/\//g, " ");

          switch ($(this)[0].pathname) {
            case "/promociones/":
              btnselected = "Promos";
              break;
            case "/entelday/":
              btnselected = "Entel Day";
              break;
            case "/juevesentel/":
              btnselected = "Jueves Entel";
              break;
            case "/freestyle/":
              btnselected = "FMS Peru 2021";
              break;
            case "/bembos/":
              btnselected = "Bembos";
              break;
            case "/lovers/":
              btnselected = "Entel Lovers";
              break;
            case "/smart-doctor/":
              btnselected = "Smart Doctor";
              break;
            case "/clientes/":
              btnselected = "Emprendedores Entel";
              break;
            case "/promociones/":
              btnselected = "Promociones";
              break;
          }

          dataLayer.push({
            event: "virtualEvent",
            category: "Home Entel",
            action: "Seleccionar boton",
            label: `Beneficios exclusivos -  ${btnselected}`,
          });
        }
      );
    }

    //Seccion Canales Autoservicios
    canalesAutoservicio() {
      $(".entel-autoservice .e-auto-channel_item a").click(
        function () {
          let btnselected = $(this)[0].pathname.replace(/\//g, " ");
          switch ($(this)[0].pathname) {
            case "/App/":
              btnselected = "App Mi Entel";
              break;
            case "/Login/":
              btnselected = "Web Mi Entel";
              break;
          }
          dataLayer.push({
            event: "virtualEvent",
            category: "Home Entel",
            action: "Seleccionar boton",
            label: ` ${btnselected} - Conoce más`,
          });
        }
      );

    }
    muchoMas() {
      // start add data-gtm from old repo
      $("#slider-services .e-services__slider__slide a").each(function (index) {
        let btnselected = $(this).find("h4,p").text();

        $(this).attr("data-gtm-category", "Home Entel");
        $(this).attr("data-gtm-action", "Seleccionar boton");
        $(this).attr("data-gtm-label", `Mucho más -  ${btnselected}`);
        // console.log($(this).data());
      });

      // end add data-gtm from old repo
      $("#slider-services .e-services__slider__slide a").click(function () {
        let btnselected = $(this).find("h4,p").text();
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Seleccionar boton",
          label: `Mucho más -  ${btnselected}`,
        });

        // console.log($(this).find("h4,p").text());
      });
    }
    contactanos() {
      $(".entel-contact .entel-contact-flex div p.e-title a").click(
        function () {
          let btnselected = $(this).text();
          dataLayer.push({
            event: "virtualEvent",
            category: "Home Entel",
            action: "Seleccionar boton",
            label: `Contactanos -  ${btnselected}`,
          });

          // console.log($(this).text());
        }
      );
      $(".entel-contact .entel-contact-redes a").click(function () {
        let btnselected = $.trim($(this).text());
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Seleccionar boton",
          label: `Contactanos -  ${btnselected}`,
        });

        // console.log($.trim($(this).text()));
      });
      $(".entel-contact .entel-contact-cobertura a").click(function () {
        let btnselected = $(this).text();
        dataLayer.push({
          event: "virtualEvent",
          category: "Home Entel",
          action: "Seleccionar boton",
          label: `Contactanos -  ${btnselected}`,
        });

        // console.log($(this).text());
      });
    }
    dataLayer() {
      var portalDataLayerPush = {
        dataVirtualEvent: function(category, action, label) {
            // console.log('category=' + category + ' - action=' + action + ' - label=' + label);
            dataLayer.push ({
                'event': 'virtualEvent',
                'category': category,
                'action': action,
                'label': label
            });
        },
      };
      $(".entel-data-layer-btn").click(function () {
        
        let $this = $(this),
          datalayer_category = $this.data("gtm-category"),
          datalayer_action = $this.data("gtm-action"),
          datalayer_label = $this.data("gtm-label");

        dataLayer.push({
          event: "virtualEvent",
          category: datalayer_category,
          action: datalayer_action,
          label: datalayer_label,
          id: datalayer_label,
          name: datalayer_label,
          label: datalayer_label,
        });
      });
      $(".entel-data-layer-link").on("click", function () {
        var $this = $(this),
          datalayer_category = $this.data("gtm-category"),
          datalayer_action = $this.data("gtm-action"),
          datalayer_label = $this.data("gtm-label");
          portalDataLayerPush.dataVirtualEvent(
          datalayer_category,
          datalayer_action,
          datalayer_label
          );
      });
      $(".e-headband-eligetuchip2 .e-link__button").on("click", function () {
        dataLayer.push({
          'event': 'gtm.event',
          'eventCategory': 'Cintillo ATG - Home',
          'eventAction': 'Click',
          'eventLabel': 'Cintillo'
        })
      });
    }
    init() {
      this.slider();
      this.entelAccess();
      this.migraEntel();
      this.mundoEntel();
      this.beneficiosExclusivos();
      this.canalesAutoservicio();
      this.muchoMas();
      this.contactanos();
      this.dataLayer();
    }
  }
  new datalayer().init();
};
