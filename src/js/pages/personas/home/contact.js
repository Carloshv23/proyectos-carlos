import '../../../components/appear'

export default () => {
    class contacts {
        constructor() {
            this.$contact = document.querySelector('.entel-contact'),
            this.$contactContent = document.querySelectorAll('.entel-contact-flex div')
        }

        animation() {
            $('.entel-contact-flex').appear(() => {
                this.activePictures(this.$contactContent)
            })
        }

        activePictures($currentPictures) {
            setTimeout(() => {
                // iterating pictures
                $currentPictures.forEach(($picture) => {
                    $picture.classList.add('js--active')
                })
            }, 300)
        }

        init() {
            if (this.$contact) {
                //
                document.addEventListener('DOMContentLoaded', () => {
                    this.animation()
                })
            }
        }
    }

    // init resize function
    new contacts().init()
}