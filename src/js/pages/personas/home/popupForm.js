/* ----- DEV Imports ----- */
// import './scss/entel_bannerform.scss'

let global = (function() {

    let self,form,overlay,close,popup,buttons,body,loading,duplicate,error,thanks,buttonMigra,orderbanner,codOrigenLink;
    
    // utm_source='',
    // utm_medium='',
    // utm_campaign='',
    // utm_content='',
    // utm_term='',
    // gclid='';

    // append html
    // let html_obj = {
    //     append: function() {
    //         let htmlString = '<div class="bannerform-overlay"></div><div class="bannerform-popup"><a class="bannerform-popup__close" href="javascript:void(0)"></a><div class="bannerform-popup__buttons active"><h5>Elige una opción</h5><a id="buttonMigra" class="ico-celapp" href="#"><strong>Migra a Entel</strong></a> <a class="ico-cel" href="tel:080009001"><strong>Renueva con Entel</strong><span>08000 9001 ( MARCA opción 2 )</span></a> <a class="ico-user" href="tel:080009000"><strong>Atención al cliente</strong><span>0800 09000</span></a></div><div class="bannerform-popup__forms"><form class="bannerform-popup__form" data-codorigen="C2C_Banner"><div class="form--content"><div class="form--body"><h3>Déjanos tu número para llamarte:</h3><div class="form--number"><input class="bannerFormNum" type="tel" maxlength="9" placeholder=" " name="form_lead_phone" autocomplete="on" tabindex="0"> <span><span>Número de celular</span></span></div><div class="form--check"><span>Al hacer clic en “Llámenme” declaras haber leído la<a href="http://www.entel.pe/politica-de-proteccion-de-datos-personales/" target="_blank" rel="noopener noreferrer" tabindex="-1"> <span>Política de Privacidad</span></a>de Entel</span> <input id="checktop" type="checkbox" value="1" name="form_lead_policy" checked="checked" tabindex="-1"><div class="input-mask-top"></div></div><button class="form-btn" id="form-btn" type="submit" data-origin="catalogo" data-gtm-action="Accion en banner " data-gtm-category="Entel home_Banners" data-gtm-label="" tabindex="-1">Llámenme</button></div><div class="form--loading"><img class="no-lazy" srcset="https://www.entel.pe/wp-content/uploads/2020/02/loading-c2c-fixd.png" alt="loading"></div><div class="form--duplicate"><p>¡Hola! Ya contamos con tu número.</p><p>Estate atento a nuestra pronta llamada.</p></div><div class="form--error"><p>Lo sentimos Ocurrió un error</p><p>al procesar tus datos.</p></div><div class="form--thanks"><img srcset="https://www.entel.pe/wp-content/uploads/2022/06/ico-gracias-white.png" alt="Entel"><p><br><strong>En breve te llamaremos!</strong><br>Gracias por preferir Entel.</p></div></div></form></div></div>'

    //         document.querySelector('body').insertAdjacentHTML('beforeend', htmlString)
    //     }
    // }

    let formpopup = {
        init : function() {
            self = this;
            
            close = document.querySelector(".bannerform-popup__close");
            overlay = document.querySelector(".bannerform-overlay");
            popup = document.querySelector(".bannerform-popup");
            form = document.querySelector(".bannerform-popup__form");
            buttons = document.querySelectorAll(".bannerform-call");
            // buttonMigra = document.getElementById("buttonMigra");
            
            popup.querySelectorAll(".form--number").forEach(function(currentItem, index) {
              currentItem.addEventListener("click", (e) => {
                e.currentTarget.querySelector("input").focus();
              });
            });

            if (buttons.length) {
              // let numord = 1;
              buttons.forEach(function(currentItem, index) {
                currentItem.dataset.bannerorder = (index+1);
                currentItem.addEventListener("click", (e) => {
                  e.preventDefault();
                  orderbanner = e.currentTarget.dataset.bannerorder ? e.currentTarget.dataset.bannerorder : '';
                  codOrigenLink = e.currentTarget.dataset.codorigen ? e.currentTarget.dataset.codorigen : 'C2C_Banner';
                  document.querySelector('.bannerform-popup__form').dataset.codorigen = codOrigenLink;

                  self.resetPopup();
                  self.openPopup();
                });
                // numord++;
              });

              close.addEventListener("click", () => {
                  self.closePopup();
              });

              // buttonMigra.addEventListener("click", (e) => {
              //     e.preventDefault();
              //     popup.style.height = "297px";
              //     popup.querySelector(".bannerform-popup__buttons").classList.remove("active");
              //     popup.querySelector(".bannerform-popup__forms").classList.add("active");
              // })

              this.hiddenform();
              this.binEventsFormBanner(form);
            }
        },
        getParameterURL : function (parameter, stringUrl) {
            let pageUrl = (stringUrl) ? stringUrl : decodeURIComponent(window.location.search.substring(1)),
                parameters = pageUrl.split('&'),
                parameterName,
                i
        
            for (i = 0; i < parameters.length; i++) {
                parameterName = parameters[i].split('=')
        
                if (parameterName[0] === parameter) {
                    return parameterName[1] === undefined ? true : parameterName[1]
                }
            }
        },
        get : function (atribute) {
        
            // get values form utmz input
            const utm_source = this.getParameterURL('utm_source')
            const utm_medium = this.getParameterURL('utm_medium')
            const utm_campaign = this.getParameterURL('utm_campaign')
            const utm_content = this.getParameterURL('utm_content')
            const utm_term = this.getParameterURL('utm_term')
            const gclid = this.getParameterURL('gclid')
        
            const utm_object = {
                utm_source: utm_source ? utm_source : '-',
                utm_medium: utm_medium ? utm_medium : '-',
                utm_campaign: utm_campaign ? utm_campaign : '-',
                utm_content: utm_content ? utm_content : '-',
                utm_term: utm_term ? utm_term : '-',
                gclid: gclid ? gclid : '-'
            }
            return utm_object[atribute]
        },
        getCodOrigen : function(modality) {

          var origin = '',
              origin_c2c_general = 'C2C_General',
              origin_c2c_ecommerce = 'C2C_Banner',
              origin_c2c_contado = 'C2C_Web_Banner_Contado',
              origin_reno_c2c_general = 'Renovacion_C2C_General',
              origin_reno_c2c_ecommerce = 'Renovacion_C2C_Ecommerce',
              origin_hogar_c2c_general = 'Hogar_C2C_General'
      
          switch (modality) {
              
              case 'migra':
                  switch (true) {
                      default:
                          if (this.get('utm_source').toLowerCase() === 'google' && this.get('utm_medium').toLowerCase() === 'cpc_search' && (this.get('utm_campaign').toLowerCase().startsWith('atg') || this.get('utm_campaign').toLowerCase().startsWith('tlv'))) {
                              origin = 'GoogleSEM_General'
                          } else if (this.get('utm_source').toLowerCase() === 'facebook' && this.get('utm_medium').toLowerCase() === 'social_paid_perf') {
                              origin = 'Facebook_Performance'
                          } else {
                              origin = origin_c2c_ecommerce
                          }   
                  }
                  break
      
              case 'reno':
                  if (this.get('utm_source').toLowerCase() === 'google' && this.get('utm_medium').toLowerCase() === 'cpc_search' && (this.get('utm_campaign').toLowerCase().startsWith('atg') || this.get('utm_campaign').toLowerCase().startsWith('tlv'))) {
                      origin = 'Renovacion_GoogleSEM_OyP'
                  } else if (this.get('utm_source').toLowerCase() === 'facebook' && this.get('utm_medium').toLowerCase() === 'social_paid_perf') {
                      origin = 'Renovacion_Facebook_Lima'
                  } else {
                      origin = origin_reno_c2c_ecommerce
                  }
                  break
          }
      
          return origin
        },
        hiddenform : function(){
            // Set vars
            let now = new Date(),
            later = new Date(now),
            early = new Date(now),
            msn = '<br><strong>En breve te llamaremos!</strong><br> Gracias por preferir Entel.',
            int_ban,
            c2c_ban = document.querySelectorAll(".form--content .form--thanks p");
      
            later.setHours(22);
            later.setMinutes(0);
            later.setSeconds(0);
      
            early.setHours(8);
            early.setMinutes(0);
            early.setSeconds(0);
      
            //validar si se encuentra fuera del rango
            if(now < early){
              msn = '<strong>Gracias!</strong> nos<br> comunicaremos contigo <small>desde las 8:00 am</small>';
            } else if(now > later){
              msn = '<strong>Gracias!</strong> nos<br> comunicaremos contigo <small>mañana desde las 8:00 am</small>';
            }
      
            // if(!(now > early && now < later)){
              int_ban = setInterval(function () {
                if (c2c_ban.length) {
                  clearInterval(int_ban);
                  c2c_ban.forEach(function (el) {
                    el.innerHTML = msn;
                  });
                }
              }, 100);
            // }
        },
        resetPopup : function(form) {
          // popup.style.height = "347px";
            // height: 387px;
            // popup.style.height = "387px";
            
            popup.querySelector(".bannerform-popup__forms").classList.add("active");
            // popup.querySelector(".bannerform-popup__buttons").classList.add("active");
            // popup.querySelector(".bannerform-popup__forms").classList.remove("active");
        },
        openPopup : function() {
            overlay.classList.add("active");
            popup.classList.add("active");
        },
        closePopup : function() {
            overlay.classList.remove("active");
            popup.classList.remove("active");
        },
        binEventsFormBanner : function(form) {
            if (form) {
                body = form.querySelector(".form--body");;
                loading = form.querySelector(".form--loading");
                duplicate = form.querySelector(".form--duplicate");
                error = form.querySelector(".form--error");
                thanks = form.querySelector(".form--thanks");

                form.querySelector(".bannerFormNum").addEventListener("input", (e) => {
                    this.validateInputPhoneNumber(e.currentTarget);
                })

                form.querySelector(".bannerFormDni").addEventListener("input", (e) => {
                    this.validateInputDni(e.currentTarget);
                })
                
        
                let handleSubmit = (e) => {
                    e.preventDefault();
                    
                    let isChecked = this.validateCheckTerms(form);
                    let isNumero = this.validateInputPhoneNumber(form.querySelector(".bannerFormNum"));
                    let isDni = this.validateInputDni(form.querySelector(".bannerFormDni"));
                    let modality = form.dataset.modality;
        
                    if (isDni.status === 1 && isNumero.status === 1 && isChecked.status === 1) {
                        body.style.display = 'none';
                        loading.style.display = 'block';
            
                        // const codOrigen = form.dataset.codorigen;
                        let numBanner = orderbanner;
                        
                        let origin = form.dataset.codorigen == "C2C_Web_Banner_Contado" ? form.dataset.codorigen : this.getCodOrigen(modality);

                        // this.sendLeadData(form, codOrigen, numBanner)
                        this.sendLeadData(form, numBanner, origin, modality)
                        // sendLead(formId, bannerOrder, origin, modality)
            
                    //     // play slider
                    //     // $(this.navSlider).slick('slickSetOption', 'autoplay', true).slick('slickPlay');
                    //     // $(this.homeSlider).slick('slickSetOption', 'autoplay', true).slick('slickPlay');
                        
                    //     // next slider
                    //     // setTimeout(() => {
                    //     //     $(this.homeSlider).slick('slickNext');
                    //     // }, 2000)
                    }
                }
                
                form.addEventListener("submit", handleSubmit);
            }
        },
        validateInputPhoneNumber(input) {
            // var input = form.querySelector(".bannerFormNum"),
            var tel = input.value,
            cont = 9,
            datalength,
            msj = { status: 0, msj: "" };
      
            if (tel !== "") {
                var fistNum = input.value[0];
        
                if (fistNum !== "9") {
                    input.value = "";
                } else {
                    input.value = tel.replace(/[^0-9.]/g, "");
                    datalength = input.value;
        
                    if (cont === datalength.length) {
                        msj = { status: 1, msj: "Validado" };
                    } else {
                        msj = { status: 0, msj: "Ingrese telefono valido" };
                    }
                }
            } else {
                msj = { status: 0, msj: "Ingrese telefono" };
            }
      
            //print validate
            if (msj.status === 1) {
                input.classList.remove("error");
            } else {
                input.classList.add("error");
            }
      
            return msj;
        },
        validateInputDni(input) {
            // var input = form.querySelector(".bannerFormDni"),
            var tel = input.value,
            cont = 8,
            datalength,
            msj = { status: 0, msj: "" };
      
            if (tel !== "") {
                // var fistNum = input.value[0];
        
                // if (fistNum !== "8") {
                //     input.value = "";
                // } else {
                  input.value = tel.replace(/[^0-9.]/g, "");
                  datalength = input.value;
      
                  if (cont === datalength.length) {
                      msj = { status: 1, msj: "Validado" };
                  } else {
                      msj = { status: 0, msj: "Ingrese DNI valido" };
                  }
                // }
            } else {
                msj = { status: 0, msj: "Ingrese telefono" };
            }
      
            //print validate
            if (msj.status === 1) {
                input.classList.remove("error");
            } else {
                input.classList.add("error");
            }
      
            return msj;
        },
        validateCheckTerms(form) {
          var isChecked = document.getElementById("checktop").checked,
            divChecked = form.querySelector(".input-mask-top"),
            msj;
    
          if (!isChecked) {
            divChecked.classList.add("error");
            msj = { status: 0, msj: "Acepte terminos" };
          } else {
            divChecked.classList.remove("error");
            msj = { status: 1, msj: "Acepto" };
          }
    
          return msj;
        },
        getcurrentIp() {
          let ipObject = { ip: '' };
          const ipRequest = new XMLHttpRequest();
          ipRequest.open('GET', 'https://api.ipify.org/?format=json', false);
          
           try {
              ipRequest.send();
    
              if (ipRequest.status == 200) {
                ipObject = JSON.parse(ipRequest.responseText);
              }
           } catch (error) {
              console.log(error)
              return ipObject.ip;
           }
    
          return ipObject.ip;
        },
        getFecha() {
            let newDate = new Date(),
              year = newDate.getFullYear(),
              month =
                newDate.getMonth() < 10
                  ? "0" + newDate.getMonth()
                  : newDate.getMonth(),
              day =
                newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate(),
              hour =
                newDate.getHours() < 10
                  ? "0" + newDate.getHours()
                  : newDate.getHours(),
              minutes =
                newDate.getMinutes() < 10
                  ? "0" + newDate.getMinutes()
                  : newDate.getMinutes(),
              seconds =
                newDate.getSeconds() < 10
                  ? "0" + newDate.getSeconds()
                  : newDate.getSeconds();
      
            return (
              year +
              "-" +
              month +
              "-" +
              day +
              " " +
              hour +
              ":" +
              minutes +
              ":" +
              seconds
            );
        },
        sendLeadData(form, ordBanner, codOrigen, modality) {


          // if(document.getElementById('utm_source')){
          //   utm_source =  document.getElementById('utm_source').value;
          //   utm_medium = document.getElementById('utm_medium').value;
          //   utm_campaign =  document.getElementById('utm_campaign').value;
          //   utm_content = document.getElementById('utm_content').value;
          //   utm_term = document.getElementById('utm_term').value;
          //   gclid =  document.getElementById('gclid').value;
          // }

            var cel = form.querySelector(".bannerFormNum"),
              dni = form.querySelector(".bannerFormDni"),
              mw_url = "https://middleware-entel.segmentid.pro/api/leads/sync", 
              current_url = window.location.origin + window.location.pathname,
              lead_data = {
                ip: this.getcurrentIp(),
                cod_origen: codOrigen,
                telefono: cel.value,
                operador: "",
                nombre: "",
                email: "",
                tipo_doc: "DNI",
                documento: dni.value,
                modalidad: modality,
                plan: "",
                producto: "banner"+ordBanner,
                departamento: "",
                fechalead: this.getFecha(),
                faseembudo: "",
                resultado_crediticia: "",
                resultado_portabilidad: "",
                utm_source: this.getParameterURL('utm_source') ? this.getParameterURL('utm_source') : '',
                utm_medium: this.getParameterURL('utm_medium') ? this.getParameterURL('utm_medium') : '',
                utm_campaign: this.getParameterURL('utm_campaign') ? this.getParameterURL('utm_campaign') : '',
                utm_content: this.getParameterURL('utm_content') ? this.getParameterURL('utm_content') : '',
                utm_term: this.getParameterURL('utm_term') ? this.getParameterURL('utm_term') : '',
                gclid: this.getParameterURL('gclid') ? this.getParameterURL('gclid') : '',
                producto_marca: "",
                producto_cantidad_cuotas: "",
                producto_monto_cuota_inicial: "",
                producto_monto_cuota_mensual: "",
                mercado: "personas",
                politicas_privacidad_datos: 1,
                fecha_programada: "",
                segmento: "",
                score: "",
                url_lead: current_url,
                extra1: "",
                extra2: "",
                extra3: "",
                prioridad: "",
            };
      
            //Enviar Leads
            var xhr = new XMLHttpRequest();
      
            xhr.onreadystatechange = () => {
              var dataLayer = window.dataLayer || [];
              var responseId = xhr.responseText.slice(6).trim().split(",")[0];
      
              if (xhr.readyState === 4) {
                switch (xhr.status) {
                  // succes
                  case 201:
                    // datalayer
                    dataLayer.push({
                      event: "gtm.event",
                      eventCategory: "Entel home_Banners",
                      eventAction: "Submit",
                      eventLabel: "Success",
                      dimension1: codOrigen,
                      dimension2: responseId,
                      product: "banner"+ordBanner
                    });
      
                    dataLayer.push({
                      dimension1: "",
                      dimension2: "",
                    });
      
                    // messages
                    cel.value = "";
                    dni.value = "";
                    loading.style.display = 'none';
                    thanks.style.display = 'block';
      
                    setTimeout(() => {
                        thanks.style.display = 'none';
                        body.style.display = 'block';
                    }, 3000);
                    break;
      
                  // repeated
                  case 200:
                    dataLayer.push({
                      event: "gtm.event",
                      eventCategory: "Entel home_Banners",
                      eventAction: "Submit",
                      eventLabel: "Failure - Duplicated",
                    });
      
                    cel.value = "";
                    dni.value = "";
                    loading.style.display = 'none';
                    duplicate.style.display = 'block';
      
                    setTimeout(() => {
                        duplicate.style.display = 'none';
                        body.style.display = 'block';
                    }, 3000);
                    break;
      
                  //error
                  default:
                    dataLayer.push({
                      event: "gtm.event",
                      eventCategory: "Entel home_Banners",
                      eventAction: "Submit",
                      eventLabel: "Failure - Server Error",
                    });
      
                    cel.value = "";
                    dni.value = "";
                    loading.style.display = 'none';
                    error.style.display = 'block';
      
                    setTimeout(() => {
                        error.style.display = 'none';
                        body.style.display = 'block';
                    }, 3000);
                    console.log("<b>Lo sentimos Ocurrió un error</b><br>al procesar tus datos.");
                    break;
                }
              }
            };
      
            xhr.open("POST", mw_url);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "*/*");
            xhr.send(JSON.stringify(lead_data));
        }
    }
    

    let initialize = function() {
      // html_obj.append();
      formpopup.init();
    }

    return {
        init: initialize
    }
})()

global.init()