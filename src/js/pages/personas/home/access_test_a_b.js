import 'slick-carousel'
import '../../../components/appear'

export function slider_access(country) {
    let sliderSelector = '.e-access_test_a_b__'+country
    return $(sliderSelector).slick({
        dots: false,
        arrows: true,
        slidesToScroll: 1,
        slidesToShow: 4,
        mobileFirst:true,
        infinite: false, 
        responsive: [
            {
                breakpoint: 359,
                settings: {
                    slidesToScroll: 4,
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 639,
                settings: {
                    slidesToScroll: 5,
                    slidesToShow: 5
                }
            }
        ]
    })
}