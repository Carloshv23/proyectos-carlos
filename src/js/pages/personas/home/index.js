// principal styles
import "normalize.css";
import "../../../../scss/pages/personas/home/index.scss";
import "../../../components/elementExists";
import lazyLoadInit from "../../../components/lazyload";
//import Loading from "../../../components/loading";
lazyLoadInit();
//new Loading().page()
/** Modal */
import { CloseModal, Modal, SceneModal } from "../../../components/modal";
new Modal().init()
new CloseModal().init()

// principal logic
import access from "./access";
// import "./popupForm"; //solo para mostrar el popup c2c
import "./form";
import banners from "./banners";
import entelNotify from "./entelNotify";
import phonesSection from "./phones";
import entelWorld from "./entelWorld";
//import benefits from "./benefits";
import services from "./services";
import contact from "./contact";
// import topBanner from './topBanner';
import datalayer from "./datalayer";
import  "./datalayer_clean";
// import TimerHandBand from "./timerHandBand";
import plansTesting from "./plans_test_a_b";
import popupPlans from "./popupPlans";
import popupChipPlans from "./popupChipPlans";
import popupPlansItemAccess from "./popupPlansItemAccess";
import "./btn-app"
import "./btn-app-equipo"
import { slider_access } from "./access_test_a_b"
import '../../../components/tab'
import { format } from "crypto-js";
slider_access('peru')
slider_access('chile')
// topBanner()
access();
banners();
entelNotify();
phonesSection();
entelWorld();
//benefits();
services();
contact();
datalayer();
// TimerHandBand();
plansTesting();
popupPlans();
popupChipPlans();
popupPlansItemAccess();

//desactiva click derecho
document.oncontextmenu = function (e) {
  return false;
};



