
// console.log('leyendo datalayersss')
window.dataLayer = window.dataLayer || [];

var $entel_data_layer_btn,
    $entel_data_layer_btn_lead,
    $entel_data_layer_view,
    $entel_data_layer_view_banner,
    $entel_data_layer_product,
    $entel_data_layer_toggle,
    $entel_data_layer_view_firt=0,
    $entel_data_layer_arrow;

    var portalDataLayerCache = {
    load: function () {
        $entel_data_layer_btn = $('.entel-data-layer-btn');
        $entel_data_layer_btn_header = $('.entel-data-layer-btn-header');
        $entel_data_layer_view = $('.entel-data-layer-view');
        $entel_data_layer_product = $('.entel-data-layer-product');
        $entel_data_layer_toggle = $('.entel-data-layer-toggle');
        $entel_data_layer_arrow = $('.entel-slider-opciones5-detail').find('.slick-arrow');
    }
};

var portalDataLayer = {
    load: function () {
        $entel_data_layer_btn.on('click', function () {
            var $this = $(this),
                datalayer_category = $this.data('gtm-category'),
                datalayer_action = $this.data('gtm-action'),
                datalayer_label = $this.data('gtm-label');
            portalDataLayerPush.dataVirtualEvent(datalayer_category, datalayer_action, datalayer_label);
        });
        $entel_data_layer_btn_header.on('click', function () {
            var $this = $(this),
                datalayer_category = $this.data('gtm-category'),
                datalayer_action = $this.data('gtm-action'),
                datalayer_label = $this.data('gtm-label'),
                datalayer_product = $this.data('gtm-product'),
                datalayer_tipo = $this.data('gtm-tipo');
            portalDataLayerPush.dataVirtualEventHeader(datalayer_category, datalayer_action, datalayer_label,datalayer_product,datalayer_tipo);
        });
        $entel_data_layer_toggle.on('click', function () {
            var $this = $(this),
                datalayer_category = $this.data('gtm-category'),
                datalayer_action = $this.data('gtm-action'),
                datalayer_label = $this.data('gtm-label');
                if ($this.hasClass('js--active')) {
                    portalDataLayerPush.dataVirtualEvent(datalayer_category, datalayer_action, datalayer_label);
            }
        });
    }
};

var portalDataLayerPush = {
    dataVirtualEventHeader: function(category, action, label,product,tipo) {
        // console.log('category=' + category + ' - action=' + action + ' - label=' + label);
        dataLayer.push ({
            'event': 'virtualEvent',
            'category': category,
            'action': action,
            'label': label,
            'product': product,
            'tipo': tipo,
        });
    },
    dataVirtualEvent: function(category, action, label) {
        // console.log('category=' + category + ' - action=' + action + ' - label=' + label);
        dataLayer.push ({
            'event': 'virtualEvent',
            'category': category,
            'action': action,
            'label': label,
        });
    },
};

var portalDataLayerInit = {
    funciones : function () {
        
        portalDataLayerCache.load();

        // if ($entel_data_layer_btn.size() !== 0 || $entel_data_layer_toggle.size() !== 0 || $entel_data_layer_arrow.size() !== 0) {
        portalDataLayer.load();
        // }

    }
};

jQuery(document).ready(portalDataLayerInit.funciones);