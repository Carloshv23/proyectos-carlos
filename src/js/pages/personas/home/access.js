import 'slick-carousel'
import '../../../components/appear'
import { CloseModal, ChangeScene, OpenModal, Modal, SceneModal } from "../../../components/modal";

export default () => {
    class Access {
        constructor() {
            this.sliderSelector = '.e-access__slider'
            this.$title = document.querySelector('.entel-access h2')
            this.$slider = document.querySelector(this.sliderSelector)
            this.$cards = document.querySelectorAll('.entel-access .entel-access__item')
        }

        clickForm () {
            let _this = this;
            $('.entel-access__item-form').on('click', function(){
                let $this = $(this); 
                new CloseModal().close()
                setTimeout(function(){
                    new OpenModal('modal_form').init()
                    _this.setForm($this.data('modalty'));
                }, 200);
            });
        }

        setForm(modalty) {
            let $subutton = $('#form__register').find('.subbutton'); 
            $('#form__register').data('modalty', modalty);
            
            if(modalty=='renueva'){
                $subutton.hide();
            } else {
                $subutton.show();
            }
        }

        slider() {
            $(this.sliderSelector).slick({
                //rows: 0,
                //dots: false,
                dots: true,
                fade: false,
                arrows: true,
                autoplay: false,
                infinite: false,
                //slidesToScroll: 1,
                slidesToScroll: 5,
                //mobileFirst: true,
                cssEase: 'linear',
                //slidesToShow: 3.6,
                slidesToShow: 5,
                // centerMode: false,
                // pauseOnHover:false,
                autoplaySpeed: 5000,
                //swipeToSlide: true,
                //centerPadding: '0',
                adaptiveHeight: false,
                pauseOnDotsHover: false,
                // responsive: [
                //     {
                //         breakpoint: 359,
                //         settings: {
                //             slidesToShow: 3,
                //             arrows: true
                //         }
                //     },
                //     {
                //         breakpoint: 374,
                //         arrows: true,
                //         settings: {
                //             slidesToShow: 3,
                //             arrows: true
                //         }
                //     },
                //     {
                //         breakpoint: 399,
                //         settings: {
                //             slidesToShow: 4.5,
                //             arrows: true
                //         }
                //     },
                //     {
                //         breakpoint: 509,
                //         settings: {
                //             slidesToShow: 5.5
                //         }
                //     },
                //     {
                //         breakpoint: 629,
                //         settings: {
                //             slidesToShow: 6.5,
                //             arrows: false
                //         }
                //     },
                //     {
                //         breakpoint: 767,
                //         settings: {
                //             slidesToShow: 6.5,
                //             arrows: false
                //         }
                //     },
                //     {
                //         breakpoint: 799,
                //         settings: {
                //             slidesToShow: 7,
                //             arrows: false
                //         }
                //     }
                // ]
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToScroll: 4,
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToScroll: 3,
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: 'unslick'
                    }
                ]
            })
        }

        animation() {
            // animation logic
            $('.e-access__slider').appear(() => {
                setTimeout(() => {
                    // activating title
                    this.$title.classList.add('js--animation')
                    
                    // activating cards
                    this.$cards.forEach($item => {
                        $item.classList.add('js--animation')

                        setTimeout(() => {
                            $item.classList.add('js--active')
                            $item.classList.remove('js--animation')
                        }, 1000)
                    })
                }, 300)
            })
        }

        load() {
            this.clickForm()

            if (this.$cards && this.$slider) {
                // this.slider() // stop carousel

                //
                // document.addEventListener('DOMContentLoaded', () => {
                //     this.animation()
                // })
            }
        }
    }

    // init
    new Access().load()
}