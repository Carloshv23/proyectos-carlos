export default () => {
    class topBanner {
        constructor() {
            this.$topBanner = document.querySelector('#entel-top-banner')
            this.$closeTopBanner = document.querySelector('#close-entel-top-banner')

            this.buttonOpen = document.querySelector('.button-open-menu')
            this.$header = document.querySelector('.lq-eheader')
            this.$noHeader = document.querySelector('.lq-no-header')
            this.$navPage = document.querySelector('.lq-enav-page')
            this.$wrapperHeader = document.querySelector('.lq-wrapper__menu ')
        }

        clickCloseTopBanner() {
            this.$closeTopBanner.addEventListener('click', () => {
                this.removeClassElement()
            })

            this.buttonOpen.addEventListener('click', () => {
                this.removeClassElement()
            })
        }

        removeClassElement() {
            this.$topBanner.remove('e-top-banner')

            this.$header.classList.remove('js--active')
            this.$noHeader.classList.remove('js--active')
            this.$navPage.classList.remove('js--active')
            this.$wrapperHeader.classList.remove('js--active')
        }

        scroll() {
            window.onscroll = () => {
                if ($(window).scrollTop() > 0) {
                    this.removeClassElement()
                }
            }

            if ($(window).scrollTop() > 0) {
                this.removeClassElement()
            }
        }

        addClassElements() {
            this.$header.classList.add('js--active')
            this.$noHeader.classList.add('js--active')
            this.$navPage.classList.add('js--active')
            this.$wrapperHeader.classList.add('js--active')
        }

        init() {
            if (this.$topBanner) {
                this.scroll()
                this.addClassElements()
                this.clickCloseTopBanner()
            }
        }
    }

    // init top banner
    new topBanner().init()
}