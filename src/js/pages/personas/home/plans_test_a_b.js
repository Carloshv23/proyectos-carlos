export default () => {
    class plansTesting {
        constructor() {}
    
        init() {
            let linkMore = document.querySelectorAll(".js--show-box-plans"),
                currentTarget,
                nextTarget,
                actualTarget;
            
            if(linkMore.length > 0){
                linkMore.forEach((current) => {
                    current.addEventListener("click", (item,i) => {
                        currentTarget = item.currentTarget;
                        nextTarget = (currentTarget.nextElementSibling) ? currentTarget.nextElementSibling : false;
                        actualTarget = document.querySelector(".js--active");
            
                        // if(actualTarget && actualTarget != currentTarget) {
                        //    actualTarget.nextElementSibling.classList.remove('js--active');
                        //    actualTarget.classList.remove('js--active');
                        // }
            
                        if(!nextTarget.classList.contains('js--active')) {
                            nextTarget.classList.add('js--active');
                            currentTarget.classList.add('js--active');
                        } else {
                            nextTarget.classList.remove('js--active');
                            currentTarget.classList.remove('js--active');
                        }
        
                    });
                });
            };
        }
    }
  
    $(() => {
      new plansTesting().init();
    })
  };
  