import 'jquery-validation/dist/jquery.validate'
import 'jquery-mask-plugin/dist/jquery.mask'

import { CloseModal, ChangeScene, OpenModal, Modal, SceneModal } from "../../../components/modal";

let contact = (function () {
    let init

    class Form {
        constructor($form) {
            this.$form = $form
            this.originCanpaign
            this.dataForm
        }

        init() {
            this.validate();
            this.mask();
            this.originCanpaign = 'C2C_General';
            this.modalty = 'Migra Postpago';
            // Renovacion_C2C_General
        }

        reset() {
            this.$form[0].reset();
            autofill_app.fillPhone();
        }


        // generic function to get parameter URL
        getParameterURL(parameter) {
            var pageUrl = decodeURIComponent(window.location.search.substring(1)),
                parameters = pageUrl.split('&'),
                parameterName,
                i

            for (i = 0; i < parameters.length; i++) {
                parameterName = parameters[i].split('=')

                if (parameterName[0] === parameter) {
                    return parameterName[1] === undefined ? true: parameterName[1]
                }
            }
        }

        mask() {
            this.$form.find('#celular').mask('999999999')
            this.$form.find('#celular').mask('Z00000000', {
                translation: {
                    'Z': {
                        pattern: /[9]/,
                        optional: false
                    }
                }
            })
        }

        validate() {

            let _this = this;

            // $('#form__register > .e-input > span').addClass('js--active');
            $('#form__register > .e-input > input').attr('placeholder', ' ');
            this.$form.on('submit', function (e) {
                e.preventDefault()
            }).validate({
                rules: {
                    celular: {
                        required: true,
                        maxlength: 9
                    },
                    term_politics: {
                        required: true,
                        minlength: 1
                    },
                },
                messages: {
                    celular: {
                        required: "Por favor ingrese un celular",
                        maxlength: "Ingrese un celular válido"
                    },  
                },
                submitHandler: function () {


                    if($('#form__register').data('modalty')=='renueva'){
                        _this.originCanpaign = 'Renovacion_C2C_General';
                        _this.modalty = 'genérico';
                    } else {
                        _this.originCanpaign = 'C2C_General';
                        _this.modalty = 'Migra Postpago';
                    }

                    // setting data to send to smart leads
                    this.dataForm = {
                        origin: _this.originCanpaign,
                        phone: $('#form__register').find('input[name="celular"]').val(),
                        modality: _this.modalty,
                        isLoading: function() {
                            // is loading
                        },
                        isSended: function() {
                            // is sended
                        },
                        isSuccess: function() {
                            // is success
                            new CloseModal().close()
                            _this.reset()
                            // setTimeout(function(){
                            //     new OpenModal('e-doslineas__modal-success').init()
                            // }, 200);
                            
                            dataLayer.push({
                                'event': 'ModalMigra',//'ModalRenueva'
                                'Ofertas y Promociones': [{
                                    'data-gtm-category': 'form superior',
                                    'data-gtm-label': $('#form__register').find('#entel-form-top-btn').data('gtm-label'),
                                    'data-gtm-action': window.location.href
                                }]
                            })
                        },
                        isRepeated: function() {
                            // is repeated
                        },
                        isError: function() {
                            // is sended
                            new CloseModal().close()
                            _this.reset()
                            // setTimeout(function(){
                            //     new OpenModal('e-doslineas__modal-error').init()
                            // }, 200);
                        }
                    }

                    _this.sendToSmartLeads(this.dataForm)
                    
                },
                errorPlacement: function (error, element) {
                    error.insertBefore(element);
                }
            })
        }

        getIpUser(){
            let ipObject = { ip: '' };
            const ipRequest = new XMLHttpRequest();
            ipRequest.open('GET', 'https://api.ipify.org/?format=json', false);

            try {
            ipRequest.send();

            if (ipRequest.status == 200) {
                ipObject = JSON.parse(ipRequest.responseText);
            }
            } catch (error) {
            return ipObject.ip;
            }

            return ipObject.ip;
        }
        // get data lead
        getCurrentDate() {
            var newDate = new Date(),
                year = newDate.getFullYear(),
                month = newDate.getMonth() < 10 ? '0' + newDate.getMonth() : newDate.getMonth(),
                day = newDate.getDate() < 10 ? '0' + newDate.getDate() : newDate.getDate(),
                hour = newDate.getHours() < 10 ? '0' + newDate.getHours() : newDate.getHours(),
                minutes = newDate.getMinutes() < 10 ? '0' + newDate.getMinutes() : newDate.getMinutes(),
                seconds = newDate.getSeconds() < 10 ? '0' + newDate.getSeconds() : newDate.getSeconds()

            return year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds
        }
        
        // generic function to send data to Smart Leads
        sendToSmartLeads(data) {
            let leadData = {},
                currentDate = this.getCurrentDate(),
                apiURL = 'https://middleware-entel.segmentid.pro/api/leads/sync',
                pageURL =  window.location.origin + window.location.pathname,
                ipUser = this.getIpUser(),
                responseId2

            leadData = {
                cod_origen: (data.origin) ? data.origin : '',
                telefono: (data.phone) ? data.phone : '',
                operador: (data.operator) ? data.operator : '',
                nombre: (data.name) ? data.name : '',
                email: (data.email) ? data.email : '',
                tipo_doc: (data.typeDocument) ? data.typeDocument : '',
                documento: (data.document) ? data.document : '',
                modalidad: (data.modality) ? data.modality : '',
                plan: (data.detail) ? data.detail : '',
                producto: 'Genérico',
                departamento: '',
                fechalead: (currentDate) ? currentDate : '',
                faseembudo: '',
                resultado_crediticia: '',
                resultado_portabilidad: '',
                utm_source: (this.getParameterURL('utm_source')) ? this.getParameterURL('utm_source') : '-',
                utm_medium: (this.getParameterURL('utm_medium')) ? this.getParameterURL('utm_medium') : '-',
                utm_campaign: (this.getParameterURL('utm_campaign')) ? this.getParameterURL('utm_campaign') : '-',
                utm_content: (this.getParameterURL('utm_content')) ? this.getParameterURL('utm_content') : '-',
                utm_term: (this.getParameterURL('utm_term')) ? this.getParameterURL('utm_term') : '-',
                gclid: (this.getParameterURL('gclid')) ? this.getParameterURL('gclid') : '-',
                producto_marca: (data.brand) ? data.brand : '',
                producto_cantidad_cuotas: (data.fee) ? data.fee : '',
                producto_monto_cuota_inicial: (data.initalFee) ? data.initalFee : '',
                producto_monto_cuota_mensual: (data.monthlyFee) ? data.monthlyFee : '',
                mercado: 'Personas',
                politicas_privacidad_datos: '1',
                fecha_programada: '',
                segmento: '',
                score: '',
                url_lead: pageURL,
                extra1: (data.toltaCost) ? data.toltaCost : '',
                extra2: '',
                extra3: '',
                prioridad: '',
                ip: (ipUser) ? ipUser : '',
            }

            let xhr = new XMLHttpRequest()

            xhr.onreadystatechange = function (e) {
                let responseId = xhr.responseText.slice(6).trim().split(',')[0]
                let phone = $('.validonlynumeric').val()


                switch (xhr.readyState) {
                    case 1:
                        if (data.isLoading !== null && data.isLoading !== undefined) {
                            data.isLoading()
                        }
                    break

                    case 4:
                        switch (xhr.status) {
                            // success
                            case 201:
                                var dataLayer = window.dataLayer || [];
                                dataLayer.push({
                                    event: 'gtm.event',
                                    eventCategory: 'Home - Envío de formulario',
                                    eventAction: 'Submit',
                                    eventLabel: 'Success',
                                    dimension1: (data.origin) ? data.origin : '',
                                    dimension2: responseId
                                });
                                dataLayer.push({
                                    dimension1: '',
                                    dimension2: ''
                                });

                                if (data.isSended !== null && data.isSended !== undefined) {
                                    data.isSended()
                                }

                                if (data.isSuccess !== null && data.isSuccess !== undefined) {
                                    data.isSuccess()
                                }

                                $.ajax({
                                    type: 'POST',
                                    url: 'https://www.entel.pe/wp-admin/admin-ajax.php',
                                    data: {
                                        action: 'vtex_fb_api_conversion',
                                        phone: phone,
                                        sku: '103408',
                                        price: '1020',
                                        url: 'https://www.entel.pe/ofertas-y-promociones/?utm_source=facebook&utm_medium=social_paid_perf',
                                        responseId
                                    },
                                    async: false,
                                    success: function (data) {
                                        console.log(data)
                                    },
                                    error: function () {
                                        console.log('Fallo FB')
                                    }
                                })

                            break

                            // repeated
                            case 200:
                                var dataLayer = window.dataLayer || [];
                                dataLayer.push({
                                    event: 'gtm.event',
                                    eventCategory: 'Home - Envío de formulario',
                                    eventAction: 'Submit',
                                    eventLabel: 'Failure - Duplicated'
                                });

                                if (data.isSended !== null && data.isSended !== undefined) {
                                    data.isSended()
                                }

                                if (data.isRepeated !== null && data.isRepeated !== undefined) {
                                    data.isRepeated()
                                }
                            break

                            // error
                            default:
                                var dataLayer = window.dataLayer || [];
                                dataLayer.push({
                                    event: 'gtm.event',
                                    eventCategory: 'Home - Envío de formulario',
                                    eventAction: 'Submit',
                                    eventLabel: 'Failure - Server Error'
                                });

                                if (data.isError !== null && data.isError !== undefined) {
                                    data.isError()
                                }
                            break
                        }
                    break
                }
            }

            setTimeout (function() {
                xhr.open('POST', apiURL)
                xhr.setRequestHeader('Content-Type', 'application/json')
                xhr.setRequestHeader('Accept', '*/*')
                xhr.send(JSON.stringify(leadData))
            }, 100)
        }
    }
    
    /**
    Form autofill
    */
    var autofill_app = {
        phone: "",
        init: function ($form) {
            var _this = this

            _this.getPhone()
            _this.$form = $form;

            if (_this.phone != "") {
                _this.fillPhone()
                _this.clearPhone()
                _this.sendDataLayer()
            }
        },
        getPhone: function () {
            var _this = this,
                phone = localStorage.getItem("autofill_phone")

            if (phone == null) {
                var hash = window.location.hash

                if (/cel=9\d{8}/.test(hash)) {
                    _this.phone = hash.substring(hash.indexOf("cel=9"), 14).split("=")[1]
                    localStorage.setItem("autofill_phone", _this.phone)
                }
            } else {
                _this.phone = phone;
            }
        },
        fillPhone: function () {
            var _this = this;
            var input = document.getElementById("entel-form-min-number");

            if(_this.phone != ''){
                input.value = _this.phone;
                // input.parentElement.querySelector('span').classList.add('js--active');
            }
        },
        clearPhone: function () {
            window.location.hash = window.location.hash.replace(/cel=9\d{8}/, "")
        },
        sendDataLayer: function() {
            var dataLayer = window.dataLayer || [];
            var _this = this,
                hashPhone = window.btoa(_this.phone)

            dataLayer.push({
            'event': 'gtm.event',
            'eventCategory': 'Home Autofill',
            'eventAction': 'Autofill',
            'eventLabel': hashPhone
            })
        }
    }

    init = function () {
        let $form = $('#form__register');

        // load phrases section
        if ($form.mExists()) {
            new Form($form).init();
        }
        autofill_app.init($form);

    }

    return {
        init: init
    }
}())

contact.init()

