import {Tabs} from './tabs'
import '../../../components/appear'

export default () => {
    class entelWorld {
        constructor() {
            this.$entelWorld = document.querySelector('#entel-world-tabs')
            this.$navTabs = document.querySelectorAll('#entel-world-tabs a')
            this.$pictures = document.querySelectorAll('#entel-world-tabs .entel-world__tabs__picture picture'),
            this.$PrepagoPictures = document.querySelectorAll('#tab-postpago .entel-world__tabs__picture picture')
        }

        tabs() {
            // init tabs
            new Tabs({
                element: this.$entelWorld
            }).init()
        }

        clickItemsTab() {
            let
                currentSection,
                $currentPictures

            // iterating items nav tabs
            this.$navTabs.forEach(($item) => {
                $item.addEventListener('click', () => {
                    // go anchor 
                    /*$('html, body').animate({
                        scrollTop: $('.entel-world').offset().top - 75
                    }, 500)*/

                    // get current section
                    currentSection = $item.getAttribute('href')
                    $currentPictures = document.querySelectorAll(`${currentSection} .entel-world__tabs__picture picture`)

                    // active pictures
                    this.resetPictures()
                    this.activePictures($currentPictures)
                })
            })
        }

        animation() {
            // animation logic
            $('#entel-world-tabs').appear(() => {
                // activating pictures
                this.activePictures(this.$PrepagoPictures)
            })
        }

        activePictures($currentPictures) {
            setTimeout(() => {
                // iterating pictures
                $currentPictures.forEach(($picture) => {
                    $picture.classList.add('js--active')
                })
            }, 300)

            setTimeout(() => {
                // iterating pictures
                $currentPictures.forEach(($picture) => {
                    $picture.classList.add('js--animation')
                })
            }, 1000)
        }

        resetPictures() {
            // iterating pictures
            this.$pictures.forEach(($picture) => {
                $picture.classList.remove('js--active')
                $picture.classList.remove('js--animation')
            })
        }

        init() {
            if (this.$entelWorld) {
                this.tabs()
                this.clickItemsTab()

                //
                document.addEventListener('DOMContentLoaded', () => {
                    this.animation()
                })
            }
        }
    }

    // init entel world
    new entelWorld().init()
}