export default () => {
  class Notify {
    constructor() {
      // vars
      // this.homeSlider = '#slider-entel'
      // this.navSlider = '.entel__slider--custom-home-navigation'
      // this.numSlides = 4
    }

    close() {
      $("body").addClass("entel-notify__body");
      $(".entel-notify__main .entel-notify__close").on("click", function () {
        $("body").removeClass("entel-notify__body");
        // console.log("test notify body");
        // $(this).css("display", "none");
      });
    }

    load() {
      this.close();
    }
  }

  // load
  new Notify().load();
};
