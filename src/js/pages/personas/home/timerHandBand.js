export default () => {
  class TimerHandBand {
    constructor(retrospectivDate) {
      this.retrospectivDate = retrospectivDate;
    }
    run() {
      console.log(this.retrospectivDate);
      // Set the date we're counting down to
      let countDownDate = new Date(this.retrospectivDate).getTime();

      // update the count down every 1 second
      let x = setInterval(function () {
        //  console.log(this.hoursID);
        // get today's date and time
        let now = new Date().getTime();

        // find the distance between now and the count down date
        let distance = countDownDate - now;

        // time calculations for days, hours, minutes and seconds
        let days = Math.floor(distance / (1000 * 60 * 60 * 24));
        let hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        let seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // fix days
        if (days > 0) {
          hours = 24 * days + hours;
        }
        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        // output the result in an element with id
        // document.getElementById("days").innerHTML = days;
        document.getElementById("hours").innerHTML = hours;
        document.getElementById("minutes").innerHTML = minutes;
        document.getElementById("seconds").innerHTML = seconds;

        // if the count down is over, write some text
        if (distance < 0) {
          clearInterval(x);
          console.log("EXPIRED timer");
          document.getElementById("hours").innerHTML = "00";
          document.getElementById("minutes").innerHTML = "00";
          document.getElementById("seconds").innerHTML = "00";
        }
      }, 1000);
    }
  }

  let dateEndEntelDay = new TimerHandBand("Mar 2, 2023 23:59:59");
  dateEndEntelDay.run();
};  
