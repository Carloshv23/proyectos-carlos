import "slick-carousel";

export default () => {
  class HomeSlider {
    constructor() {
      // vars
      this.homeSlider = "#slider-entel";
      this.navSlider = ".entel__slider--custom-home-navigation";
    }

    orderBanner() {
      let now = new Date(),
      slider,
      items,
      item,
      weekday = now.getDay();

      if(weekday == 2 || weekday == 4){
        slider = document.querySelector('#slider-entel');
        items = slider.querySelectorAll('.entel__slider--home__item');

        item = items[1];
        if (item) {
          if(item.classList.contains('entel__slider--home__pagounico')) {
            slider.insertBefore(item, slider.firstChild);
          };
        }
      }
    }

    hiddenform(){
      // Set vars
      let now = new Date(),
      later = new Date(now),
      early = new Date(now),
      msn = '<br><strong>En breve te llamaremos!</strong><br> Gracias por preferir Entel.',
      int_ban,
      c2c_ban = document.querySelectorAll(".banner__form .form--content .form--thanks p");

      later.setHours(22);
      later.setMinutes(0);
      later.setSeconds(0);

      early.setHours(8);
      early.setMinutes(0);
      early.setSeconds(0);

      //validar si se encuentra fuera del rango
      if(now < early){
        msn = '<strong>Gracias!</strong> nos<br> comunicaremos contigo <small>desde las 8:00 am</small>';
      } else if(now > later){
        msn = '<strong>Gracias!</strong> nos<br> comunicaremos contigo <small>mañana desde las 8:00 am</small>';
      }
      // later.setHours(19);
      // later.setMinutes(0);
      // later.setSeconds(0);

      // early.setHours(8);
      // early.setMinutes(0);
      // early.setSeconds(0);

      // //validar si se encuentra fuera del rango
      // if(now < early){
      //   msn = '<strong>Muchas Gracias por tu interés!</strong> <br> nos comunicaremos contigo <small>en el rango de 8am a 1pm para atender tu solicitud.</small>';
      // } else if(now > later){
      //   msn = '<strong>Muchas Gracias por tu interés!</strong> <br> nos comunicaremos contigo <small>mañana en el rango de 8am a 1pm para atender tu solicitud.</small>';
      // }

      // if(!(now > early && now < later)){
        int_ban = setInterval(function () {
          if (c2c_ban.length) {
            clearInterval(int_ban);
            c2c_ban.forEach(function (el) {
              el.innerHTML = msn;
            });
          }
        }, 100);
      // }
    }

    slider() {
      let banner_title = $('.entel__slider__option__title');
      
      $(this.homeSlider).on('init', function (event, slick) {
        let text = $(slick.$slides[0]).children().children().data('layer-name')
        banner_title.text(text);
      });

      $(this.homeSlider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        let text = $(slick.$slides[nextSlide]).children().children().data('layer-name')
        banner_title.text(text)
      });

      $(this.homeSlider).slick({
        dots: true,
        infinite: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        autoplay: true,
        autoplaySpeed: 5000,
        mobileFirst: true,
        pauseOnFocus: true,
        pauseOnHover: true,
        focusOnSelect: true,
        fade: true,
        speed: 1000,
        rows: 0,
        lazyLoad: "progressive",
        //asNavFor: this.navSlider,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              arrows: true
            }
          }
        ]
      });
    }
    
    nav() {
      $(this.navSlider).not('.slick-initialized').slick({
        infinite: true,
        arrows: false,
        // slidesToShow: this.numSlides,
        slidesToScroll: 1,
        // slidesToShow: 8,
        dots: false,
        // adaptiveHeight: true,
        // variableWidth: true,
        focusOnSelect: true,
        autoplay: true,
        autoplaySpeed: 5000,
        // edgeFriction: 0,
        // draggable: false,
        asNavFor: this.homeSlider,
        // mobileFirst: true,
        // centerMode: true,
        responsive: [
          {
              breakpoint: 1200,
              settings: {
                arrows: false,
              }
          },
          {
              breakpoint: 992,
              settings: {
                arrows: false,
              }
          },
          {
              breakpoint: 768,
              settings: {
                arrows: false,
              }
          }
      ]
      });

      setTimeout(() => {
        document
          .querySelector(".entel__slider--custom-home-navigation__item")
          .classList.add("js--active");
      }, 300);

      $(this.navSlider).on("afterChange", function (event, slick) {
        $(".entel__slider--custom-home-navigation__item").removeClass(
          "js--active"
        );
        $(
          ".slick-current .entel__slider--custom-home-navigation__item"
        ).addClass("js--active");
      });
    }
    
    getFecha() {
      let newDate = new Date(),
        year = newDate.getFullYear(),
        month =
          newDate.getMonth() < 10
            ? "0" + newDate.getMonth()
            : newDate.getMonth(),
        day =
          newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate(),
        hour =
          newDate.getHours() < 10
            ? "0" + newDate.getHours()
            : newDate.getHours(),
        minutes =
          newDate.getMinutes() < 10
            ? "0" + newDate.getMinutes()
            : newDate.getMinutes(),
        seconds =
          newDate.getSeconds() < 10
            ? "0" + newDate.getSeconds()
            : newDate.getSeconds();

      return (
        year +
        "-" +
        month +
        "-" +
        day +
        " " +
        hour +
        ":" +
        minutes +
        ":" +
        seconds
      );
    }

    bullets() {
      $(".entel__slider--custom-home-navigation").html("");

      let banner = document.querySelector(this.homeSlider),
          slides = banner.querySelectorAll('.slick-slide'),
          nav =  document.querySelector('.entel__wrapper__slider .entel__slider--custom-home-navigation'),
          element,
          arrow_prev = document.querySelector('.entel__slider__option__prev'),
          arrow_next = document.querySelector('.entel__slider__option__next');

      slides.forEach(function (item, index) {
        if(!item.classList.contains('slick-cloned')) {
          element = document.createElement('div');
          element.innerHTML  = '<div class="entel__slider--custom-home-navigation__item"  data-entel-slide-index="' + index + '" data-slick-index="' + index + '"><div class="line__timer"><span class="line__timer__bar">&nbsp;</span></div></div>'
          nav.appendChild(element);
        }
      })

      arrow_prev.addEventListener('click', function(){
        $(banner).slick('slickPrev');
      });

      arrow_next.addEventListener('click', function(){
        $(banner).slick('slickNext');
      });
    }

    getParameterURL(parameter, stringUrl) {
      let pageUrl = stringUrl
          ? stringUrl
          : decodeURIComponent(window.location.search.substring(1)),
        parameters = pageUrl.split("&"),
        parameterName,
        i;

      for (i = 0; i < parameters.length; i++) {
        parameterName = parameters[i].split("=");

        if (parameterName[0] === parameter) {
          return parameterName[1] === undefined ? true : parameterName[1];
        }
      }
    }

    // TODO: mejorar form banners
    binEventsFormBanner(form) {
      if (form) {
        this.eventCheckTerms(form);
        
        form.addEventListener("input", () => {
          this.validateInputPhoneNumber(form);

          // pause slider
          $(this.homeSlider).slick('slickSetOption', 'autoplay', false).slick('slickPause');
          $(this.navSlider).slick('slickSetOption', 'autoplay', false).slick('slickPause');
        })

        let handleSubmit = (e) => {
          e.preventDefault();
          
          let isChecked = this.validateCheckTerms(form);
          let isNumero = this.validateInputPhoneNumber(form);

          if (isNumero.status === 1 && isChecked.status === 1) {
            $(form).find(".form--body").hide();
            $(form).find(".form--loading").show();

            const codOrigen = $(form).data('codorigen');
            const numBanner = $(this.homeSlider).slick('slickCurrentSlide')+1;

            this.sendLeadData(form, codOrigen, numBanner)

            // play slider
            $(this.navSlider).slick('slickSetOption', 'autoplay', true).slick('slickPlay');
            $(this.homeSlider).slick('slickSetOption', 'autoplay', true).slick('slickPlay');
            
            // next slider
            setTimeout(() => {
              $(this.homeSlider).slick('slickNext');
            }, 2000)
          }
        }
        
        form.addEventListener("submit", handleSubmit);
      }
    }

    eventCheckTerms(form) {
      var inputmasktop = form.querySelector(".input-mask-top");

      let handleClick = () => {
        $(form).find(".input-mask-top").toggleClass("error");

        if ($("#checktop").prop("checked")) {
          $("#checktop").prop("checked", false);
        } else {
          $("#checktop").prop("checked", true);
        }
      }

      inputmasktop.addEventListener("click", handleClick);
    }

    validateInputPhoneNumber(form) {
      var input = form.querySelector(".validonlynumeric"),
        tel = input.value,
        cont = 9,
        datalength,
        msj = { status: 0, msj: "" };

      if (tel !== "") {
        var fistNum = input.value[0];

        if (fistNum !== "9") {
          input.value = "";
        } else {
          input.value = tel.replace(/[^0-9.]/g, "");
          datalength = input.value;

          if (cont === datalength.length) {
            msj = { status: 1, msj: "Validado" };
          } else {
            msj = { status: 0, msj: "Ingrese telefono valido" };
          }
        }
      } else {
        msj = { status: 0, msj: "Ingrese telefono" };
      }

      //print validate
      if (msj.status === 1) {
        input.classList.remove("error");
      } else {
        input.classList.add("error");
      }

      return msj;
    }

    validateCheckTerms(form) {
      var isChecked = document.getElementById("checktop").checked,
        divChecked = form.querySelector(".input-mask-top"),
        msj;

      if (!isChecked) {
        divChecked.classList.add("error");
        msj = { status: 0, msj: "Acepte terminos" };
      } else {
        divChecked.classList.remove("error");
        msj = { status: 1, msj: "Acepto" };
      }

      return msj;
    }

    getcurrentIp() {
      let ipObject = { ip: '' };
      const ipRequest = new XMLHttpRequest();
      ipRequest.open('GET', 'https://api.ipify.org/?format=json', false);
      
       try {
          ipRequest.send();

          if (ipRequest.status == 200) {
            ipObject = JSON.parse(ipRequest.responseText);
          }
       } catch (error) {
          console.log(error)
          return ipObject.ip;
       }

      return ipObject.ip;
    }

    sendLeadData(form, codOrigen, numBanner) {
      var input = form.querySelector(".validonlynumeric"),
        mw_url = "https://middleware-entel.segmentid.pro/api/leads/sync", 
        // mw_url = http://dev.middleware-entel.dtransforma.com/api/leads/sync
        current_url = window.location.origin + window.location.pathname,
        lead_data = {
          ip: this.getcurrentIp(),
          cod_origen: codOrigen,
          telefono: input.value,
          operador: "",
          nombre: "",
          email: "",
          tipo_doc: "DNI",
          documento: "",
          modalidad: "Postpago Migra",
          plan: "Cuota cero",
          producto: "banner"+numBanner,
          departamento: "",
          fechalead: this.getFecha(),
          faseembudo: "",
          resultado_crediticia: "",
          resultado_portabilidad: "",
          utm_source: $('#utm_source').val(),
          utm_medium: $('#utm_medium').val(),
          utm_campaign: $('#utm_campaign').val(),
          utm_content: $('#utm_content').val(),
          utm_term: $('#utm_term').val(),
          gclid: $('#gclid').val(),
          producto_marca: "",
          producto_cantidad_cuotas: "",
          producto_monto_cuota_inicial: "",
          producto_monto_cuota_mensual: "",
          mercado: "personas",
          politicas_privacidad_datos: 1,
          fecha_programada: "",
          segmento: "",
          score: "",
          url_lead: current_url,
          extra1: "",
          extra2: "",
          extra3: "",
          prioridad: "",
        };

      //Enviar Leads
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        var dataLayer = window.dataLayer || [];
        var responseId = xhr.responseText.slice(6).trim().split(",")[0];

        if (xhr.readyState === 4) {
          switch (xhr.status) {
            // succes
            case 201:
              // datalayer
              dataLayer.push({
                event: "gtm.event",
                eventCategory: "Entel home_Banners",
                eventAction: "Submit",
                eventLabel: "Success",
                dimension1: codOrigen,
                dimension2: responseId,
                product: "banner"+numBanner
              });

              dataLayer.push({
                dimension1: "",
                dimension2: "",
              });

              // dataLayer.push({
              //   event: "ModalAyuda - Banner Formulario",
              //   Entel: [
              //     {
              //       "data-modalidad": current_url,
              //       "data-gtm-category": "Postpago Migra",
              //     },
              //   ],
              // });

              // messages
              input.value = "";
              $(form).find(".form--loading").hide();
              $(form).find(".form--thanks").show();

              setTimeout(() => {
                $(form).find(".form--thanks").hide();
                $(form).find(".form--body").show();
              }, 3000);
              break;

            // repeated
            case 200:
              dataLayer.push({
                event: "gtm.event",
                eventCategory: "Entel home_Banners",
                eventAction: "Submit",
                eventLabel: "Failure - Duplicated",
              });

              input.value = "";
              $(form).find(".form--loading").hide();
              $(form).find(".form--duplicate").show();

              setTimeout(() => {
                $(form).find(".form--duplicate").hide();
                $(form).find(".form--body").show();
              }, 3000);
              break;

            //error
            default:
              dataLayer.push({
                event: "gtm.event",
                eventCategory: "Entel home_Banners",
                eventAction: "Submit",
                eventLabel: "Failure - Server Error",
              });

              input.value = "";
              $(form).find(".form--loading").hide();
              $(form).find(".form--error").show();

              setTimeout(() => {
                $(form).find(".form--error").hide();
                $(form).find(".form--body").show();
              }, 3000);
              console.log("<b>Lo sentimos Ocurrió un error</b><br>al procesar tus datos.");
              break;
          }
        }
      };

      xhr.open("POST", mw_url);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.setRequestHeader("Accept", "*/*");
      xhr.send(JSON.stringify(lead_data));
    }
    //--------

    formBanners() {
      const forms = document.querySelectorAll(".banner__form");

      if (forms.length) {
        forms.forEach(function(currentItem, index) {
          new HomeSlider().binEventsFormBanner(currentItem);
        });
      }
    }

    headbandClick() {
      $('.e-headband-nuevos-planes-rio .box-content__button a').on('click', function () {
        var int_litobut = window.setInterval(function() {
          if (document.querySelectorAll(".button.lito div").length > 0) {
            window.clearInterval(int_litobut); 
            document.querySelectorAll(".button.lito")[0].click();  
          }
        }, 500);  
      })
    }
    
    load() {
      // this.orderBanner();
      this.slider();
      this.formBanners();
      this.hiddenform();
      this.headbandClick();
    }
  }

  // load
  new HomeSlider().load();
};
