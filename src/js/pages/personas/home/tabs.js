export class Tabs {
    constructor(options) {
        this.$element = options.element

        // vars
        this.$navElements = (this.$element) ? this.$element.querySelectorAll('nav li a') : false
        this.$contentWrap = (this.$element) ? this.$element.querySelector('.e-tabs__content') : false
        this.$contentElements = (this.$element) ? this.$element.querySelectorAll('.e-tabs__content > div') : false
    }

    load() {
        this.clickNav()
    }

    clickNav() {
        let idContent

        this.$navElements.forEach($item => {
            $item.addEventListener('click', (e) => {
                e.preventDefault()

                // getting
                idContent = $item.getAttribute('href')

                // reset
                this.resetElements()

                // adding current elemets
                $item.classList.add('js--active')
                this.$contentWrap.querySelector(idContent).classList.add('js--active')
            })
        })
    }

    resetElements() {
        this.$navElements.forEach(($element, index) => {
            this.$navElements[index].classList.remove('js--active')
            this.$contentElements[index].classList.remove('js--active')
        })
    }

    init() {
        // validating id form
        if (this.$element) {
            this.load()
        }
    }
}