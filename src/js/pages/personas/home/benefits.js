import "slick-carousel";

export default () => {
  class Benefits {
    constructor() {
      this.$sliderBenefitsNav = $("#slider-benefits-nav");
      this.$sliderBenefitsFor = $("#slider-benefits-for");
      this.$benefitsCards = document.querySelectorAll(
        ".e-benefits__slider__card"
      );
    }

    animation() {
      $(".e-benefits__slider").appear(() => {
        setTimeout(() => {
          // actives items
          this.activeItems();
        }, 300);
      });
    }

    activeItems() {
      // iterating items
      this.$benefitsCards.forEach(($item) => {
        $item.classList.add("js--active");
      });
    }

    scroll() {
      // scroll images parallax
      document.addEventListener("scroll", function () {
        let heightcalculated = window.scrollY / 60;
        if (window.matchMedia(`(min-width: 992px)`).matches) {
          document
            .querySelectorAll(".e-benefits__slider__card")
            .forEach(($img) => {
              $img.style.transform = `translateY(-${heightcalculated}px)`;
            });
        }
      });
    }

    resetScroll() {
      // scroll images parallax
      document.querySelectorAll(".e-benefits__slider__card").forEach(($img) => {
        $img.style.transform = `translateY(0px)`;
      });
    }

    sliders() {
      this.$sliderBenefitsNav.slick({
        slidesToShow: 7,
        slidesToScroll: 1,
        asNavFor: this.$sliderBenefitsFor,
        dots: false,
        focusOnSelect: true,
        arrows: false,
        infinite: false,
        adaptiveHeight: true,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 4,
            },
          },
        ],
      });
      this.$sliderBenefitsFor.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        centerMode: false,
        asNavFor: this.$sliderBenefitsNav,
        adaptiveHeight: true,
        infinite: false,
        fade: true,
        cssEase: "linear",
      });
    }

    init() {
      if (this.$benefitsCards) {
        this.sliders();
        document.addEventListener("DOMContentLoaded", () => {
          this.animation();
        });
      }
    }
  }

  new Benefits().init();
};
