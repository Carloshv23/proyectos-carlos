export default () => {
  class PopupPlans {
    constructor() {}

    init() {
      $('.js--show-popup-plans').on('click', (e) => {
        e.preventDefault();

        var idPopup = $(e.currentTarget).data('id-popup'),
          positionCurrentPlan = $(e.currentTarget).offset(),
          positionPopup = {},
          $currentPlanPopupEl = $(`.popup-plans[data-id-popup="${idPopup}"]`);

        const mediaQuery = window.matchMedia('(min-width: 425px)')

        if (mediaQuery.matches) {
          positionPopup = {
            left: positionCurrentPlan.left - 60,
            top: positionCurrentPlan.top - 140
          }
        } else {
          positionPopup = {
            left: positionCurrentPlan.left - 70,
            top: positionCurrentPlan.top - 90
          }
        }

        this.openPopup(positionPopup, $currentPlanPopupEl)
      });

      // close popup
      $('.popup-plans-overlay').on('click', () => {
        this.closePopup();
      });

      $('.popup-plans__close-btn > span').on('click', () => {
        this.closePopup();
      });
    }

    openPopup(positionPopup, $currentPlanPopupEl) {
      $currentPlanPopupEl.css(positionPopup);
      $currentPlanPopupEl.addClass('popup-plans--show');
      $('.popup-plans-overlay').addClass('popup-plans-overlay--show');
      $('#overlay_eheader').addClass('overlay_eheader');
      $('.lq-wrapper__menu').css('z-index', '101');
    }

    closePopup() {
      $('.popup-plans').removeClass('popup-plans--show');
      $('.popup-plans-overlay').removeClass('popup-plans-overlay--show');
      $('#overlay_eheader').removeClass('overlay_eheader');
      $('.lq-wrapper__menu').css('z-index', '99');
    }
  }

  $(() => {
    // init entel plans
    new PopupPlans().init();
  })
};
