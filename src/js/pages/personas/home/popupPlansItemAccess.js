export default () => {
  class PopupPlansItemAccess {
    constructor() {}

    init() {
      $('.entel-access__item_c2c').on('click', (e) => {
        e.preventDefault();

        var positionCurrentPlan = $(e.currentTarget).offset(),
          positionPopup = {},
          $currentPlanPopupEl = $('.popup-plans__access_item');

        const mediaQuery = window.matchMedia('(min-width: 425px)')

        if (mediaQuery.matches) {
          positionPopup = {
            left: positionCurrentPlan.left - 50,
            top: positionCurrentPlan.top - 10
          }
        } else {
          positionPopup = {
            left: positionCurrentPlan.left - 145,
            top: positionCurrentPlan.top - (-100)
          }
        }

        this.openPopupItemAccess(positionPopup, $currentPlanPopupEl)
      });

      // close popup
      $('.popup-plans-overlay__access_item').on('click', () => {
        this.closePopupItemAccess();
      });
      
      $('.popup-plans__access_item__close-btn > span').on('click', () => {
        this.closePopupItemAccess();
      });
    }

    openPopupItemAccess(positionPopup, $currentPlanPopupEl) {
      $currentPlanPopupEl.css(positionPopup);
      $currentPlanPopupEl.addClass('popup-plans-overlay__access_item--show');
      $('.popup-plans-overlay__access_item').addClass('popup-plans-overlay__access_item--show');
      $('#overlay_eheader').addClass('overlay_eheader');
      $('.lq-wrapper__menu').css('z-index', '101');
    }

    closePopupItemAccess() {
      $('.popup-plans__access_item').removeClass('popup-plans-overlay__access_item--show');
      $('.popup-plans-overlay__access_item').removeClass('popup-plans-overlay__access_item--show');
      $('#overlay_eheader').removeClass('overlay_eheader');
      $('.lq-wrapper__menu').css('z-index', '99');
    }
  }

  $(() => {
    // init entel plans
    new PopupPlansItemAccess().init();
  })
};
