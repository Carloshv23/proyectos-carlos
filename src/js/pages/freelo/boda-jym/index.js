import 'normalize.css'
import '../../../../scss/pages/freelo/boda-jym/index.scss';
import '../../../components/elementExists';
import lazyLoadInit from "../../../components/lazyload";
AOS.init();
lazyLoadInit();

// Establece la fecha límite
var countDownDate = new Date("Apr 22, 2023 00:00:00").getTime();

// Actualiza la cuenta regresiva cada segundo
var x = setInterval(function() {

	// Obtiene la fecha y hora actual
	var now = new Date().getTime();

	// Calcula la diferencia entre la fecha límite y la fecha actual
	var distance = countDownDate - now;

	// Calcula los días, horas, minutos y segundos restantes
	var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
  	// Muestra los resultados en los elementos correspondientes
	document.getElementById("days").innerHTML = days;
	document.getElementById("hours").innerHTML = hours;
	document.getElementById("minutes").innerHTML = minutes;
	document.getElementById("seconds").innerHTML = seconds;
                                        
	if (distance < 0) {
		clearInterval(x);
		document.getElementById("countdown").innerHTML = "<h2 class='today'>!Hoy nos casamos!</h2>";
	}
}); // cierra la función setInterval y la llave de la función anónima

if ($(window).width() > 767) {
    $(".wedding-jym__img2").attr("data-aos",'fade-right');
    $(".wedding-jym__img3").attr("data-aos",'fade-left');
}

$('#playsoundtrack').on('click',function(){
	var x = document.getElementById("soundtrack"); 
	if(!x.paused && !x.ended) { 
		x.pause(); 
	 } 
	 else 
	 { 
		x.play(); 
	 } 
})