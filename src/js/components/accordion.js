(function () {
    $.fn.mAccordion = function (options) {
        let $accordion = $(this)
            settings = $.extend ({
                multiple: false,
                anchor: true
            }, options),
            counterAccordion = 1

        // iterating items
        return this.each(function () {
            let item = $(this).find('.m-accordion__box'),
                currentBox

            item.find('.m-accordion__toggle').on('click', function(e) {
                e.preventDefault()

                currentBox = $(this).closest('.m-accordion__box')

                // go to anchor
                if (settings.anchor && window.innerWidth <= 768) {
                    setTimeout(function() {
                        let idContent = currentBox.attr('id')

                        $('html, body').animate({
                            scrollTop: $('#' + idContent).offset().top - 75
                        },{
                            duration: 500,
                            easing: "swing"
                        })
                    }, 500)
                }

                if (!$accordion.hasClass('js--no-accordion')) {
                    // validating class
                    if (currentBox.hasClass('js--active')) {
                        currentBox.removeClass('js--active')
                        currentBox.find('.m-accordion__content').slideUp()
                    } else {
                        currentBox.addClass('js--active')
                        currentBox.find('.m-accordion__content').slideDown()
                    }

                    // validating to multiple
                    if (!settings.multiple) {
                        item.not(currentBox).removeClass('js--active')
                        item.not(currentBox).find('.m-accordion__content').slideUp()
                    }
                }
            })

            // adding ids
            if (settings.anchor) {
                let counter = 1

                $(this).find('.m-accordion__box').each(function() {
                    if (!$(this).attr('id')) {
                        $(this).attr('id', 'accordion-' + counterAccordion + '-content-' + counter)
                    }

                    counter++
                })
            }

            counterAccordion++
        })
    }

    // init
    $('.m-accordion').mAccordion()
}())

$.fn.entel_accordion = function (options) {
    /**
     * Vars settings
     */
    var settings = $.extend ({
    }, options)

    /**
     * For each item
     */
    return this.each(function () {
        /**
         * Vars
         */
        var $item_accordion = $(this),
            $item_accordion_title = $item_accordion.find ('.entel-accordion__title'),
            $item_accordion_content = $item_accordion.find ('.entel-accordion__content')

        $item_accordion_title.on ('click', function (e) {
            e.preventDefault()

            /**
             * Vars
             */
            var $item_accordion_current = $(this),
                $item_accordion_current_parent = $item_accordion_current.parent ('.entel-accordion__item'),
                $item_accordion_current_content = $item_accordion_current.next (),
                $item_accordion_current_content_inner = $item_accordion_current_content.find ('.entel-accordion__content__inner')

            /**
             * Set height current content accordion item by current content inner
             */
            $item_accordion_current_content.height ($item_accordion_current_content_inner.outerHeight ())

            /**
             * Set height 0 to all contents accordion item less current content accordion item
             */
            $item_accordion_content.not ($item_accordion_current_content).height (0)

            /**
             * If current item accordion item has active, else
             */
            if ($item_accordion_current.parent().hasClass('open')) {
                /**
                 * Remove active class to current item accordion item
                 */
                $item_accordion_current.parent().removeClass('open')

                /**
                 * Set height 0 to all content accordion item
                 */
                $item_accordion_content.height(0)
            } else {
                /**
                 * ADd active class to current item accordion item
                 */
                $item_accordion_current.parent().addClass('open')
            }

            /**
             * Remove active class all items accordion item less current item accordion item
             */
            $item_accordion_content.not($item_accordion_current_content).parent().removeClass('open')
        })
    })
}