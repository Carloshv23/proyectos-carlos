function goToTab () {
    // go elements var
    const $goElements = document.querySelectorAll('.m-go-to-tab')

    // tab vars
    const $tabNavs = document.querySelectorAll('.e-tab__functions__nav a')
    const $tabContents = document.querySelectorAll('.e-tab__main__content__parent .e-tab__content')

    const goToTab = (idTab) => {
        // vars
        const $content = document.querySelector(`.e-tab__content${idTab}`)
        const $nav = document.querySelector(`a.e-tab__button[href="${idTab}"`)

        if ($content && $nav) {
            resetTabs()

            // active current tab
            $nav.classList.add('js--active')
            $content.classList.add('js--open')
        }
    }

    const resetTabs = () => {
        $tabNavs.forEach($tab => {
            $tab.classList.remove('js--active')
        })

        $tabContents.forEach($content => {
            $content.classList.remove('js--open')
        })
    }

    (function init () {
        if (
            $tabNavs &&
            $tabContents
        ) {
            // iterating
            $goElements.forEach($anchor => {
                // click event
                $anchor.addEventListener('click', e => {
                    e.preventDefault()

                    // vars
                    const idTab = $anchor.getAttribute('data-tab-anchor')

                    // run
                    goToTab(idTab)
                })
            })
        }
    })()
}

goToTab()