import getParameterURL from './urlParameter'
//import PersistentData from './persistentData'

// Constantes
let
    urlPauta,
    utmGclid = getParameterURL('gclid'),
    utmSource = getParameterURL('utm_source'),
    utmMedium = getParameterURL('utm_medium'),
    utmCampaign = getParameterURL('utm_campaign'),
    utmContent = getParameterURL('utm_content'),
    utmTerm = getParameterURL('utm_term'),
    equipmentId = (typeof _URL_EQUIPO !== 'undefined') ? _URL_EQUIPO : getParameterURL('equipo'),
    inputOrigen = document.querySelectorAll('input[name=origen]'),
    inputUtm = document.querySelectorAll('input[name=utm]'),
    inputUtmSource = document.querySelectorAll('input[name=utm_source]'),
    inputUtmMedium = document.querySelectorAll('input[name=utm_medium]'),
    inputUtmCampaign = document.querySelectorAll('input[name=utm_campaign]'),
    inputUtmContent = document.querySelectorAll('input[name=utm_content]'),
    inputCampaign = document.querySelectorAll('input[name=campaign]'),
    inputUtmTerm = document.querySelectorAll('input[name=utm_term]'),
    inputDataCampaing = document.querySelectorAll('[data-source]');

// Llenado de persistencia
// const setPersistencia = () => {
//     let oPersistentData = new PersistentData(utmGclid, utmSource, utmMedium, utmCampaign, utmContent, utmTerm);
//     utmGclid = oPersistentData.utmGclid,
//         utmSource = oPersistentData.utmSource,
//         utmMedium = oPersistentData.utmMedium,
//         utmCampaign = oPersistentData.utmCampaign,
//         utmContent = oPersistentData.utmContent,
//         utmTerm = oPersistentData.utmTerm;
// }

const camposUtm = () => {
    let i_utm, i_utm_source, i_utm_medium, i_utm_campaign, i_utm_content, i_utm_term, i_campaign, i_origen;
    let
    urlPauta,
    utmGclid = getParameterURL('gclid'),
    utmSource = getParameterURL('utm_source'),
    utmMedium = getParameterURL('utm_medium'),
    utmCampaign = getParameterURL('utm_campaign'),
    utmContent = getParameterURL('utm_content'),
    utmTerm = getParameterURL('utm_term'),    
    inputOrigen = document.querySelectorAll('input[name=origen]'),
    inputUtm = document.querySelectorAll('input[name=utm]'),
    inputUtmSource = document.querySelectorAll('input[name=utm_source]'),
    inputUtmMedium = document.querySelectorAll('input[name=utm_medium]'),
    inputUtmCampaign = document.querySelectorAll('input[name=utm_campaign]'),
    inputUtmContent = document.querySelectorAll('input[name=utm_content]'),
    inputCampaign = document.querySelectorAll('input[name=campaign]'),
    inputUtmTerm = document.querySelectorAll('input[name=utm_term]'),
    inputDataCampaing = document.querySelectorAll('[data-source]');
    console.log(inputDataCampaing)
    if (utmGclid) {
        // Google        
        i_utm = 'utm_source=' + utmGclid;;
        i_utm_source = "google";
        i_utm_medium = "google_search";
        i_utm_campaign = "";
        i_utm_content = "";
        i_utm_term = "";
        i_campaign = '3028';
        i_origen = "GooglePagado";

    } else if (utmSource) {
        i_utm = 'utm_source=' + utmSource + '&utm_medium=' + utmMedium + '&utm_campaign=' + utmCampaign + '&utm_content=' + utmContent + '&utm_term=' + utmTerm;
        i_utm_source = utmSource;
        i_utm_medium = utmMedium;
        i_utm_campaign = utmCampaign;
        i_utm_content = utmContent;
        i_utm_term = utmTerm;
        i_campaign = '';
        i_origen = "";

        if (utmSource == "facebook") {
            if (utmMedium == "social_paid") {
                i_campaign = '1975'
                i_origen = "FacebookPagado";
            } else {
                i_campaign = '2776';
                i_origen = "FacebookOrganico";
            }
        } else if (utmSource === "social_paid") {
            if (utmMedium.toLowerCase() === "apex") {
                i_campaign = '3661';
                i_origen = "Apex";
            } else {
                i_campaign = '2066';
                i_origen = "Push";
            }
        } else if (utmSource === "programmatic_dco") {
            i_campaign = '';
            i_origen = "programmatic_dco";
        } else if (utmSource === "display_paid") {
            i_campaign = '3661';
            i_origen = "Apex";
        } else if (utmSource == "sms" || utmSource == "SMS") {
            i_campaign = '3063';
            i_origen = "SMS";
        } else if (utmSource == "satpush") {
            i_campaign = '3064';
            i_origen = "Sat_push";
        } else if (utmSource == "hubspot") {
            i_campaign = '3027';
            i_origen = "Mailing";
        } else if (utmSource == "programatica") {
            i_campaign = '3213';
            i_origen = "Programatica";
        } else if (utmSource == "programmatic_dco") {
            i_campaign = '';
            i_origen = "programmatic_dco";
        }
        else if (utmSource == "pushpushgo") {
            i_campaign = '2066';
            i_origen = "Push";
        } else if (utmSource == "google_display" || utmSource == "google") {
            i_campaign = '3588';
            i_origen = "GoogleDisplay";
        } else if (utmSource == "facebook_dmp") {
            i_campaign = '2066';
            i_origen = "FacebookDmp";
        } else if (utmSource == "facebook_dmp_2") {
            i_campaign = '2066';
            i_origen = "Facebook_dmp_2";
        } else if (utmSource == "messenger") {
            i_campaign = '2066';
            i_origen = "messenger";
        } else if (utmSource == "google_dco") {
            i_campaign = '2066';
            i_origen = "google_dco";
        } else if (utmSource === "APEX" || utmSource === "apex" || utmSource === "Apex") {
            i_campaign = '3661';
            i_origen = "Apex";
        } else {
            i_campaign = '2066';
            i_origen = "Propio";
        }
    } else {
        i_utm = 'no pagado';
        i_utm_source = "";
        i_utm_medium = "";
        i_utm_campaign = "";
        i_utm_content = "";
        i_utm_term = "";
        i_campaign = '3490';
        i_origen = "GoogleOrganico";
    }

    inputUtm.forEach(element => { element.value = i_utm });
    inputUtmSource.forEach(element => { element.value = i_utm_source });
    inputUtmMedium.forEach(element => { element.value = i_utm_medium });
    inputUtmCampaign.forEach(element => { element.value = i_utm_campaign });
    inputUtmContent.forEach(element => { element.value = i_utm_content });
    inputUtmTerm.forEach(element => { element.value = i_utm_term });
    inputCampaign.forEach(element => { element.value = i_campaign });
    inputDataCampaing.forEach(element => { element.setAttribute("data-source", i_origen) });
    inputOrigen.forEach(element => { element.value = getOrigen($(element.parentElement), i_origen, ""); })
    setUtmModalAutomatico(i_origen)
    //return { i_utm: i_utm, i_utm_source: i_utm_source, i_utm_medium: i_utm_medium, i_utm_campaign: i_utm_campaign, i_utm_content: i_utm_content, i_utm_term: i_utm_term, i_campaign: i_campaign, i_origen: i_origen };
}

function setOriginBannerForm() {
    let forms = document.querySelectorAll("form")
    forms.forEach(element => {
        let dataCampaign = element.getAttribute("data-campaign");
        let dataSource = element.getAttribute("data-source");
        let iorigen = element.querySelector('input[name=origen]');
        if(dataCampaign && dataSource){                        
            iorigen.value = getOrigen($(element.parentElement),dataSource, dataCampaign);
        }        
    })
}

function getOrigen($form, name, _sufijo = "") {
    //let tipo_producto = "Movil";
    let tipo_producto = ($form.find('input[name=category_product]').attr('type'))?$form.find('input[name=category_product]').val():"Movil";
    let origen = `Empresas_${tipo_producto}_${name}`;
    origen = setOriginCampaing(origen, name, _sufijo);
    return origen;
}

const setOriginCampaing = (dataOrigen, name, _sufijo) => {
    if (_sufijo !== "") {
        return `${dataOrigen}_` + _sufijo;
    }

    let setDataOrigen = dataOrigen;
    // ID de paginas de campaña Multilinea
    let pagesCampaingMultilinea = ["ficha_multilineas", "promociones_multilinea","combos_power"];

    // ID de paginas de campaña Chip
    let pagesCampaingChip = ["oferta_chip", "ficha_planes", "lineas-adicionales-empresas"];

    // ID de paginas de campaña Movil
    let pagesCampaingMovil = ["internet_movil_ilimitado", "internet-fijo-empresas", "catalog_internet", "internet-fijo", "internet-movil", "internet-inalambrico"];

    // ID de paginas de campaña Cuota cero
    let pagesCampaingCuotaCero = ["equipos_cuotas", "equipos_12_cuotas", "landing_18_cuotas","landing_12_cuotas"];

    // ID de paginas de campaña Iphone
    let pagesCampaingIphone = ["datail_equipment", "portabilidad-iphone"];


    let page = (document.querySelector("[data-page-current]")) ? document.querySelector("[data-page-current]").getAttribute("data-page-current") : "";// Devuelve el id de la pagina actual

    if (pagesCampaingCuotaCero.includes(page)) {
        // Analizando la campaña cuota cero
        setDataOrigen = dataOrigen + "_CuotaCero";
    } else if (pagesCampaingIphone.includes(page)) {
        // Analizando la campaña Iphone  
        if (page === "portabilidad-iphone") {
            setDataOrigen = dataOrigen + "_Iphone";
        } else {
            if (equipmentId.includes("iphone")) {
                setDataOrigen = dataOrigen + "_Iphone";
            }else{
                if(utmContent === "equipos_premium_comdata"){
                    setDataOrigen = dataOrigen + "_CuotaCero";
                }else if(utmContent != null){
                    if(utmContent.includes("lanzamiento_samsung_s22_boton_samsung_")){
                        setDataOrigen = dataOrigen + "_CuotaCero";
                    }
                }                
            }
        }

    } else if (pagesCampaingMultilinea.includes(page)) {
        setDataOrigen = `${setDataOrigen}_Multilinea`;        
    } else if (pagesCampaingChip.includes(page)) {
        setDataOrigen = `${setDataOrigen}_Chip`;
    } else if (pagesCampaingMovil.includes(page)) {
        setDataOrigen = `${setDataOrigen}_Internet_movil`;
    }
    return setDataOrigen;
}

// Realiza eventos.
const eventos = () => {
    $(document).on('click', 'body', function (event) {
        if ($(event.target).is('.m-modal__content > div') || $(event.target).is('.m-modal__scene') || $(event.target).is('.m-modal__close--js')) {
            camposUtm()
            setOriginBannerForm()
        }
    });
    $("[data-modal]").on("click", function (e) {
        let dataCampaign = $(this).attr("data-campaign");
        let dataSource = $(this).attr("data-source");
        let scenneIn = $(this).attr("data-scene-in");
        
        if (scenneIn && dataCampaign) {
            let $form = $("#" + scenneIn);
            let $inputOrigen = $form.find('input[name=origen]');
            $inputOrigen.val(getOrigen($form,dataSource, dataCampaign))
            

        }
    })
}

const setUtmModalAutomatico = (dataSource)=>{
    let modalAutomatico = document.querySelector("#e-modal__scene__form_modal_automatic")    
    if(modalAutomatico){
        let tipo = getTipoProductoModalAutomatico();
        if(tipo !==""){
            let form = modalAutomatico.querySelector("form");
            let inputOrigen = form.querySelector("input[name=origen]");
            inputOrigen.value= getOrigen($(form), dataSource, "Multilinea")
        }        
    }    
}
const getTipoProductoModalAutomatico=() => {
    let page = (document.querySelector("[data-page-current]")) ? document.querySelector("[data-page-current]").getAttribute("data-page-current") : "";// Devuelve el id de la pagina actual
    let tipo = "";
    if(page === "home" || page === "app-privado"){
        tipo = "Chip";           
    }else if(page === "portabilidad-iphone" || page ==="oferta_chip" || page ==="lineas-adicionales-empresas" || page === "migra_entel" || page === "equipos_12_cuotas" || page === "combos_power" || page === "promociones_multilinea" || page === "catalog_equipment" || page === "equipos_cuotas" || page ==="catalogo_home" || page === "oferta_exclusiva"){
        tipo = "Multilinea";  
    }
    return tipo;
}

export default () => {
    eventos()
    //setPersistencia()
    camposUtm()
    setOriginBannerForm()
}