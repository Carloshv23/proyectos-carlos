
// load


$(document).on('click', '.e-tab__button', function (e) {
    e.preventDefault();
    contenedor = $(this).data('contenedor');
    let id_tab = this.href.split("#")[1];
    //id_parent = $(this).parents('.'+contenedor);
    id_parent = $('.'+contenedor);
    // console.log(contenedor);
    if($(id_parent).find('.e-tab__button').filter('[data-contenedor="'+contenedor+'"]').hasClass('js--active')){
        if(!$(this).hasClass('js--active')){
            $(id_parent).find('.e-tab__button').filter('[data-contenedor="'+contenedor+'"]').removeClass('js--active');
            $(id_parent).find('.e-tab__content').filter('[data-contenedor="'+contenedor+'"]').removeClass('js--open');
            $(this).addClass('js--active');
            $('#'+id_tab).addClass('js--open');
        }
    }else{
        $(id_parent).find('.e-tab__button').filter('[data-contenedor="'+contenedor+'"]').removeClass('js--active');
        $(id_parent).find('.e-tab__content').filter('[data-contenedor="'+contenedor+'"]').removeClass('js--open');
        $(this).addClass('js--active');
        $('#'+id_tab).addClass('js--open');
    }
});

$(window).resize(function(){
    //resetTab();
});

let estado_reset=0;
function resetTab(){
    //var ancho_ventana;
    var ancho_ventana = 991;
    //ancho_ventana = ancho_ventana.split("-")[2];
    var contenedor = ".e-tab__container";
    //var data_container = $(contenedor).attr('class').split(".")[0];
    var data_container = "e-tab__container";
    if(window.innerWidth>ancho_ventana){
        if(estado_reset==0){
            new ResetButtonTab("#m-tab__button__desktop-container").init();
            $(contenedor).find('.e-tab__content').filter('[data-contenedor="'+data_container+'"]').removeClass('js--open');
            $(contenedor).find('.e-tab__content').filter('[data-contenedor="'+data_container+'"]').eq(0).addClass('js--open');
            estado_reset=1;
        }
    }else{
        if(estado_reset==1){
            new ResetButtonTab("#m-mobile-container").init();
            $(contenedor).find('.e-tab__content').filter('[data-contenedor="'+data_container+'"]').removeClass('js--open');
            $(contenedor).find('.e-tab__content').filter('[data-contenedor="'+data_container+'"]').eq(0).addClass('js--open');
            estado_reset=0;
        }
    }
};

class ResetButtonTab {
    constructor (idContainerButtonTab){
        this.idContainerButtonTab  = idContainerButtonTab
    }
    init(){
        $(this.idContainerButtonTab).find('.e-tab__button').removeClass('js--active');
        $(this.idContainerButtonTab).find('.e-tab__button').eq(0).addClass('js--active');
    }
}