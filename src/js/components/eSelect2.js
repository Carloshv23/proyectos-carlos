import Choices from "choices.js";

export default (dataOption) => {
    let
        singleSelectOption,
        singleSelectElement = '.e-input--select--js select'

    // single select
    singleSelectOption = {
        searchEnabled: false,
        itemSelectText: '',
        shouldSort: false,
        choices: dataOption,
        callbackOnCreateTemplates: function (template) {
            return {
                choice: (classNames, data) => {
                    return template(`
              <div class="${classNames.item} ${classNames.itemChoice} ${
            data.disabled ? classNames.itemDisabled : classNames.itemSelectable
            }" data-select-text="${this.config.itemSelectText}" data-choice ${
            data.disabled
              ? 'data-choice-disabled aria-disabled="true"'
              : 'data-choice-selectable'
            } data-id="${data.id}" data-value="${data.value}" ${
            data.groupId > 0 ? 'role="treeitem"' : 'role="option"'
            } 
            data-gtm-category="${data.customProperties ? data.customProperties['gtmCategory'] : ''}"
            data-gtm-action="${data.customProperties ? data.customProperties['gtmAction'] : ''}"
            data-gtm-label="${data.customProperties ? data.customProperties['gtmLabel'] : ''}"
            >
            ${data.label}
              </div>
            `);
                },
            };
        },
    }
    return new Choices(singleSelectElement, singleSelectOption)
}