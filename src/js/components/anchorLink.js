export default () => {
    document.querySelectorAll('.e-link__anchor').forEach(a => {
        a.addEventListener('click', function (e) {
            e.preventDefault();
            let 
            href = this.getAttribute("href"),
            elem = document.querySelector(href)||document.querySelector("a[name="+href.substring(1, href.length)+"]"),
            offset = this.getAttribute('data-offset'),
            offsetTop
            
            if (offset) {
                offsetTop = elem.offsetTop - offset
            } else {
                offsetTop = elem.offsetTop
            }
            console.log(offsetTop, elem.offsetTop)
            //gets Element with an id of the link's href 
            //or an anchor tag with a name attribute of the href of the link without the #
            window.scroll({
                top: offsetTop, 
                left: 0, 
                behavior: 'smooth' 
            });
            // document.getElementById("form-cta").scrollIntoView({behavior: 'smooth'});
            //if you want to add the hash to window.location.hash
            //you will need to use setTimeout to prevent losing the smooth scrolling behavior
           //the following code will work for that purpose
           /*setTimeout(function(){
                window.location.hash = this.hash;
            }, 2000); */
        });
    });
}