import 'jquery-validation/dist/jquery.validate'

export class FormValidate {
    constructor(options) {
        this.$form = options.form        
        this.submit = (options.submit === true || options.submit === false) ? options.submit : false
        this.notRequired = (options.notRequired) ? options.notRequired : []
        this.rules = (options.rules) ? options.rules : false
        this.success = (options.success) ? options.success : false
        this.disableButtom = (options.disableButtom) ? options.disableButtom : false
        this.sendAutomatic = (options.sendAutomatic) ? options.sendAutomatic : false
        this.messageError = (options.messageError) ? options.messageError : {}
        this.inputsConfirm = (typeof options.inputsConfirm === 'object' && Object.keys(options.inputsConfirm).length && options.inputsConfirm.hasOwnProperty('firstField') && options.inputsConfirm.hasOwnProperty('secondField')) ? options.inputsConfirm : false
        this.fields = {
            names: []
        }
    }

    getFields () {
        let required,
            currentName,
            fields = document.getElementById(this.$form[0].id).elements

        // iterate fields
        for (let i = 0; i < fields.length; i++) {
            required = true
            currentName = fields[i].getAttribute('name')

            // validating not requerid
            if (Array.isArray(this.notRequired) && this.notRequired.length && this.notRequired.includes(currentName)) {
                required = false
            }

            // addding fields
            this.fields[currentName] = {
                id: fields[i].getAttribute('id'),
                name: currentName,
                minlength: fields[i].getAttribute('minlength'),
                maxlength: fields[i].getAttribute('maxlength'),
                type: fields[i].getAttribute('type'),
                checked: fields[i].getAttribute('checked'),
                required: required
            }

            // addings names
            if (!this.fields.names.includes(currentName) && currentName) {
                this.fields.names.push(currentName)
            }
        }
    }
    
    /**
     * Realiza el envío automático del formulario si la
     * validación del formulario es true o completada.
    */
    enableSendAutomatic(){        
        if(this.sendAutomatic === true){    
            let $buttomSuccess= this.$form.find('[type="submit"]');            
            let sendButton = 0;            
            for (let i = 0; i < this.fields.names.length; i++) {
                if(this.$form.find('input[name="' + this.fields.names[i] + '"]').attr("type")==="checkbox"){
                    if (this.$form.find('input[name="' + this.fields.names[i] + '"]').is(':visible') && this.$form.find('input[name="' + this.fields.names[i] + '"]').is(':checked') === false && !this.notRequired.includes(this.fields.names[i])) {
                        sendButton ++; 
                        //console.log(this.fields.names[i]);
                    }
                }else{
                    if (this.$form.find('input[name="' + this.fields.names[i] + '"]').is(':visible') && this.$form.find('input[name="' + this.fields.names[i] + '"]').val() === '' && !this.notRequired.includes(this.fields.names[i])) {                    
                        sendButton ++;                
                        //console.log(this.fields.names[i]);        
                    }
                }
                
                if (this.$form.find('select[name="' + this.fields.names[i] + '"]').val() === '' && !this.notRequired.includes(this.fields.names[i])) {
                    sendButton ++;
                    //console.log(this.fields.names[i]);
                }
                
            }
            console.log(sendButton)
            if(sendButton === 0){                
                if(this.$form.valid()){
                    let validator = this.$form.validate()
                    $buttomSuccess.trigger('click')
                    validator.resetForm();
                }
                
            }                        
        }
    }

    enableButtom(){
        if(this.disableButtom){
            let $buttomSuccess= this.$form.find(".e-button");
            let isempty=false;
                  
            let fieldsTmp= Object.values(this.fields);
            let ckCheck = false;
            for (let i = 0; i < this.fields.names.length; i++) {
                if (this.$form.find('input[name="' + this.fields.names[i] + '"]').is(':visible') && this.$form.find('input[name="' + this.fields.names[i] + '"]').val() === '' && !this.notRequired.includes(this.fields.names[i])) {                
                    isempty = true;
                }
                if (this.$form.find('select[name="' + this.fields.names[i] + '"]').val() === '' && !this.notRequired.includes(this.fields.names[i])) {
                    isempty = true;              
                }
            }
            
            $buttomSuccess.prop('disabled', isempty);
        }        
    }
    validate() {
        let getRules,
            getMessage,
            getMessages,
            hasEmptyInputs,
            $this = this,
            $form = this.$form,
            submit = this.submit,
            success = this.success,
            inputsConfirm = this.inputsConfirm,
            idForm = (this.$form[0].id) ? this.$form[0].id : false

        hasEmptyInputs = function() {
            // iterate fields
            for (let i = 0; i < this.fields.names.length; i++) {
                if ($form.find('input[name="' + this.fields.names[i] + '"]').val() === '' && !this.notRequired.includes(this.fields.names[i])) {
                    return true
                }
            }

            return false
        }.bind(this)

        getRules = function() {
            let currentName,
                rules = {},
                namesecondFieldConfirm

            // validating rules
            if (typeof this.rules === 'object' && Object.keys(this.rules).length) {
                rules = this.rules
            } else {
                // settings rules
                for (let i = 0; i < this.fields.names.length; i++) {
                    currentName = this.fields.names[i]

                    // setting required
                    rules[currentName] = {
                        required: this.fields[currentName].required
                    }

                    // validating fields confirm
                    if (inputsConfirm) {
                        namesecondFieldConfirm = inputsConfirm.secondField.attr('name')

                        if (currentName === namesecondFieldConfirm) {
                            rules[currentName]['validate_repeat'] = true
                        }
                    }

                    // minlength
                    if (this.fields[currentName].minlength) {
                        rules[currentName]['minlength'] = this.fields[currentName].minlength
                    }
                    
                    // minlength
                    if (this.fields[currentName].maxlength) {
                        rules[currentName]['maxlength'] = this.fields[currentName].maxlength
                    }

                    // email
                    if (this.fields[currentName].type === 'email') {
                        rules[currentName]['validate_email'] = true
                    }
                    
                    // ruc
                    if (this.fields[currentName].name === 'ruc') {
                        rules[currentName]['validate_ruc'] = true
                    }
                }
            }

            return rules
        }.bind(this)

        getMessages = function() {
            let messages = {}

            // validating messages
            if (typeof this.messages === 'object' && Object.keys(this.messages).length) {
                messages = this.messages
            }

            return messages
        }.bind(this)

        getMessage = function(error) {
            let message = this.messageError.messages.error,
                $message = this.messageError.element

            // validating error
            if (error === 'error') {
                message = (message) ? message : '&nbsp;'
                $message.addClass('error').removeClass('success')
            } else {
                message = this.messageError.messages.success
                message = (message) ? message : '&nbsp;'

                $message.removeClass('error').addClass('success')
            }

            $message.html(message)
        }.bind(this)

        // load validate
        $form.on('submit', function (e) {
            e.preventDefault()
        }).validate({
            rules: getRules(),
            messages: getMessages(),
            submitHandler: function (form) {
                // getting message
                getMessage('success')

                // validation function success
                if (typeof success === 'function') {
                    
                    success()
                } else {
                    console.log('The form is correctly validated. Please enter a function "success"')
                }

                if (submit && idForm) {
                    document.getElementById(idForm).submit()
                }
            },
            errorPlacement: function () {
                // getting message
                getMessage('error')
            }
        })

        $form.find('input[type="checkbox"]').change(function() {
            if($(this).is(':checked')){
                $(this).val("on")
            }else{
                $(this).val("")
            }
        })

        $form.find('select').change(function() {
            $this.enableButtom()
            $this.enableSendAutomatic()
        })

        // if input is focus out
        $form.find('input').focusout(function() {
            if ($form.find('.e-input input.error').length < 1 && !hasEmptyInputs()) {
                // getting message
                getMessage('success')
            }
            $this.enableButtom()
            $this.enableSendAutomatic()
        })

        $form.find('input').change(function() {
            if ($form.find('.e-input input.error').length < 1 && !hasEmptyInputs()) {
                // getting message
                getMessage('success')
            }
            $this.enableButtom()
            $this.enableSendAutomatic()
        })

        // if input is focus out
        $form.find('input').on('input', function() {
            if ($form.find('.e-input input.error').length < 1 && !hasEmptyInputs()) {
                // getting message
                getMessage('success')
            }
            $this.enableButtom()
            $this.enableSendAutomatic()
        })

        // adding email method validate
        $.validator.addMethod('validate_email', function (value, element) {
            if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                return true
            }
            else {
                return false
            }
        }, 'Correo Inv&aacute;lido.')

        // adding case invalid ruc
        $.validator.addMethod("validate_ruc", function (value, element) {
            if (value === "20000000000" ||
                value === "20111111111" ||
                value === "20222222222" ||
                value === "20333333333" ||
                value === "20444444444" ||
                value === "20555555555" ||
                value === "20666666666" ||
                value === "20777777777" ||
                value === "20888888888" ||
                value === "20999999999") {
                return false
            } else {
                return true
            }
        }, 'RUC Inv&aacute;lido.')

        // adding extension method validate
        $.validator.addMethod('extension', function (value, element, param) {
            param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif"
            return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"))
        }, 'Please enter a value with a valid extension.')

        // adding filesize method validate
        $.validator.addMethod('filesize', function (value, element, param) {
            return this.optional(element) || (element.files[0].size <= param)
        }, 'File size must be less than {0}')

        // adding repeat method validate
        if (inputsConfirm) {
            $.validator.addMethod('validate_repeat', function (value, element) {
                let firstValue = $(inputsConfirm.firstField).val(),
                    secondField = $(inputsConfirm.secondField).val()

                if (firstValue === secondField) {
                    return true
                } else {
                    return false
                }

            }, 'Campo incorrecto.')
        }
    }

    validatingMessageError() {
        let $form = this.$form

        // validate error message object
        if (typeof this.messageError === 'object' && Object.keys(this.messageError).length) {
            // validating element
            if (!this.messageError.hasOwnProperty('element') && !this.messageError.element) {
                this.messageError.element = $form.find('.e-message-form')
            }

            // validating messages
            if (!this.messageError.hasOwnProperty('messages') && !Object.keys(this.messageError.messages).length) {
                this.messageError.messages = {
                    error: 'Lo sentimos, hay error de validación.',
                    success: 'Muy bien :)'
                }
            }
        } else {
            this.messageError = {
                element: $form.find('.e-message-form'),
                messages: {
                    error: 'Lo sentimos, hay error de validación.',
                    success: 'Muy bien :)'
                }
            }
        }
    }

    sendAutomaticForm(form){
        let $form = document.querySelector(form)
        let $ruc = $form.querySelector('input[name="ruc"]')
        let $celular = $form.querySelector('input[name="celular"]')
        let $radios = $form.querySelectorAll('input[name="modality"]')
        let $checkboxs = $form.querySelectorAll('input[type="checkbox"]')
        let $send = $form.querySelector(`button[type="submit"]`)

        if($ruc){
            $ruc.addEventListener('keydown', e =>{
                const lengthRuc = $ruc.value.length;
                setTimeout(() => {
                    if ($ruc.value.length >= 11 && $celular.value.length >= 9 && lengthRuc === 10) {
                       $send.click()
                    }
                }, 200)
            })
        }
        
        if($celular){
            $celular.addEventListener('keydown', e =>{        
                const lengthPhone = $celular.value.length;
                setTimeout(() => {
                    if ($ruc.value.length >= 11 && $celular.value.length >= 9 && lengthPhone === 8) {
                       $send.click();
                    }
                }, 200)
            })
        }

        if($radios){
            $radios.forEach($radioButton => {
                $radioButton.addEventListener('change', event => {
                  $send.click();
                })
            })
        }

        if($checkboxs){
            $checkboxs.forEach($checkbox => {
                $checkbox.addEventListener('change', event => {
                  $send.click();
                })
            })
        }
    }

    init() {
        // validating id form
        if (document.querySelector(`#${this.$form[0].id}`)) {
            this.validatingMessageError()
            this.getFields()
            this.validate()
            //this.sendAutomaticForm(`#${this.$form[0].id}`)
        } else {
            console.log('Please verify id form settings.')
        }
    }
}