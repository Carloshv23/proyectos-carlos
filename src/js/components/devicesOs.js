export class codeOS {
    constructor(options) {
        // options
        this.options = (typeof options === 'object' && Object.keys(options).length) ? options : false
        this.code = (this.options.code && typeof this.options.code === 'function') ? this.options.code : false
        this.os = (this.options.os) ? this.options.os : false

        // vars
        this.userAgent = navigator.userAgent || navigator.vendor || window.opera
        this.isIos = (/iPad|iPhone|iPod/.test(this.userAgent) && !window.MSStream)
        this.isAndroid = (/android/i.test(this.userAgent))
    }
    run() {
        // validating os parameter exists
        if (this.os) {
            switch (true) {
                case (this.os === 'ios' && this.isIos) :
                    this.code()
                    break
    
                case (this.os === 'android' && this.isAndroid) :
                    this.code()
                    break
            }
        }
    }
}

export class changeUrlLink {
    constructor(options) {
        // options
        this.options = (typeof options === 'object' && Object.keys(options).length) ? options : false
        this.element = (options.element) ? options.element : false,
        this.url = (options.url) ? options.url : false,
        this.gtmAction = (options.gtmAction) ? options.gtmAction : false,
        this.gtmLabel = (options.gtmLabel) ? options.gtmLabel : false
    }
    run() {     
        if (this.options && this.element) {
            const elementList = document.querySelectorAll(this.element)
            for (const element of elementList) {
                if (this.url) element.href = this.url   
                if (this.gtmAction) element.dataset.gtmAction = this.gtmAction   
                if (this.gtmLabel) element.dataset.gtmLabel = this.gtmLabel   
            }
        }
    }
}