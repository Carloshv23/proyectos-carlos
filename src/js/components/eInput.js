export let resetInputs
let clearInputs

$.fn.eInput = function () {
    //vars
    let $currentRemoveX = $(this).parent().parent().find('.e-input__remove-x')

    // remove input focus class if is empty
    clearInputs = function () {
        // find all inputs
        $('.e-input input').each(function () {
            $(this).removeClass('js--focus')
            $(this).parent().removeClass('js--focus')
            $(this).parent().find('span').removeClass('js--focus')

            if ($(this).val() === '') {
                $(this).parent().find('span').removeClass('js--active')
            } else {
                $(this).parent().find('span').addClass('js--active')
            }
        })
    }

    resetInputs = function () {
        clearInputs()
        $('.e-input input').each(function () {
            $(this).parent().find('span').removeClass('js--active')
        })
    }

    // adding description class
    if ($(this).parent().find('p').length) {
        $(this).parent().addClass('e-input--has-description--js').length
    }

    // if input is focus out
    $(this).focusout(function () {
        clearInputs()
    })

    // if input is focus
    $(this).focus(function () {
        clearInputs()

        // set focus to current input
        $(this).addClass('js--focus')
        $(this).parent().find('span').removeClass('js--active').addClass('js--focus')
        $(this).parent().addClass('js--focus')

        // if has class error
        if ($(this).hasClass('error')) {}

        // validating input with remove x
        setTimeout(function () {
            if ($currentRemoveX.length !== 0 && $(this).val() !== '') {
                $currentRemoveX.addClass('js--active')
            } else {
                $currentRemoveX.removeClass('js--active')
            }
        }.bind(this), 500)
    })

    // validate value
    $(this).on('input', function () {
        let $button = $(this).parent().parent().find('.e-input__button'),
            $removeX = $(this).parent().parent().find('.e-input__remove-x')

        if (this.value !== '') {
            $button.addClass('js--active')
            $removeX.addClass('js--active')
        } else {
            $button.removeClass('js--active')
            $removeX.removeClass('js--active')
        }
    })

    // click remove event
    $currentRemoveX.on('click', function () {
        $(this).val('')
        $(this).focus()
    }.bind(this))

    // validating input type
    let dataValidate = $(this).attr('data-validate')

    switch (dataValidate) {
        case 'numeric':
            $(this).mask('0#')
            break
    }
}

// init input
var $eInput = $('.e-input input, .e-input select')

$.each($eInput, function (index, item) {
    $(item).eInput()
})