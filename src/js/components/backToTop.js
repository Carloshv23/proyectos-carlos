class backToTop {
    constructor(){
        this.button = document.querySelectorAll('.e-button__back-to-top--js')
    }

    init() {
        if(this.button) {
            this.button.forEach(element => {
                element.addEventListener('click', function (event) {
                    window.scrollTo({ top: 0, behavior: 'smooth' });
                })
            })
        }
    }

    scroll() {        
        if (document.documentElement.scrollTop > 600) {
            this.button.forEach(element => {
                element.classList.add("js--active")
            })
        } else {
            this.button.forEach(element => {
                element.classList.remove("js--active")
            })
        }
    }
}

export default () => {
    new backToTop().init()
    window.onscroll = function () {
        new backToTop().scroll()
    };
}