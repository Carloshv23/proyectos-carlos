export class Anchor {
    constructor(options) {
        this.hasOptions = (options && options.constructor === Object) ? true :  false
        this.offset = (this.hasOptions && options.hasOwnProperty('offset') && options.offset) ? options.offset : 0
        this.detectScrollDirection = (this.hasOptions && options.hasOwnProperty('detectScrollDirection') && options.detectScrollDirection) ? options.detectScrollDirection : false

        // vars
        this.$anchors = document.querySelectorAll('.e-anchor')
    }

    init() {
        if (this.$anchors.length > 0) {
            this.clickEvent()
        }
    }

    clickEvent() {
        let anchor = null,
            currentOffset = null

        // event
        this.$anchors.forEach(($anchor) => {
            $anchor.addEventListener('click', (e) => {
                e.preventDefault()
    
                // set vars
                anchor = $anchor.getAttribute('href')
                currentOffset = $anchor.getAttribute('data-offset')
                currentOffset = (currentOffset && parseInt(currentOffset) >= 0) ? currentOffset : this.offset

                // validating scroll direction and desktop
                if (this.detectScrollDirection && window.innerWidth >= 1024) {
                    // upp
                    if ($(anchor).offset().top < window.scrollY) {
                        currentOffset = this.offset
                    }
                    // down
                    else {
                        currentOffset = 0
                    }
                }

                // validatting anchor
                if (anchor !== 'javascript:void(0)' && document.querySelector(anchor)) {
                    // moving scroll
                    $('html, body').animate({
                        scrollTop: $(anchor).offset().top - currentOffset
                    }, 500)
                }
            })
        })
    }
}