export default class mediaQuery {
    constructor(options) {
        this.options = (typeof options === 'object' && Object.keys(options).length) ? options : false
        this.mediaQuery = (options.mediaQuery && typeof options.mediaQuery === 'string') ? options.mediaQuery : false
        this.mediaQueryOn = (options.mediaQueryOn && typeof options.mediaQueryOn === 'function') ? options.mediaQueryOn : false
        this.mediaQueryOff = (options.mediaQueryOff && typeof options.mediaQueryOff === 'function') ? options.mediaQueryOff : false
    }

    resize() {
        if (this.options && this.mediaQuery) {
            let resizeFunction,
                media = window.matchMedia(`(${this.mediaQuery})`)

            // setting resize function
            resizeFunction = (media) => {
                // validating match
                if (media.matches) {
                    // code from media query
                    if (this.mediaQueryOn) {
                        this.mediaQueryOn()
                    }
                } else {
                    // else media query
                    if (this.mediaQueryOff) {
                        this.mediaQueryOff()
                    }
                }
            }

            // init resize function
            resizeFunction(media)
            media.addListener(resizeFunction)

            // resize windows
            window.addEventListener('resize', resizeFunction)
        }
    }
}