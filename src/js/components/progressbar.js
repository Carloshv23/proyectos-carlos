export class progressbar {
    constructor(options) {
        this.$element = options.element
        this.unity = null
        this.percent = null
        this.totalSteps = null
        this.step = (options.step) ? options.step : 1
        this.$barSteps = (options.element.find('span').mExists()) ? options.element.find('span') : false
    }

    loadBar() {
        this.totalSteps = parseInt(this.$element.attr('data-total-steps'))

        // validating steps
        if (this.totalSteps && this.totalSteps > 1 && this.$barSteps) {
            this.unity = (100 * 1) / this.totalSteps
            this.percent = this.unity * this.step

            this.$barSteps.css('width', this.percent + '%')

        } else if (this.totalSteps < 2) {
            console.log('Please insert total steps > 0')
        } else if (this.totalSteps){
            console.log('Please insert total steps value')
        }
    }

    init() {
        // validating
        if (this.$element.mExists() && this.$element[0].id) {
            this.loadBar()
        } else {
            console.log('Please verify id progressbar settings.')
        }
    }
}