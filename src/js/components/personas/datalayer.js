window.dataLayer = window.dataLayer || [];

var $entel_data_layer_btn,
  $entel_data_layer_btn_lead,
  $entel_data_layer_view,
  $entel_data_layer_view_banner,
  $entel_data_layer_product,
  $entel_data_layer_toggle,
  $entel_data_layer_view_firt = 0,
  $entel_data_layer_arrow,
  $entel_data_layer_slider_slick_dots;

var portalDataLayerCache = {
  load: function () {
    $entel_data_layer_btn = $(".entel-data-layer-btn");
    $entel_data_layer_view = $(".entel-data-layer-view");
    $entel_data_layer_product = $(".entel-data-layer-product");
    $entel_data_layer_toggle = $(".entel-data-layer-toggle");
    $entel_data_layer_arrow = $(".entel-slider").find(
      ".slick-arrow"
    );
    $entel_data_layer_slider = $(".entel-data-layer-slider");
  },
};

var portalDataLayer = {
  load: function () {
    $entel_data_layer_btn.on("click", function () {
      var $this = $(this),
        datalayer_category = $this.data("gtm-category"),
        datalayer_action = $this.data("gtm-action"),
        datalayer_label = $this.data("gtm-label");
      portalDataLayerPush.dataVirtualEvent(
        datalayer_category,
        datalayer_action,
        datalayer_label
      );
    });
    $entel_data_layer_toggle.on("click", function () {
      var $this = $(this),
        datalayer_category = $this.data("gtm-category"),
        datalayer_action = $this.data("gtm-action"),
        datalayer_label = $this.data("gtm-label");
      if ($this.hasClass("js--active")) {
        portalDataLayerPush.dataVirtualEvent(
          datalayer_category,
          datalayer_action,
          datalayer_label
        );
      }
      if ($this.hasClass("open")) {
        portalDataLayerPush.dataVirtualEvent(
          datalayer_category,
          datalayer_action,
          datalayer_label
        );
      }
    });
    $entel_data_layer_slider.on("click", ".slick-dots button", function () {
      let $this = $(this),
        datalayer_category = $this.data("gtm-category"),
        datalayer_action = $this.data("gtm-action"),
        datalayer_label = $this.data("gtm-label");
      console.log(datalayer_category);
      dataLayer.push({
        event: "virtualEvent",
        category: datalayer_category,
        action: datalayer_action,
        label: datalayer_label,
      });
    });
    $entel_data_layer_slider.on("click", ".slick-arrow", function () {
      let $this = $(this),
        datalayer_category = $this.data("gtm-category"),
        datalayer_action = $this.data("gtm-action"),
        datalayer_label = $this.data("gtm-label");
      dataLayer.push({
        event: "virtualEvent",
        category: datalayer_category,
        action: datalayer_action,
        label: datalayer_label,
      });
    });
  },
};

var portalBannerDataLayer = {
  load: function () {
    $entel_data_layer_view.on("click", function () {
      var $this = $(this),
        datalayer_id = $this.data("gtm-id"),
        datalayer_name = $this.data("gtm-name"),
        datalayer_position = $this.data("gtm-position"),
        datalayer_creative = $this.data("gtm-creative");

      portalDataLayerPush.dataPromotionClick(
        datalayer_id,
        datalayer_name,
        datalayer_position,
        datalayer_creative
      );
    });

    portalDataLayerPush.dataPromotionView(
      "Banner_Prevencion_01",
      "Comprometidos para que sigas conectado",
      "Prevención - Banner Superior - 1",
      "(not available)"
    );
  },
};

var portalProductDataLayer = {
  load: function () {
    $entel_data_layer_product.each(function () {
      var $this = $(this),
        datalayer_currencycode = $this.data("gtm-currencycode"),
        datalayer_id = $this.data("gtm-id"),
        datalayer_name = $this.data("gtm-name"),
        datalayer_price = $this.data("gtm-price"),
        datalayer_category = $this.data("gtm-category"),
        datalayer_brand = $this.data("gtm-brand"),
        datalayer_variant = $this.data("gtm-variant"),
        datalayer_position = $this.data("gtm-position"),
        datalayer_list = $this.data("gtm-list"),
        datalayer_modalidad = $this.data("gtm-modalidad"),
        datalayer_plan = $this.data("gtm-plan"),
        datalayer_cuotainicial = $this.data("gtm-cuotainicial"),
        datalayer_cuotamensual = $this.data("gtm-cuotamensual");

      portalDataLayerPush.dataProductPlanImpression(
        datalayer_currencycode,
        datalayer_id,
        datalayer_name,
        datalayer_price,
        datalayer_category,
        datalayer_brand,
        datalayer_variant,
        datalayer_position,
        datalayer_list,
        datalayer_modalidad,
        datalayer_plan,
        datalayer_cuotainicial,
        datalayer_cuotamensual
      );
    });

    $entel_data_layer_product.on("click", function () {
      var $this = $(this),
        datalayer_currencycode = $this.data("gtm-currencycode"),
        datalayer_id = $this.data("gtm-id"),
        datalayer_name = $this.data("gtm-name"),
        datalayer_price = $this.data("gtm-price"),
        datalayer_category = $this.data("gtm-category"),
        datalayer_brand = $this.data("gtm-brand"),
        datalayer_variant = $this.data("gtm-variant"),
        datalayer_position = $this.data("gtm-position"),
        datalayer_list = $this.data("gtm-list"),
        datalayer_modalidad = $this.data("gtm-modalidad"),
        datalayer_plan = $this.data("gtm-plan"),
        datalayer_cuotainicial = $this.data("gtm-cuotainicial"),
        datalayer_cuotamensual = $this.data("gtm-cuotamensual");

      portalDataLayerPush.dataProductPlanClick(
        datalayer_currencycode,
        datalayer_id,
        datalayer_name,
        datalayer_price,
        datalayer_category,
        datalayer_brand,
        datalayer_variant,
        datalayer_position,
        datalayer_list,
        datalayer_modalidad,
        datalayer_plan,
        datalayer_cuotainicial,
        datalayer_cuotamensual
      );
    });
  },
};

var portalDataLayerPush = {
  dataVirtualEvent: function (category, action, label) {
    // console.log('category=' + category + ' - action=' + action + ' - label=' + label);
    dataLayer.push({
      event: "virtualEvent",
      category: category,
      action: action,
      label: label,
    });
  },
  dataVirtualEventLead: function (category, action, label, leadid) {
    //console.log('category=' + category + ' - action=' + action + ' - label=' + label);
    dataLayer.push({
      event: "virtualEvent",
      category: category,
      action: action,
      label: label,
      leadId: leadid,
    });
  },
  dataPromotionView: function (id, name, position, creative) {
    // console.log('id=' + id + ' - name=' + name + ' - position=' + position + ' - creative=' + creative);
    dataLayer.push({
      event: "promotionView",
      ecommerce: {
        promoView: {
          promotions: [
            {
              id: id,
              name: name,
              position: position,
              creative: creative,
            },
          ],
        },
      },
    });
  },
  dataPromotionClick: function (id, name, position, creative) {
    // console.log('id=' + id + ' - name=' + name + ' - position=' + position + ' - creative=' + creative);
    dataLayer.push({
      event: "promotionClick",
      ecommerce: {
        promoClick: {
          promotions: [
            {
              id: id,
              name: name,
              position: position,
              creative: creative,
            },
          ],
        },
      },
    });
  },
  dataProductPlanImpression: function (
    moneda,
    id,
    name,
    price,
    category,
    brand,
    variant,
    position,
    list,
    modalidad,
    plan,
    cuota_inicial,
    cuota_mensual
  ) {
    //console.log('moneda=' + moneda + ' id=' + id + ' - name=' + name + ' - price=' + price + ' - category=' + category + ' - brand=' + brand + ' - variant=' + variant + ' - position=' + position + ' - list=' + list + ' - modalidad=' + modalidad + ' - plan=' + plan + ' - cuota_inicial=' + cuota_inicial + ' - cuota_mensual=' + cuota_mensual);
    dataLayer.push({
      event: "productImpression",
      ecommerce: {
        currencyCode: moneda,
        impressions: [
          {
            id: id,
            name: name,
            price: price,
            category: category,
            brand: brand,
            variant: variant,
            position: position,
            list: list,
            dimension7: modalidad,
            dimension8: plan,
            dimension9: cuota_inicial,
            dimension10: cuota_mensual,
          },
        ],
      },
    });
  },

  dataProductPlanClick: function (
    moneda,
    id,
    name,
    price,
    category,
    brand,
    variant,
    position,
    list,
    modalidad,
    plan,
    cuota_inicial,
    cuota_mensual
  ) {
    //console.log('moneda=' + moneda + ' id=' + id + ' - name=' + name + ' - price=' + price + ' - category=' + category + ' - brand=' + brand + ' - variant=' + variant + ' - position=' + position + ' - list=' + list + ' - modalidad=' + modalidad + ' - plan=' + plan + ' - cuota_inicial=' + cuota_inicial + ' - cuota_mensual=' + cuota_mensual);
    dataLayer.push({
      event: "productClick",
      ecommerce: {
        currencyCode: moneda,
        click: {
          actionField: { list: list },
          products: [
            {
              id: id,
              name: name,
              price: price,
              category: category,
              brand: brand,
              variant: variant,
              position: position,
              dimension7: modalidad,
              dimension8: plan,
              dimension9: cuota_inicial,
              dimension10: cuota_mensual,
            },
          ],
        },
      },
    });
  },
};

var portalDataLayerInit = {
  funciones: function () {
    portalDataLayerCache.load();

    // if ($entel_data_layer_btn.size() !== 0 || $entel_data_layer_toggle.size() !== 0 || $entel_data_layer_arrow.size() !== 0) {
    portalDataLayer.load();
    // }

    // if ($entel_data_layer_view.size() !== 0) {
    portalBannerDataLayer.load();
    // }

    // if ($entel_data_layer_product.size() !== 0) {
    portalProductDataLayer.load();
    // }
  },
};

jQuery(document).ready(portalDataLayerInit.funciones);
