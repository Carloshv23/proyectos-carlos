const date = new Date(), 
      primerDia = new Date(date.getFullYear(), date.getMonth(), 1),
      ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0),
      primerDiaDate = "", 
      primerDiaMonth = "", 
      ultimoDiaDate = "", 
      ultimoDiaMonth = ""

function validateDate(valDate, varDate) {
  if (valDate < 10) {
    return (varDate = "0" + valDate)
  } else {
    return (varDate = valDate)
  }
}
function validateMonth(valMonth, varMonth) {
  if (valMonth <= 9) {
    return (varMonth = "0" + valMonth)
  } else {
    return (varMonth = valMonth)
  }
}
function previousThreeMonth(monthNow) {
  if (document.querySelectorAll("span.previous-months")) {
    const meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"],
    numeroMes = parseInt(monthNow)
    if(!isNaN(numeroMes) && numeroMes>=1 && numeroMes<=12) { 
      document.querySelectorAll("span.previous-months").forEach((el) => {
        el.innerHTML =
          meses[numeroMes - 4]+
          ", "+
          meses[numeroMes - 3]+
          " y "+
          meses[numeroMes - 2]
      });
    }
  }
}
function setDateInLegals() {
  if (document.querySelectorAll("span.date-start")) {
    document.querySelectorAll("span.date-start").forEach((el) => {
      el.innerHTML =
        validateDate(primerDia.getDate(), primerDiaDate) +
        "/" +
        validateMonth(primerDia.getMonth() + 1 , primerDiaMonth) +
        "/" +
        primerDia.getFullYear()
    });
  }
  if (document.querySelectorAll("span.date-end")) {
    document.querySelectorAll("span.date-end").forEach((el) => {
      el.innerHTML =
        validateDate(ultimoDia.getDate(), ultimoDiaDate) +
        "/" +
        validateMonth(ultimoDia.getMonth() + 1, ultimoDiaMonth) +
        "/" +
        ultimoDia.getFullYear()
    });
  }
}
previousThreeMonth(ultimoDia.getMonth()+1)
setDateInLegals()