import { OpenModal, ChangeScene } from "../../modal";
import { resetInputs } from "../../eInput";
// import executeAtentionTime from '../../empresas/atentionTime'
export default class formSuccess {
  constructor(options) {
    this.options =
      typeof options === "object" && Object.keys(options).length
        ? options
        : false;
    this.form =
      typeof this.options.formID === "string"
        ? document.querySelector(this.options.formID)
        : false;
    this.page =
      typeof this.options.page === "string" && this.options.page
        ? this.options.page
        : false;
    this.modal =
      typeof this.options.modal === "object" ? this.options.modal : false;
  }
  init() {
    let $form = this.form;
    $.ajax({
      type: "POST",
      dataType: "json",
      url: ADMIN_URL,
      data: {
        action: "enviar_registros_varios",
        dataString: $(`#${this.form.getAttribute("id")}`).serialize(),
      },
      beforeSend: function () {
        $form
          .querySelector('button[type="submit"]')
          .classList.add("e-button--loading-inline");
      },
      success: function (data) {
        // open modal
        console.log(data);
        dataLayer.push({
          category: "Día de la felicidad",
          action: "Registro exitoso",
          label: "Déjanos tus datos - Registro exitoso",
        });
        new OpenModal("e-dia-felicidad__modal_success").init();
        resetInputs();
        this.form.reset();
        $("#e-button-enviar").removeClass("e-button--secondary");
        $("#e-button-enviar").addClass("e-button--disabled");
      }.bind(this),
      complete: function () {
        $form
          .querySelector('button[type="submit"]')
          .classList.remove("e-button--loading-inline");
      },
      error: function (err) {
        console.log(":(");
      },
    });
  }
}
