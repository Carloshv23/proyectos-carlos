let
    inputUrl = document.querySelectorAll('input[name=url]')

export default () => {
    if (inputUrl) {
        inputUrl.forEach(element => {
            element.value = window.location.origin + window.location.pathname      
        });
    }
}