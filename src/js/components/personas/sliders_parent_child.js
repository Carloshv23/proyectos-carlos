import 'slick-carousel'

$('.slider-nav__parent').slick({
  slidesToShow: 2,  
  slidesToScroll: 1,
  asNavFor: '.slider-for__parent',
  centerMode: true,
  focusOnSelect: true,
  arrows: false,
  dots:false,
  adaptiveHeight:true,
  mobileFirst:true,
  // infinite:false,
  swipeToSlide:false,
  responsive: [
    {
      breakpoint: 319,
      settings: {
        slidesToShow: 3,
      }
  },
    {
        breakpoint: 767,
        settings: {
          slidesToShow: 5,
        }
    },
    {
      breakpoint: 1023,
      settings: {
        slidesToShow: 7,
      }
  }
]
});

$('.slider-for__parent').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots:false,
    asNavFor: '.slider-nav__parent',
    adaptiveHeight: true,
    infinite:false,
    swipeToSlide:false,
    draggable: false,
});

slider_childs('app');
slider_childs('web_recargas');
slider_childs('mientel');
slider_childs('fb');
slider_childs('messenger');
slider_childs('whatsapp');
slider_childs('yape');
slider_childs('bolsas_app');
slider_childs('bolsas_web');
slider_childs('bolsas_portal');
slider_childs('new_app');
slider_childs('new_web_recargas');
slider_childs('web_mientel');
slider_options('1');
slider_options('2');
slider_options('3');
slider_options('4');
slider_options('5');
$('.entel-slider-opciones5-content').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  centerMode: false,
  asNavFor: '.entel-slider-opciones5-nav',
  adaptiveHeight: true,
  infinite: false,
  fade: true,
  draggable: false,
  swipe: false,
  cssEase: 'linear'
});
$('.entel-slider-opciones5-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.entel-slider-opciones5-content',
  dots: false,
  focusOnSelect: true,
  arrows: false,
  infinite: false,
  adaptiveHeight: true,
  mobileFirst: true,
  centerPadding: 7,
  responsive: [
  {
      breakpoint: 380,
      settings: {
          slidesToShow: 4
      }
      },
      {
      breakpoint: 700,
      settings: {
          slidesToShow: 5
      }
      }
  ]
});
function slider_childs (name) {
  $('.slider-nav__child'+name).slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for__child'+name,
    centerMode: true,
    variableWidth: false,
    arrows: false,
    centerPadding: '0',
    mobileFirst:true,
    swipeToSlide:false,
    responsive: [
        {
            breakpoint: 1023,
            settings: {
            centerPadding: '0',
            }
        }
    ]
  });
  $('.slider-for__child'+name).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      asNavFor: '.slider-nav__child'+name,
      focusOnSelect: true,
      mobileFirst:true,
      adaptiveHeight:true,
      swipeToSlide:false,
      responsive: [
          {
              breakpoint: 1023,
              settings: {
                slidesToShow: 5,
                dots: false,
                vertical:true,
              }
          }
      ]
  });
  
}

function slider_options(name) {
  $('.entel-slider-opciones5-detail-'+name).slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    centerMode: false,
    adaptiveHeight: true,
    infinite: false,
    fade: true,
    draggable: false,
    cssEase: 'linear',
    mobileFirst:true,
    responsive: [
        {
            breakpoint: 767,
            settings: {
              dots: true
            }
        }
    ]
  });
}
$('.slider-nav__parent slider-nav__parent__btns a').on('click', function () {
  $('.slider-nav').slick('refresh');
  $('.slider-for').slick('refresh');
});

$('.slider-nav__parent .slider-nav__parent__btns a').on('click', function () {
  if($(this).hasClass( "slider-nav__parent__btn--new_tiendas" )){
    // console.log('b')
    $(".slider-for .slick-list").addClass("autoh");
  }
  else{
    // console.log('a')
    $(".slider-for .slick-list").removeClass("autoh");
  }
});