import Choices from "choices.js";
import mediaQuery from "./mediaQuery";

let
    singleSelect,
    singleSelectOption,
    singleSelectElement = '.e-input--select--js select'

// single select
singleSelectOption = {
    searchEnabled: false,
    itemSelectText: '',
    shouldSort: false,
    choices: [
        {
            value: '',
            label: '-- Seleccionar --',
            customProperties: {
                gtmCategory: '',
                gtmAction: '',
                gtmLabel: ''
            },
        },
        {
            value: 'lower_price',
            label: 'Menor a mayor precio',
            customProperties: {
                gtmCategory: 'Catálogo Equipos - Empresas',
                gtmAction: 'Clic Menor a mayor precio',
                gtmLabel: 'Menor a mayor precio'
            },
        },
        {
            value: 'higher_price',
            label: 'Mayor a menor precio',
            customProperties: {
                gtmCategory: 'Catálogo Equipos - Empresas',
                gtmAction: 'Clic Mayor a menor precio',
                gtmLabel: 'Mayor a menor precio'
            },
        }
    ],
    callbackOnCreateTemplates: function (template) {
        return {
            choice: (classNames, data) => {
                return template(`
              <div class="${classNames.item} ${classNames.itemChoice} ${
              data.disabled ? classNames.itemDisabled : classNames.itemSelectable
            }" data-select-text="${this.config.itemSelectText}" data-choice ${
              data.disabled
                ? 'data-choice-disabled aria-disabled="true"'
                : 'data-choice-selectable'
            } data-id="${data.id}" data-value="${data.value}" ${
              data.groupId > 0 ? 'role="treeitem"' : 'role="option"'
            } 
            data-gtm-category="${data.customProperties ? data.customProperties['gtmCategory'] : ''}"
            data-gtm-action="${data.customProperties ? data.customProperties['gtmAction'] : ''}"
            data-gtm-label="${data.customProperties ? data.customProperties['gtmLabel'] : ''}"
            >
            ${data.label}
              </div>
            `);
            },
        };
    },    
}

singleSelect = new Choices(singleSelectElement, singleSelectOption);

let singleSelectInit = function () {
    singleSelect.init()
}
let singleSelectDestroy = function () {
    singleSelect.destroy()
}


export default () => {
    if (document.querySelectorAll(singleSelectElement)) {
        new Choices(singleSelectElement, singleSelectOption)
        /* new mediaQuery({
            'mediaQuery': 'min-width : 1024px',
            'mediaQueryOn': singleSelectInit,
            'mediaQueryOff': singleSelectDestroy
        }).resize() */
    }
};