import 'slick-carousel'
import mediaQuery from "./mediaQuery"

export class sliderBanner {
    constructor(options) {
        // options
        this.options = (typeof options === 'object' && Object.keys(options).length) ? options : false
        this.element = (options && options.element) ? options.element : false
    }

    run() {
        if (this.options && this.element) {
            $(this.element).slick({
                autoplaySpeed: 3500,
                //-lazyLoad: "progressive",
                speed: 600,
                //arrows: false,
                arrows: false,
                dots: true,
                mobileFirst: true,
                prevArrow: "<img class='a-left control-c prev slick-prev' src='https://www.entel.pe/wp-content/uploads/2021/12/catalogo_home_arrow_left_v3.png'>",
                nextArrow:"<img class='a-right control-c next slick-next' src='https://www.entel.pe/wp-content/uploads/2021/12/catalogo_home_arrow_right_v3.png'>",
                rows: 0,
                responsive: [
                    {
                        breakpoint: 1023,
                        settings: {
                            arrows: true,
                        }
                    }
                ]
            })
        }
    }

    resize(slick) {
        let sliderBannerDesktop = function () {
            slick.slick('slickUnfilter')
            slick.slick('slickFilter', ".e-empresas__slider__slide--desktop")
        }
        let sliderBannerMobile = function () {
            slick.slick('slickUnfilter')
            slick.slick('slickFilter', ".e-empresas__slider__slide--mobile")
        }
        new mediaQuery({
            'mediaQuery': 'min-width : 425px',
            'mediaQueryOn': sliderBannerDesktop,
            'mediaQueryOff': sliderBannerMobile
        }).resize()
    }
}