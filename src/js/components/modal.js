const elementBody = document.querySelector('body');
let elementModal;

export class Modal {
    init() {
        const
            buttonModalList = document.querySelectorAll(".m-modal__button")

        for (const buttonModal of buttonModalList) {
            buttonModal.addEventListener('click', function (event) {
                let idModal = this.dataset.modal;
                new OpenModal(idModal).init();
            })
        }

    }
}

export class OpenModal {
    constructor(idModal) {
        this.idModal = idModal
        
    }
    init() {
        try {
            let
                innerWidthBody, marginElementBody,
                innerWidthBodyScroll = elementBody.offsetWidth;
            elementBody.classList.add("m-modal__body");
            innerWidthBody = elementBody.offsetWidth;
            marginElementBody = innerWidthBody - innerWidthBodyScroll;
            elementBody.style.marginRight = marginElementBody + "px";
            elementModal = document.getElementById(this.idModal);
            elementModal.parentElement.classList.add("js--open");
            // reset escenas
            const resetSceneCurrent = elementModal.querySelectorAll('.m-modal__scene');
            resetSceneCurrent.forEach(function (element, value) {
                if (element.classList.contains('js--active')) {
                    element.classList.remove("js--active");
                }
                if (value === 0 && !element.classList.contains('js--active')) {
                    element.classList.add("js--active");
                }
            })
        } catch (error) {
            new CloseModal().close()
        }
    }
}

export class CloseModal {
    init() {
        
        const
            buttonCloseModalList = document.querySelectorAll(".m-modal__close--js")

        for (const buttonCloseModal of buttonCloseModalList) {
            buttonCloseModal.addEventListener('click', function (event) {
                elementBody.style.marginRight = "";
                elementBody.classList.remove("m-modal__body");
                elementModal.parentElement.classList.remove("js--open");
            })
        }

        document.onkeydown = function (evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                if (elementBody.classList.contains('m-modal__body')) {
                    elementBody.style.marginRight = "";
                    elementBody.classList.remove("m-modal__body");
                    elementModal.parentElement.classList.remove("js--open");
                }
            }
        }

        $(document).on('click', 'body', function (event) {            
            if ($(event.target).is('.m-modal__content > div') || $(event.target).is('.m-modal__scene') || $(event.target).is('.m-modal__close--js')) {
                elementBody.style.marginRight = "";
                elementBody.classList.remove("m-modal__body");
                elementModal.parentElement.classList.remove("js--open");    
            }
        });
    }

     close() {
        elementBody.style.marginRight = "";
        elementBody.classList.remove("m-modal__body");
        elementModal.parentElement.classList.remove("js--open"); 
    }
}

export class SceneModal {
    init() {
        const
            buttonSceneModalList = document.querySelectorAll(".m-modal__scene--js")
        let
            sceneIn, sceneOut

        for (const buttonSceneModal of buttonSceneModalList) {
            buttonSceneModal.addEventListener('click', function (event) {
                sceneIn = this.dataset.sceneIn;
                sceneOut = this.dataset.sceneOut;
                new ChangeScene(sceneIn, sceneOut).init();
                /* document.getElementById(sceneOut).classList.remove("js--active");
                document.getElementById(sceneIn).classList.add("js--active"); */
            })
        }
    }
}

export class ChangeScene {
    constructor(sceneIn, sceneOut) {
        this.sceneIn = sceneIn;
        this.sceneOut = sceneOut;
    }

    init() {
        //document.getElementById(this.sceneOut).classList.remove("js--active");
        //document.getElementById(this.sceneIn).classList.add("js--active");
        
        const sceneOut = document.getElementById(this.sceneOut);
        const sceneIn = document.getElementById(this.sceneIn);
        try {
            sceneOut.classList.remove("js--active");
            sceneIn.classList.add("js--active");
        } catch (error) {
            
            new CloseModal().close()
        }
    }
}

