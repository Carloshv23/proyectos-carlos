export default (url, name) => {
    const loadXHR = (url) => {
        return new Promise(function (resolve, reject) {
            try {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", url);
                xhr.responseType = "blob";
                xhr.onerror = function () { reject("Network error.") };
                xhr.onload = function () {
                    if (xhr.status === 200) { resolve(xhr.response) }
                    else { reject("Loading error:" + xhr.statusText) }
                };
                xhr.send();
            }
            catch (err) { reject(err.message) }
        });
    }

    const downloadFile = (blob, filename) => {
        if (window.navigator.msSaveOrOpenBlob) {
            console.log("A1");
            window.navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            console.log("A2");
            const a = document.createElement('a');
            document.body.appendChild(a);
            const url = window.URL.createObjectURL(blob);
            a.href =url;
            a.download = filename;
            a.target = '_blank';            
            a.click();
            setTimeout(() => {
                window.URL.revokeObjectURL(url);
                document.body.removeChild(a);
            }, 0)
        }
    }

    loadXHR(url).then(function (blob) {
        downloadFile(blob, name)
    });
}