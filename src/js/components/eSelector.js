import Choices from "choices.js";

export default (dataOption) => {

    let
        singleSelectOption,
        singleSelectElement = '#departamento'

    // single select
    singleSelectOption = {
        searchEnabled: false,
        itemSelectText: '',
        shouldSort: false,
        choices: dataOption,
        placeholderValue: '¿En qué momento del día podemos comunicarnos?',
    }

    const selector = new Choices(singleSelectElement, singleSelectOption)
    const element = document.querySelector(singleSelectElement);
    const buttonSubmit = element.closest('form').querySelector('[type="submit"]');
    const labelElement = element.parentElement.closest('.entel__form_sl').querySelector('label');
    buttonSubmit.addEventListener("click", () => {
        if (element.value ==='') {
            element.parentElement.classList.add('choise-error');
            return false;
        }else{
            element.parentElement.classList.remove('choise-error');
        }
    });
    element.addEventListener(
        'change',
        function (event) {
            element.classList.remove('error');
            element.parentElement.classList.remove('choise-error');
            labelElement.classList.add('js--active')
        },
        false,
    );

    return selector
}