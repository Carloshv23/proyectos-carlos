export default class Loading {
    constructor() {
        this.body = document.querySelector('body')
        this.loadingPage = document.querySelector('.e-loading__page')
    }
    page() {
        if (this.loadingPage) {
            this.body.classList.add('e-loading__body')
            window.onload = function () {
                this.loadingPage.classList.add('js--close')            
                this.body.classList.remove('e-loading__body')
            }.bind(this);
        }
    }
}