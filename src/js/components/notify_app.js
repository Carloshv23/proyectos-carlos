import mediaQuery from './mediaQuery'
// Notificación del App de Entel
class notifyApp {
    constructor(options) {
        // options
        this.options = (typeof options === 'object' && Object.keys(options).length) ? options : false
        this.element = (options.element) ? document.querySelector(options.element) : false
    }

    scroll() {
        if (this.options && this.element) {
            // agregar clase que colocará al header debajo de la notificación del App
            document.querySelector("body").classList.add("e-empresas__notify-app__body");
            // colocar fixed top el header cuando se haga scroll (solo en mobile)
            let
                scrollpos = window.scrollY,
                mediaQueryOn,
                mediaQueryOff
            const
                notify_bar = this.element,
                notify_bar_height = notify_bar.offsetHeight,
                header = document.querySelector("#header, .e-catalogue__header"),
                add_class_on_scroll = () => header.classList.add("e-empresas__header--fixed"),
                remove_class_on_scroll = () => header.classList.remove("e-empresas__header--fixed")

            if (document.querySelector(".e-catalogue__header")) {
                document.querySelector("body").classList.add("e-empresas__notify-app__body--only-mobile")
            }

            mediaQueryOn = function () {
                scrollpos = window.scrollY;
                if (!this.element.classList.contains("js--close")) {
                    if (scrollpos >= notify_bar_height) {
                        add_class_on_scroll()
                    } else {
                        remove_class_on_scroll()
                    }
                }
            }.bind(this)
            mediaQueryOff = function () {
                remove_class_on_scroll()
            }


            window.addEventListener('scroll', function () {
                /* new mediaQuery({
                    'mediaQuery': 'max-width : 767px',
                    'mediaQueryOn': mediaQueryOn,
                    'mediaQueryOff': mediaQueryOff
                }).resize() */
                mediaQueryOn()
            })
        }
    }

    close() {
        if (this.options && this.element) {
            const notifyClose = document.querySelector('.e-empresas__notify-app__close--js')
            notifyClose.addEventListener('click', function (event) {
                document.querySelector("body").classList.remove("e-empresas__notify-app__body")
                document.querySelector("body").classList.remove("e-empresas__notify-app__body--only-mobile")
                this.element.classList.add("js--close")
            }.bind(this))
        }
    }
}


export default () => {
    const notify = '#e-empresas__notify-app'
    new notifyApp({
        element: notify
    }).scroll()
    new notifyApp({
        element: notify
    }).close()
}