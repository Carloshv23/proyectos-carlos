// generic function to get URL parameter
export default function getParameterURL(parameter, stringUrl) {
    let pageUrl = (stringUrl) ? stringUrl : decodeURIComponent(window.location.search.substring(1)),
        parameters = pageUrl.split('&'),
        parameterName,
        i

    for (i = 0; i < parameters.length; i++) {
        parameterName = parameters[i].split('=')

        if (parameterName[0] === parameter) {
            return parameterName[1] === undefined ? true : parameterName[1]
        }
    }
}