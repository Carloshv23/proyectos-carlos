import getParameterURL from './urlParameter'
import PersistentData from './persistentData'

let
    urlPauta,
    utmGclid = getParameterURL('gclid'),
    utmSource = getParameterURL('utm_source'),
    utmMedium = getParameterURL('utm_medium'),
    utmCampaign = getParameterURL('utm_campaign'),
    utmContent = getParameterURL('utm_content'),
    utmTerm = getParameterURL('utm_term'),
    equipmentId = (typeof _URL_EQUIPO !== 'undefined')?_URL_EQUIPO : getParameterURL('equipo'),

    inputTipoProducto = document.querySelectorAll('input[name=tipo_producto]'),
    inputOrigen = document.querySelectorAll('input[name=origen]'),
    inputUtm = document.querySelectorAll('input[name=utm]'),
    inputUtmSource = document.querySelectorAll('input[name=utm_source]'),
    inputUtmMedium = document.querySelectorAll('input[name=utm_medium]'),
    inputUtmCampaign = document.querySelectorAll('input[name=utm_campaign]'),
    inputUtmContent = document.querySelectorAll('input[name=utm_content]'),
    inputCampaign = document.querySelectorAll('input[name=campaign]'),
    inputUtmTerm = document.querySelectorAll('input[name=utm_term]');

const setOriginCampaing = (dataOrigen,name) => {
    let setDataOrigen = dataOrigen;
    // ID de paginas de campaña Multilinea
    let pagesCampaingMultilinea = ["ficha_multilineas", "promociones_multilinea"];

    // ID de paginas de campaña Chip
    let pagesCampaingChip = ["oferta_chip","ficha_planes","lineas-adicionales-empresas"];
    
    // ID de paginas de campaña Movil
    let pagesCampaingMovil = ["internet_movil_ilimitado","internet-fijo-empresas","catalog_internet","internet-fijo", "internet-movil","internet-inalambrico"];

    // ID de paginas de campaña Cuota cero
    let pagesCampaingCuotaCero = ["equipos_cuotas", "equipos_12_cuotas"];
    
    // ID de paginas de campaña Iphone
    let pagesCampaingIphone = ["datail_equipment","portabilidad-iphone"];
    
    
    let page = (document.querySelector("[data-page-current]"))?document.querySelector("[data-page-current]").getAttribute("data-page-current"):"";// Devuelve el id de la pagina actual
    console.log(page);
    if (pagesCampaingCuotaCero.includes(page)) {
        // Analizando la campaña cuota cero
        if(name.toLowerCase() === "apex"){
            if (utmCampaign.includes("oferta_comercial_12m_comdata") || utmCampaign.includes("oferta_comercial_18m_comdata")) {
                setDataOrigen = dataOrigen + "_CuotaCero";
            }else{
                setDataOrigen = "Empresas_Movil_GoogleOrganico_CuotaCero";
            }
        }else{
            if(utmContent && utmCampaign){
                if ((utmContent.includes("oferta_comercial_12m_comdata") || utmContent.includes("oferta_comercial_18m_comdata")) && utmCampaign === "aon_terminales") {
                    setDataOrigen = dataOrigen + "_CuotaCero";
                }else{
                    setDataOrigen = "Empresas_Movil_GoogleOrganico_CuotaCero";
                }
            }else{
                setDataOrigen = "Empresas_Movil_GoogleOrganico_CuotaCero";
            }            
        }
    }else if (pagesCampaingIphone.includes(page)) {
        // Analizando la campaña Iphone  
        if(page==="portabilidad-iphone"){
            if(utmContent && utmCampaign){
                if(utmCampaign.includes("iphone") || utmContent.includes("iphone")) {
                    setDataOrigen = dataOrigen + "_Iphone";
                }else{
                    setDataOrigen = "Empresas_Movil_GoogleOrganico_Iphone";
                }
            }else{
                setDataOrigen = "Empresas_Movil_GoogleOrganico_Iphone";
            } 
        }else{
            if(equipmentId.includes("iphone")){
                if(utmContent && utmCampaign){
                    if(utmCampaign.includes("iphone") || utmContent.includes("iphone")) {
                        setDataOrigen = dataOrigen + "_Iphone";
                    }else{
                        setDataOrigen = "Empresas_Movil_GoogleOrganico_Iphone";
                    }
                }else{
                    setDataOrigen = "Empresas_Movil_GoogleOrganico_Iphone";
                }            
            }
        }      
                
    }else if (pagesCampaingMultilinea.includes(page)) {
        setDataOrigen = `${setDataOrigen}_Multilinea`;
    }else if (pagesCampaingChip.includes(page)) {
        setDataOrigen = `${setDataOrigen}_Chip`;
    }else if (pagesCampaingMovil.includes(page)) {
        setDataOrigen = `${setDataOrigen}_Internet_movil`;
    }
    return setDataOrigen;
}

/**
 * input(type='hidden' id='banner_category_product' name='category_product' value='Fijo')
 * Añadir este input con el value el prefijo de la categoría del producto.
 */
function getOrigen(formElement, name) {
    let tipo_producto = (formElement.querySelector('input[name=category_product]')) ? formElement.querySelector('input[name=category_product]').value : "Movil";
    let origen = `Empresas_${tipo_producto}_${name}`;
    origen = setOriginCampaing(origen,name);
    return origen;
}

export default function getUtm() {


    // Persistent Data
    let oPersistentData = new PersistentData(utmGclid, utmSource, utmMedium, utmCampaign, utmContent, utmTerm);
    utmGclid = oPersistentData.utmGclid,
        utmSource = oPersistentData.utmSource,
        utmMedium = oPersistentData.utmMedium,
        utmCampaign = oPersistentData.utmCampaign,
        utmContent = oPersistentData.utmContent,
        utmTerm = oPersistentData.utmTerm;



    if (utmGclid) {
        // Google
        urlPauta = 'utm_source=' + utmGclid;
        inputUtm.forEach(element => {
            element.value = urlPauta
        })
        inputUtmSource.forEach(element => {
            element.value = 'google'
        })
        inputUtmMedium.forEach(element => {
            element.value = 'google_search'
        })
        inputUtmCampaign.forEach(element => {
            element.value = ''
        })
        inputUtmContent.forEach(element => {
            element.value = ''
        })
        inputUtmTerm.forEach(element => {
            element.value = ''
        })
        inputCampaign.forEach(element => {
            element.value = '3028'
        })
        inputOrigen.forEach(element => {
            //element.value = 'Empresas_Movil_GooglePagado'
            element.value = getOrigen(element.parentElement, "GooglePagado");
        })
    } else if (utmSource) {
        // Otras pautas
        urlPauta = 'utm_source=' + utmSource + '&utm_medium=' + utmMedium + '&utm_campaign=' + utmCampaign + '&utm_content=' + utmContent + '&utm_term=' + utmTerm;
        inputUtm.forEach(element => {
            element.value = urlPauta
        })
        inputUtmSource.forEach(element => {
            element.value = utmSource
        })
        inputUtmMedium.forEach(element => {
            element.value = utmMedium
        })
        inputUtmCampaign.forEach(element => {
            element.value = utmCampaign
        })
        inputUtmContent.forEach(element => {
            element.value = utmContent
        })
        inputUtmTerm.forEach(element => {
            element.value = utmTerm
        })
        if (utmSource == "facebook") {
            if (utmMedium == "social_paid") {
                inputCampaign.forEach(element => {
                    element.value = '1975'
                })
                inputOrigen.forEach(element => {
                    //element.value = 'Empresas_Movil_FacebookPagado'
                    element.value = getOrigen(element.parentElement, 'FacebookPagado');
                })
            } else {
                inputCampaign.forEach(element => {
                    element.value = '2776'
                })
                inputOrigen.forEach(element => {
                    //element.value = 'Empresas_Movil_FacebookOrganico'
                    element.value = getOrigen(element.parentElement, 'FacebookOrganico');
                })
            }
        } else if (utmSource === "social_paid") {
            if (utmMedium === "APEX" || utmMedium === "apex" || utmMedium === "Apex") {
                inputCampaign.forEach(element => {
                    element.value = '3661'
                })
                inputOrigen.forEach(element => {
                    //element.value = 'Empresas_Movil_Apex'
                    element.value = getOrigen(element.parentElement, 'Apex')
                })
            } else {
                inputCampaign.forEach(element => {
                    element.value = '2066'
                })
                inputOrigen.forEach(element => {
                    //element.value = 'Empresas_Movil_Push'
                    element.value = getOrigen(element.parentElement, 'Push');
                })
            }
        } else if (utmSource === "programmatic_dco") {
                       
                        inputOrigen.forEach(element => {
                            //element.value = 'Empresas_Movil_Apex'
                            element.value = 'programmatic_dco'
                        })
          }  

        else if (utmSource === "display_paid") {
            inputCampaign.forEach(element => {
                element.value = '3661'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_Apex'
                element.value = getOrigen(element.parentElement, 'Apex')
            })
        } else if (utmSource == "sms") {
            inputCampaign.forEach(element => {
                element.value = '3063'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_SMS'
                element.value = getOrigen(element.parentElement, 'SMS')
            })
        } else if (utmSource == "satpush") {
            inputCampaign.forEach(element => {
                element.value = '3064'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_Sat_push'
                element.value = getOrigen(element.parentElement, 'Sat_push');
            })
        } else if (utmSource == "hubspot") {
            inputCampaign.forEach(element => {
                element.value = '3027'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_Mailing'
                element.value = getOrigen(element.parentElement, 'Mailing');
            })
        } else if (utmSource == "programatica") {
            inputCampaign.forEach(element => {
                element.value = '3213'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_Programatica'
                element.value = getOrigen(element.parentElement, 'Programatica');
            })
        } else if (utmSource == "programmatic_dco") {

            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_messenger'
                element.value = 'programmatic_dco';
            })
        }
        else if (utmSource == "pushpushgo") {
            inputCampaign.forEach(element => {
                element.value = '2066'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_Push'
                element.value = getOrigen(element.parentElement, 'Push');
            })
        } else if (utmSource == "google_display" || utmSource == "google") {
            inputCampaign.forEach(element => {
                element.value = '3588'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_GoogleDisplay'
                element.value = getOrigen(element.parentElement, 'GoogleDisplay')
            })
        } else if (utmSource == "facebook_dmp") {
            inputCampaign.forEach(element => {
                element.value = '2066'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_FacebookDmp'
                element.value = getOrigen(element.parentElement, 'FacebookDmp');
            })
        } else if (utmSource == "facebook_dmp_2") {
            inputCampaign.forEach(element => {
                element.value = '2066'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_FacebookDmp'
                element.value = getOrigen(element.parentElement, 'Facebook_dmp_2');
            })
        } else if (utmSource == "messenger") {
            inputCampaign.forEach(element => {
                element.value = '2066'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_messenger'
                element.value = getOrigen(element.parentElement, 'messenger');
            })
        } else if (utmSource == "google_dco") {
            inputCampaign.forEach(element => {
                element.value = '2066'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_google_dco'
                element.value = getOrigen(element.parentElement, 'google_dco');
            })
        }else if (utmSource === "APEX" || utmSource === "apex" || utmSource === "Apex") {
            inputCampaign.forEach(element => {
                element.value = '3661'
            })
            inputOrigen.forEach(element => {
                element.value = getOrigen(element.parentElement, 'Apex');
            })
        } else {
            inputCampaign.forEach(element => {
                element.value = '2066'
            })
            inputOrigen.forEach(element => {
                //element.value = 'Empresas_Movil_Push'
                element.value = getOrigen(element.parentElement, 'Push')
            })
        }
    } else {
        // No pagado
        inputUtm.forEach(element => {
            element.value = 'no pagado'
        })
        inputCampaign.forEach(element => {
            element.value = '3490'
        })
        inputOrigen.forEach(element => {
            //element.value = 'Empresas_Movil_GoogleOrganico'
            element.value = getOrigen(element.parentElement, 'GoogleOrganico');
        })
    }

    /* console.log(
        utmGclid
        +'\n'+utmSource
        +'\n'+utmMedium
        +'\n'+utmCampaign
        +'\n'+utmContent) */
}