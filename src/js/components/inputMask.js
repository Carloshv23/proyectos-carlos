import 'jquery-mask-plugin'

export class inputMask {
    constructor(options) {
        this.$input = options.input
        this.types = ['email', 'name', 'mobile', 'url', 'number', 'dni','phone', 'ruc', 'ruc_20', 'numbers_letters_and_space', 'address', 'url']
        this.type = (this.types.includes(options.type)) ? options.type : ''
        this.maxlength = (!isNaN(parseInt(options.maxlength))) ? options.maxlength : false
        this.pattern = options.pattern ? options.pattern : false
    }

    load() {
        let translation,
            $chain = 'Z',
            $number = '9#',
            patternTwo = ''

        // validating maxlength
        if (this.maxlength) {
            $chain = 'Z'.repeat(this.maxlength)
            $number = '9'.repeat(this.maxlength)
        }

        if (!this.pattern) {
            // validating type
            switch (this.type) {
                case 'email':
                    this.pattern = /[\w@\-.+]/
                    break

                case 'url':
                    this.pattern = /[\w@\-.+//:]/
                    break

                case 'name':
                    this.pattern = /[A-Za-zÑñÁáÉéÍíÓóÚú\s]/
                    break

                case 'address':
                case 'numbers_letters_and_space':
                    this.pattern = /[a-zA-Z0-9À-ÿ ,.-_]/
                    break

                case 'phone':
                    $chain = 'Z' + '0'.repeat(8)
                    this.pattern = /[9]/
                    break
                case "dni":
                    $chain = 'Z' + '0'.repeat(8)
                    this.pattern = /[0-9]/;
                    break;
                case 'ruc':
                    $chain = 'ZX' + '0'.repeat(9)
                    this.pattern = /[1-2]/

                    patternTwo = {
                        pattern: /[0]/,
                        optional: false,
                        optional: false
                    }
                    break

                case 'ruc_20':
                    $chain = 'ZX' + '0'.repeat(9)
                    this.pattern = /[2]/

                    patternTwo = {
                        pattern: /[0]/,
                        optional: false,
                        optional: false
                    }
                    break
            }
        }

        if (this.type === 'number') {
            this.$input.mask($number,{
                onInvalid: function (val, e, f, invalid, options) {
                    e.target.parentElement.querySelector("span").classList.remove("bounce-input")
                    e.target.parentElement.querySelector("span").classList.add("bounce-input")
                    e.target.classList.add('error');                    
                },
                onComplete: function (cep,e) {
                    e.target.parentElement.querySelector("span").classList.remove("bounce-input")
                    e.target.classList.remove('error');
                }
            })
        } else {
            translation = {
                'Z': {
                    pattern: this.pattern,
                    optional: false,
                    recursive: true
                }
            }

            // validdating second pattern
            if (typeof patternTwo === 'object') {
                translation['X'] = patternTwo
            }

            // setting mask
            this.$input.mask($chain, {
                translation: translation,
                onInvalid: function (val, e, f, invalid, options) {
                    e.target.parentElement.querySelector("span").classList.remove("bounce-input")
                    e.target.parentElement.querySelector("span").classList.add("bounce-input")
                    e.target.classList.add('error');                    
                },
                onComplete: function (cep,e) {
                    e.target.parentElement.querySelector("span").classList.remove("bounce-input")
                    e.target.classList.remove('error');
                }
            })
        }
    }

    init() {
        // validating id form
        if (this.$input.length && this.$input[0].id) {
            this.load()
        } else {
            console.log('Please verify id input settings.')
        }
    }
}