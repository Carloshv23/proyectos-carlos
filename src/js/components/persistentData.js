export default class PersistentData {
    constructor(utmGclid, utmSource, utmMedium, utmCampaign, utmContent, utmTerm) {
        this.utmGclid = utmGclid;
        this.utmSource = utmSource;
        this.utmMedium = utmMedium;
        this.utmCampaign = utmCampaign;
        this.utmContent = utmContent;
        this.utmTerm = utmTerm;
        this.init();
    }

    init() {
        let atributosUtm = [
            { "id": "gclid", "name": this.utmGclid },
            { "id": "utm_source", "name": this.utmSource },
            { "id": "utm_medium", "name": this.utmMedium },
            { "id": "utm_campaign", "name": this.utmCampaign },
            { "id": "utm_content", "name": this.utmContent },
            { "id": "utm_term", "name": this.utmTerm }
        ];
        if (this.utmSource !== undefined) {
            this.saveUtm(atributosUtm);
        } else {
            let atributosUtm = this.getUtmList();
            
            if (!atributosUtm === false) {
                this.utmGclid = atributosUtm.gclid;
                this.utmSource = atributosUtm.utm_source;
                this.utmMedium = atributosUtm.utm_medium;
                this.utmCampaign = atributosUtm.utm_campaign;
                this.utmContent = atributosUtm.utm_content;
                this.utmTerm = atributosUtm.utm_term;
            }
        }        
    }
    saveUtm(aAtributes) {
        if (typeof (Storage) !== 'undefined') {
            if(process.env.NODE_ENV !== 'production')
                // console.log("Guardando en almacén.");
            
            var timeNow = new Date().getTime();
            $.each(aAtributes, function (key, item) {
                if (typeof item.name !== 'undefined' && item.name !== 'undefined') {
                    localStorage.setItem("e_presistent_" + item.id, item.name);
                }
            });
            localStorage.setItem('e_presistent_created', timeNow);
        }
    }

    getUtmList() {
        // Duración de las variables
        var timeNow = new Date().getTime();
        var tiempoDeVidaLimite = 300; // En segundos
        var atributosCreadoEl = localStorage.getItem('e_presistent_created');
        var segundosTranscurridos = ((timeNow - atributosCreadoEl) / 1000);        
        if (parseInt(segundosTranscurridos) > tiempoDeVidaLimite) {
            if(process.env.NODE_ENV !== 'production')
                // console.log("Tiempo de vida expiró y destruyendo localstore", (tiempoDeVidaLimite + timeNow), (Number(atributosCreadoEl) + tiempoDeVidaLimite));
            this.destroyUtm();
            return false;
        } else {
            // Busca en el localStorage si tiene almacenadas atributos utm
            //if(process.env.NODE_ENV !== 'production')
                // console.log("Llenando de localstorage");
            var atributosUtm = {};
            var variablesSesionStorage = window.localStorage            
            var aUtmAceptados = ["gclid", "utm_source", "utm_medium", "utm_campaign", "utm_content", "utm_term"];
            $.each(variablesSesionStorage, function (key, item) {
                var idSesion = key.substring(0, 13);
                var nameInput = key.substring(13);
                if (idSesion === "e_presistent_" && $.inArray(nameInput, aUtmAceptados) > -1) {
                    var valor = (item !== "undefined") ? item : '';
                    atributosUtm[nameInput] = valor;
                }
            });            
            return atributosUtm;
        }

    }
    destroyUtm() {
        localStorage.clear();
    }
}
