(function () {
    $.fn.eInput = function() {
        // remove input focus class if is empty
        function clearInputs() {
            // find all inputs
            $('.entel-input input').each(function() {
                if ($(this).val() === '') {
                    $(this).removeClass('focus')
                    $(this).parent().find('span').removeClass('focus')
                }
            })
        }
        // if input is focus out
        $(this).focusout (function() {
            clearInputs()
        })

        // if input is focus
        $(this).focus (function() {
            clearInputs()

            // set focus to current input
            $(this).addClass('focus')
            $(this).parent().find('span').addClass('focus')

            // if has class error
            if ($(this).hasClass('error')) {
                console.log('error')
            }
        })
    }
}())