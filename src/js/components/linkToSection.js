/**
 * Permite linkear hacia una sección específica de la página
 */

export default (idLink, idSection) => {
    const $eLink = document.getElementById(idLink);

    $eLink.addEventListener('click', function (event) {
        $("html, body").animate({
            scrollTop: $(`#${idSection}`).offset().top - 55
        }, {
            duration: 500,
            easing: "swing"
        });
    })
}