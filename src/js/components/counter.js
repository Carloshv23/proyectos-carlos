export default () => {
    class Counter {
        constructor(data) {
            this.counter = document.querySelector('.counter-add');
            this.sliders = document.querySelectorAll('.slider-planesmovil_item');
            this.count = 1;
            // this.total = 4;
            this.detail;
            
            this.data = [
                [
                    {
                        total: 'S/ 74.80/mes',
                        plan: 'S/ 37.40'
                    },
                    {
                        total: 'S/ 109.70/mes',
                        plan: 'S/ 36.57'
                    },
                    {
                        total: 'S/ 144.60/mes',
                        plan: 'S/ 36.15'
                    },
                    {
                        total: 'S/ 179.50/mes',
                        plan: 'S/ 35.90'
                    },
                ],
                [
                    {
                        total: 'S/ 84.80/mes',
                        plan: 'S/ 42.40'
                    },
                    {
                        total: 'S/ 119.70/mes',
                        plan: 'S/ 39.90'
                    },
                    {
                        total: 'S/ 154.60/mes',
                        plan: 'S/ 38.65'
                    },
                    {
                        total: 'S/ 189.50/mes',
                        plan: 'S/ 37.90'
                    },
                ],
                [
                    {
                        total: 'S/ 90.80/mes',
                        plan: 'S/ 45.40'
                    },
                    {
                        total: 'S/ 125.70/mes',
                        plan: 'S/ 41.90'
                    },
                    {
                        total: 'S/ 160.60/mes',
                        plan: 'S/ 40.15'
                    },
                    {
                        total: 'S/ 195.50/mes',
                        plan: 'S/ 39.10'
                    },
                ],
                [
                    {
                        total: 'S/ 94.80/mes',
                        plan: 'S/ 47.40'
                    },
                    {
                        total: 'S/ 129.70/mes',
                        plan: 'S/ 43.23'
                    },
                    {
                        total: 'S/ 164.60/mes',
                        plan: 'S/ 41.15'
                    },
                    {
                        total: 'S/ 199.50/mes',
                        plan: 'S/ 39.90'
                    },
                ],
                [
                    {
                        total: 'S/ 109.80/mes',
                        plan: 'S/ 54.90'
                    },
                    {
                        total: 'S/ 144.70/mes',
                        plan: 'S/ 48.23'
                    },
                    {
                        total: 'S/ 179.60/mes',
                        plan: 'S/ 44.90'
                    },
                    {
                        total: 'S/ 214.50/mes',
                        plan: 'S/ 42.90'
                    },
                ],
                [
                    {
                        total: 'S/ 124.80/mes',
                        plan: 'S/ 62.40'
                    },
                    {
                        total: 'S/ 159.70/mes',
                        plan: 'S/ 53.23'
                    },
                    {
                        total: 'S/ 194.60/mes',
                        plan: 'S/ 48.65'
                    },
                    {
                        total: 'S/ 229.50/mes',
                        plan: 'S/ 45.90'
                    },
                ],
                [
                    {
                        total: 'S/ 134.80/mes',
                        plan: 'S/ 67.40'
                    },
                    {
                        total: 'S/ 169.70/mes',
                        plan: 'S/ 56.57'
                    },
                    {
                        total: 'S/ 204.60/mes',
                        plan: 'S/ 51.15'
                    },
                    {
                        total: 'S/ 239.50/mes',
                        plan: 'S/ 47.90'
                    },
                ],
                [
                    {
                        total: 'S/164.80/mes',
                        plan: 'S/82.40'
                    },
                    {
                        total: 'S/199.70/mes',
                        plan: 'S/66.57'
                    },
                    {
                        total: 'S/234.60/mes',
                        plan: 'S/58.65'
                    },
                    {
                        total: 'S/269.50/mes',
                        plan: 'S/53.90'
                    },
                ],
            ]

            // console.log(this.data[4][0].total);
        }

        init() {
            if(this.counter) {
                let min = this.counter.querySelector('.min');
                let max = this.counter.querySelector('.max');
                this.detail = this.counter.querySelector('.detail');
                
                let self = this;

                min.addEventListener('click', function() {
                    self.count--;
                    self.setCount(self.count);
                })

                max.addEventListener('click', function() {
                    self.count++;
                    self.setCount(self.count);
                })
            }
        }

        setCount() {
            let msn = this.count > 1 ? ' Líneas adicionales' : ' Línea adicional';

            if(this.count > 4){
                this.count = 4;
            }else if(this.count < 1){
                this.count = 1;
            }

            this.detail.innerHTML = this.count + msn;

            this.setMount();
        }

        setMount() {
            this.data.forEach((item, index) => {
                console.log(index);
                console.log(item[this.count-1].total);
                console.log(item[this.count-1].plan);
                this.sliders[index].querySelector('.e-box-plan__prices .blue').innerHTML = item[this.count-1].total;
                this.sliders[index].querySelector('.e-box-plan__month strong').innerHTML = item[this.count-1].plan;
            });
        }
    }
    // init
    new Counter().init()
}