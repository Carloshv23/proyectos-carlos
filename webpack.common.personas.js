const path = require('path');
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {
    CleanWebpackPlugin
} = require('clean-webpack-plugin');
const WebpackMd5Hash = require("webpack-md5-hash");
const SpritesmithPlugin = require('webpack-spritesmith');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

let fileBan = process.env.npm_lifecycle_event.replace('start:', '');
fileBan = fileBan.replace('build:', '');
fileBan = fileBan.replace('builddev:', '');
const FILENAME = fileBan;
let pages = [`${FILENAME}`];
let oEmtry = pages.reduce(function (result, name, index, array) {
  result[name] = ['@babel/polyfill', path.resolve(__dirname, `./src/js/pages/personas/${name}/index.js`)];
  return result;
}, {})

module.exports = {
  entry: oEmtry,
  output: {
    filename: `js/${FILENAME}.js`,
    path: path.resolve(__dirname, 'dist'),
  },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
            ignoreOrder: false, // Enable to remove warnings about conflicting order,
        }),
        new HtmlWebpackPlugin({
            template: `./src/pug/pages/personas/${FILENAME}/index.pug`,
            hash: true,
            filename: `${FILENAME}.html`,
            chunks: [`${FILENAME}`],
            inject: process.env.NODE_ENV === 'production' ? false : true,
            minify: {
                collapseWhitespace: process.env.NODE_ENV === 'production' ? true : false
            }
        }), 
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve(__dirname, 'src/assets/sprite'),
                glob: '*.png'
            },
            target: {
                image: path.resolve(__dirname, 'src/assets/img/2020/09/spritev1.17.png'),
                css: path.resolve(__dirname, 'src/scss/base/sprite.scss')
            },
            apiOptions: {
                cssImageRef: "~spritev1.17.png"
            },
            spritesmithOptions: {
                padding: 20
            },
            retina: "@2x"
        }),
        new SpriteLoaderPlugin({
            plainSprite: true,
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new WebpackMd5Hash(),
    ],
    resolve: {
        modules: ["node_modules", "assets/img/2020/09", ]
    },
    module: {
        rules: [{
                test: /\.(sa|sc|c)ss$/i,
                use: 'style-loader'
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                use: MiniCssExtractPlugin.loader,
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                use: {
                    loader: 'css-loader',
                    options: {
                        sourceMap: true
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                exclude: [/plugin/],
                use: {
                    loader: "postcss-loader",
                    options: {
                        sourceMap: true
                    }
                }
            },
            {
                test: /\.(sa|sc|c)ss$/i,
                use: {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true,
                        implementation: require("sass"),
                        sassOptions: {
                            fiber: false,
                        },
                    }
                }
            },
            {
                test: /\.(png|svg|jpg|gif|webp)$/,
                include: path.resolve(__dirname, 'src/assets/img'),
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    //outputPath: process.env.NODE_ENV === 'production' ? '/wp-content/uploads/2020/08/' : 'img',
                    //outputPath: 'img',
                    outputPath: (url, resourcePath, context) => {
                        // To get relative path you can use
                        const relativePath = path.relative(context, resourcePath);
                        const folders = path.dirname(relativePath).split(path.sep)
                        const folderYear = folders[folders.length - 1]
                        const folderMonth = folders[folders.length - 2]
                        if (process.env.NODE_ENV === 'production') {
                            return `/wp-content/uploads/${folderMonth}/${folderYear}/${url}`;
                        } else {
                            return `${folderMonth}/${folderYear}/${url}`;
                        }
                    },
                },
            },
            {
                test: /\.(mp4)$/,
                include: path.resolve(__dirname, 'src/assets/media'),
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'media',
                },
            },
            {
                test: /\.svg$/,
                include: path.resolve(__dirname, 'src/assets/svg'),
                use: [{
                    loader: 'svg-sprite-loader',
                    options: {
                        extract: true,
                        spriteFilename: "img/sprite.svg"
                    }
                }]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
                include: path.resolve(__dirname, 'src/fonts'),
                use: [{
                    loader: 'file-loader',
                    options: {
                        outputPath: 'fonts',
                    },
                }],
            },
            {
                test: /\.pug$/,
                use: [{
                        loader: 'html-loader-srcset',
                        options: {
                            interpolate: true,
                            attrs: ['img:src', ':srcset', 'img:data-src', ':data-srcset', ':data-lazy', ':data-poster', 'source:data-src'],
                        }
                    },
                    {
                        loader: 'pug-html-loader',
                        options: {
                            pretty: true,
                        }
                    }
                ]
            },
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: require.resolve('jquery'),
                use: [{
                    loader: 'expose-loader',
                    options: 'jQuery'
                }, {
                    loader: 'expose-loader',
                    options: '$'
                }]
            },
            {
                type: 'javascript/auto',
                test: /\.json$/,
                include: /(lottie)/,
                loader: 'lottie-web-webpack-loader',
                options: {
                    assets: {
                        scale: 0.5 // proportional resizing multiplier
                    }
                }
            }
        ],
    },
};